# README #

GUAP3A.COM

Proyecto voluntario pro fondos para Venezuela.

Somos ganas, somos manos a la obra,
Somos venezolanas de raíces bien arraigadas,
Somos tres que no están allá,
con mucho por hacer acá.
Somos publicistas que con un puño y letra,
se unen por su país.
Somos gente que apaña*,
Somos gente que guapea*.

Version 1.5
Repositorio (https://claudiosvcl@bitbucket.org/claudiosvcl/guapea)

### Construido con: ###
- HTML5
- CSS3
- Boostrap 4
- Gulp
- JQuery
- Wordpress

### Si necesitas más información escribe a guap3a@gmail.com ###