<?php 
$timebeforerevote = 1;

add_action('wp_ajax_nopriv_post-like', 'gotham_post_like');
add_action('wp_ajax_post-like', 'gotham_post_like');


function gotham_post_like() {
  $nonce = $_POST['nonce'];
 
  if ( ! wp_verify_nonce( $nonce, 'ajax-nonce' ) )
   die ( 'Busted!');
    
  if(isset($_POST['post_like'])) {
    $ip = $_SERVER['REMOTE_ADDR'];
    $post_id = $_POST['post_id'];
    $voted_IP = '';
    $meta_IP = get_post_meta($post_id, "voted_IP");
    if( ! empty( $meta_IP ) ) {
      $voted_IP = $meta_IP[0];
    }
    if(!is_array($voted_IP)) {
      $voted_IP = array();
    }
    
    $meta_count = get_post_meta($post_id, "votes_count", true);

    if(!gotham_hasAlreadyVoted($post_id)) {
      $voted_IP[$ip] = time();

      update_post_meta($post_id, "voted_IP", $voted_IP);
      update_post_meta($post_id, "votes_count", ++$meta_count);
      
      echo $meta_count;
    }
    else {
      echo "already";
    }
  }
  exit;
}

function gotham_hasAlreadyVoted($post_id) {
  global $timebeforerevote;
  $voted_IP = '';
  $meta_IP = get_post_meta($post_id, "voted_IP");
  if( ! empty( $meta_IP ) ) {
    $voted_IP = $meta_IP[0];
  }
  if(!is_array($voted_IP)) {
    $voted_IP = array();
  }
  $ip = $_SERVER['REMOTE_ADDR'];
  
  if(in_array($ip, array_keys($voted_IP))) {
    $time = $voted_IP[$ip];
    $now = time();
    
    if(round(($now - $time) / 60) > $timebeforerevote)
      return false;
      
    return true;
  }
  
  return false;
}

function gotham_getPostLikeLink($post_id) {

  $vote_count = get_post_meta($post_id, "votes_count", true);

  if (empty($vote_count)) {
    $vote_count = 0;
  }

  $output = '<p class="post-like">';
  if(gotham_hasAlreadyVoted($post_id)) {
    $output .= ' <span class="like alrdvoted"></span>';
  }
  else {
    $output .= '<a href="#" data-post_id="'.esc_attr($post_id).'">
          <span class="like"></span>
        </a>';
  }
  if(is_singular('post')||is_singular('portfolio')){
    $output .= '<span class="count">'.esc_html($vote_count).'</span>';
  }
  $output .= '</p>';
  return $output;
}