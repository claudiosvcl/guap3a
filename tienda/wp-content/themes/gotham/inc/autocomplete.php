<?php 
add_action( 'wp_ajax_gotham_acsearch', 'gotham_autocomplete_suggestions' );
add_action( 'wp_ajax_nopriv_gotham_acsearch', 'gotham_autocomplete_suggestions' );

function gotham_autocomplete_suggestions(){
  // Query
  $posts = get_posts( array(
    's' =>$_REQUEST['term'], 'post_type' => array('post', 'portfolio', 'page'), 'posts_per_page' => -1
  ) );

  // Initialise suggestions array
  $suggestions=array();
  global $post;
  foreach ($posts as $post): setup_postdata($post);
  // Initialise suggestion array
  $suggestion = array();
  $suggestion['label'] = esc_html($post->post_title);
  $suggestion['link'] = esc_url(get_permalink());
  if(has_post_thumbnail()) {
    $suggestion['thumbnail'] = get_the_post_thumbnail();
  } 
  elseif(get_post_type($post->ID) == 'page') {
    $suggestion['thumbnail'] = '<div class="sebgcopa"></div>';
  }
  elseif (get_post_type($post->ID) == 'post') {
    $suggestion['thumbnail'] = '<div class="sebgcopo"></div>';
  }
  elseif(get_post_type($post->ID) == 'portfolio') {
    $suggestion['thumbnail'] = '<div class="sebgcopt"></div>';
  }
  $suggestion['post_type'] = get_post_type($post->ID);
  $suggestion['date'] = esc_html(get_the_date());

  $suggestions[]= $suggestion;
  endforeach;

  $response = $_GET["callback"] . "(" . json_encode($suggestions) . ")";
  echo $response;

  exit;
}