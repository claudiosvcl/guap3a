<?php
/**
 * This file allow to save images in customizer
 */ 
function gotham_load_customize_setting_image_data() {

    class gotham_Customize_Setting_Image_Data extends WP_Customize_Setting {

        /**
         * Overwrites the `update()` method so we can save some extra data.
         */
        protected function update( $value ) {

            if ( $value ) {

                $post_id = attachment_url_to_postid( $value );

                if ( $post_id ) {

                    $image = wp_get_attachment_image_src( $post_id );

                    if ( $image ) {

                        /* Set up a custom array of data to save. */
                        $data = array(
                            'url'    => esc_url_raw( $image[0] ),
                            'width'  => absint( $image[1] ),
                            'height' => absint( $image[2] ),
                            'id'     => absint( $post_id )
                        );

                        set_theme_mod( "{$this->id_data[ 'base' ]}_data", $data );
                    }
                }
            }

            /* No media? Remove the data mod. */
            if ( empty( $value ) || empty( $post_id ) || empty( $image ) )
                remove_theme_mod( "{$this->id_data[ 'base' ]}_data" );

            /* Let's send this back up and let the parent class do its thing. */
            return parent::update( $value );
        }
    }

}

add_action( 'customize_register', 'gotham_load_customize_setting_image_data', 1 );