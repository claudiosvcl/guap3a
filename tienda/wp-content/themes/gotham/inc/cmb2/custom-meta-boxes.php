<?php
/**
 * Custom metaboxes and fields for Gotham
 * http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * https://github.com/WebDevStudios/CMB2
 */

add_action( 'cmb2_admin_init', 'gotham_metaboxes' );

function gotham_metaboxes() {

	$prefix = 'gotham_';

  /**
   * Meta boxes in page, portfolio and post
   */

	$cmb_demo = new_cmb2_box( array(
		'id'           => $prefix . 'options_header',
		'title'        => esc_html__( 'Header Setting', 'gotham' ),
		'object_types' => array( 'page', 'portfolio', 'post' ),
		'priority'     => 'high',
	) );

	$cmb_demo->add_field( array(
	'name'      => esc_html__( 'Style', 'gotham' ),
	'desc'      => esc_html__( 'Change logo color and burger menu icon color', 'gotham' ),
	'id'        => $prefix . 'header_style',
	'type'      => 'select',
	'options'   => array(
		'dark'  => esc_html__( 'Dark', 'gotham' ),
		'light' => esc_html__( 'Light', 'gotham' ),
		),
	) );

  /**
   * Meta boxes in page
   */

	$cmb_demo = new_cmb2_box( array(
		'id'           => $prefix . 'options_page',
		'title'        => esc_html__( 'Title Area Settings', 'gotham' ),
		'object_types' => array( 'page' ),
		'priority'     => 'high',
	) );

	$cmb_demo->add_field( array(
	    'name'       => esc_html__( 'Title area', 'gotham' ),
	    'desc'       => esc_html__( 'Select alignment or no title area', 'gotham' ),
	    'id'         => $prefix . 'select_title_area',
	    'type'       => 'radio_inline',
	    'default'    => 'title_area',
	    'options'    => array(
	        'title_area'        => '<img src="'. get_template_directory_uri() .'/inc/cmb2/addons/left-title-area.png" style="height:50px;" alt="left-title-area">',
	        'title_area_center' => '<img src="'. get_template_directory_uri() .'/inc/cmb2/addons/center-title-area.png" style="height:50px;" alt="center-title-area">',
	        'no'                => '<img src="'. get_template_directory_uri() .'/inc/cmb2/addons/no-title-area.png" style="height:50px;" alt="no-title-area">',
	    ),
	) );

	$cmb_demo->add_field( array(
		'name'    => esc_html__( 'Title', 'gotham' ),
		'desc'    => esc_html__( 'Show title', 'gotham' ),
		'id'      => $prefix . 'select_title',
        'type'    => 'select',
		'options' => array(
			'page_title' => esc_html__( 'Yes', 'gotham' ),
			'no'         => esc_html__( 'No', 'gotham' ),
		),
	) );

	$cmb_demo->add_field( array(
		'name'    => esc_html__( 'Breadcrumb', 'gotham' ),
        'desc'    => esc_html__( 'Show breadcrumb', 'gotham' ),
        'id'      => $prefix . 'select_breadcrumb',
        'type'    => 'select',
		'options' => array(
			'breadcrumb' => esc_html__( 'Yes', 'gotham' ),
			''           => esc_html__( 'No', 'gotham' ),
		),
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Subtitle', 'gotham' ),
        'desc' => esc_html__( 'Add subtitle', 'gotham' ),
        'id'   => $prefix . 'subtitle',
        'type' => 'textarea_small',
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Text color', 'gotham' ),
        'desc' => esc_html__( 'Change default color', 'gotham' ),
        'id'   => $prefix . 'title_area_color',
        'type' => 'colorpicker',
        'std'  => ''
	) );

	$cmb_demo->add_field( array(
		'name'    => esc_html__( 'Arrow scroll to down', 'gotham' ),
        'desc' 	  => esc_html__( 'Display arrow down', 'gotham' ),
        'id'      => $prefix . 'arrow_down',
        'type'    => 'select',
		'options' => array(
			'arrow_down' => esc_html__( 'Yes', 'gotham' ),
			''           => esc_html__( 'No', 'gotham' ),
		),
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__('Height', 'gotham' ),
        'desc' => esc_html__( 'Change title area height', 'gotham' ),
        'id'   => $prefix . 'area_height',
        'type' => 'text',
	) );

	$cmb_demo->add_field( array(
		'name'             => esc_html__( 'Background', 'gotham' ),
		'desc'             => esc_html__( 'Choose background style', 'gotham' ),
		'id'               => $prefix . 'options_bg',
		'type'             => 'radio_inline',
		'options'          => array(
			'bg_image'     => esc_html__( 'Background Image', 'gotham' ),
			'bg_para'      => esc_html__( 'Background Parallax', 'gotham' ),
			'bg_color'     => esc_html__( 'Background Color', 'gotham' ),
		),
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'background image', 'gotham' ),
        'desc' => esc_html__( 'Upload background image', 'gotham' ),
        'id'   => $prefix . 'title_area_background_image',
        'type' => 'file',
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'background parallax', 'gotham' ),
        'desc' => esc_html__( 'Upload background image', 'gotham' ),
        'id'   => $prefix . 'title_area_parallax',
        'type' => 'file',
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'background-color', 'gotham' ),
        'desc' => esc_html__( 'Add background color', 'gotham' ),
        'id'   => $prefix . 'title_area_background_color',
        'type' => 'colorpicker',
	) );

	$cmb_demo = new_cmb2_box( array(
		'id'            => $prefix . 'options_sidebar',
		'title'         => esc_html__( 'Sidebar Setting', 'gotham' ),
		'object_types'  => array( 'page' ),
		'priority'      => 'default',
	) );

	$cmb_demo->add_field( array(
		'name'    => esc_html__( 'Sidebar', 'gotham' ),
        'desc'    => esc_html__( 'Add sidebar', 'gotham' ),
        'id'      => $prefix . 'select_sidebar',
        'type'    => 'select',
        'std'     => '',
		'options' => array(
			''              => esc_html__( 'No sidebar', 'gotham' ),
			'sidebar_right' => esc_html__( 'Sidebar right', 'gotham' ),
			'sidebar_left'  => esc_html__( 'Sidebar left', 'gotham' ),
		),
	) );

	$cmb_demo->add_field( array(
		'name'    =>  esc_html__( 'Sidebar widget area', 'gotham' ),
        'desc'    => esc_html__('Select sidebar', 'gotham' ),
        'id'      => $prefix . 'sidebar_widget_area',
        'type'    => 'select',
        'std'     => '',
		'options' => array(
			'main_sidebar'      => esc_html__( 'Main', 'gotham' ),
			'secondary_sidebar' => esc_html__( 'Secondary', 'gotham' ),
			'third_sidebar'     => esc_html__( 'Third', 'gotham' ),
			'fourth_sidebar'    => esc_html__( 'Fourth', 'gotham' ),
		),
	) );

	/**
     * Meta boxes in portfolio cpt
     */

	$cmb_demo = new_cmb2_box( array(
		'id'            => $prefix . 'options_portfolio',
		'title'         => esc_html__( 'Title Area Settings', 'gotham' ),
		'object_types'  => array( 'portfolio' ),
	) );

	$cmb_demo->add_field( array(
	    'name'    => esc_html__( 'Title area', 'gotham' ),
	    'desc'    => esc_html__( 'Select alignment or no title area', 'gotham' ),
	    'id'      => $prefix . 'select_title_area',
	    'type'    => 'radio_inline',
	    'default' => 'title_area',
	    'options' => array(
	        'title_area'        => '<img src="'. get_template_directory_uri() .'/inc/cmb2/addons/left-title-area.png" style="height:50px;" alt="left-title-area">',
	        'title_area_center' => '<img src="'. get_template_directory_uri() .'/inc/cmb2/addons/center-title-area.png" style="height:50px;" alt="center-title-area">',
	        'no'                => '<img src="'. get_template_directory_uri() .'/inc/cmb2/addons/no-title-area.png" style="height:50px;" alt="no-title-area">',
	    ),
	) );

	$cmb_demo->add_field( array(
		'name'    => esc_html__( 'Title', 'gotham' ),
        'desc'    => esc_html__( 'Show title', 'gotham' ),
        'id'      => $prefix . 'select_title',
        'type'    => 'select',
		'options' => array(
			'page_title' => esc_html__( 'Yes', 'gotham' ),
			'no'         => esc_html__( 'No', 'gotham' ),
		),
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Client', 'gotham' ),
        'desc' => esc_html__( 'Add client', 'gotham' ),
        'id'   => $prefix . 'portfolio_client',
        'type' => 'text',
	) );

	$cmb_demo->add_field( array(
		'name'    => esc_html__( 'Arrow scroll to down', 'gotham' ),
        'desc'    => esc_html__( 'Display arrow down', 'gotham' ),
        'id'      => $prefix . 'arrow_down',
        'type'    => 'select',
		'options' => array(
			'arrow_down' => esc_html__( 'Yes', 'gotham' ),
			''           => esc_html__( 'No', 'gotham' ),
		),
	) );

	$cmb_demo->add_field( array(
		'name' =>  esc_html__( 'Text color', 'gotham' ),
        'desc' => esc_html__( 'Change default color', 'gotham' ),
        'id'   => $prefix . 'title_area_color',
        'type' => 'colorpicker',
        'std'  => ''
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__('Height', 'gotham' ),
        'desc' => esc_html__( 'Change title area height', 'gotham' ),
        'id'   => $prefix . 'area_height',
        'type' => 'text',
	) );

	$cmb_demo->add_field( array(
		'name'             => esc_html__( 'Background', 'gotham' ),
		'desc'             => esc_html__( 'Choose background style', 'gotham' ),
		'id'               => $prefix . 'options_bg',
		'type'             => 'radio_inline',
		'options'          => array(
			'bg_image'     => esc_html__( 'Background Image', 'gotham' ),
			'bg_para'      => esc_html__( 'Background Parallax', 'gotham' ),
			'bg_color'     => esc_html__( 'Background Color', 'gotham' ),
		),
	) );

	$cmb_demo->add_field( array(
		'name' =>  esc_html__( 'Background image', 'gotham' ),
        'desc' => esc_html__( 'Upload background image', 'gotham' ),
        'id'   => $prefix . 'title_area_background_image',
        'type' => 'file',
	) );

	$cmb_demo->add_field( array(
		'name' =>  esc_html__( 'Background parallax', 'gotham' ),
        'desc' => esc_html__( 'Upload background image', 'gotham' ),
        'id'   => $prefix . 'title_area_parallax',
        'type' => 'file',
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Background-color', 'gotham' ),
        'desc' => esc_html__( 'Add background color', 'gotham' ),
        'id'   => $prefix . 'title_area_background_color',
        'type' => 'colorpicker',
	) );

	$cmb_demo = new_cmb2_box( array(
		'id'            => $prefix . 'options_project',
		'title'         => esc_html__( 'Project Information', 'gotham' ),
		'object_types'  => array( 'portfolio' ),
	) );

	$cmb_demo->add_field( array(
		'name' =>  esc_html__( 'Project title', 'gotham' ),
        'desc' => esc_html__( 'Add title', 'gotham' ),
        'id'   => $prefix . 'project_title',
        'type' => 'text',
	) );

	$cmb_demo->add_field( array(
		'name' =>  esc_html__( 'About this project', 'gotham' ),
        'desc' => esc_html__( 'Add description', 'gotham' ),
        'id'   => $prefix . 'about_project',
        'type' => 'textarea',
	) );

	$cmb_demo->add_field( array(
		'name' =>  esc_html__( 'Images', 'gotham' ),
        'desc' => esc_html__( 'Upload images', 'gotham' ),
        'id'   => $prefix . 'image',
        'type' => 'file_list',
	) );

	$cmb_demo->add_field( array(
		'name' =>  esc_html__( 'Video', 'gotham' ),
        'desc' => wp_kses( __( 'Enter youtube, vimeo, twitter, or instagram URL, supports services listed at <a href="http://codex.wordpress.org/Embeds">http://codex.wordpress.org/Embeds</a>', 'gotham' ), array( 'a' => array( 'href' => array() ) ) ),
        'id'   => $prefix . 'video1',
        'type' => 'oembed',
        'repeatable' => true,
	) );

	$cmb_demo->add_field( array(
		'name' =>  esc_html__( 'Presentation', 'gotham' ),
        'desc' => esc_html__( 'Choose a layout to present your portfolio. This field display images and video.', 'gotham' ),
        'id'   => $prefix . 'portfolio_presentation',
        'type' => 'select',
        'std'  => '',
		'options' => array(
			'small_image'  => esc_html__( 'Small image', 'gotham' ),
			'big_image'    => esc_html__( 'Big image', 'gotham' ),
			'small_slider' => esc_html__( 'Small slider', 'gotham' ),
			'big_slider'   => esc_html__( 'Big slider', 'gotham' ),
			''             => esc_html__( 'No display', 'gotham' ),
		),
	) );

	$cmb_demo->add_field( array(
		'name'    =>  esc_html__( 'Share portfolio', 'gotham' ),
        'desc'    => esc_html__( 'Display share button', 'gotham' ),
        'id'      => $prefix . 'share_port',
        'type'    => 'select',
		'options' => array(
			'yes' => esc_html__( 'Yes', 'gotham' ),
			'no'  => esc_html__( 'No', 'gotham' ),
		),
	) );

	$cmb_demo->add_field( array(
		'name' =>  esc_html__( 'Date', 'gotham' ),
        'desc' => esc_html__( 'Add date', 'gotham' ),
        'id'   => $prefix . 'portfolio_date',
        'type' => 'text_date',
	) );

	$cmb_demo->add_field( array(
		'name'      =>  esc_html__( 'Portfolio url', 'gotham' ),
        'desc'      => esc_html__( 'Enter url', 'gotham' ),
        'id'        => $prefix . 'portfolio_url',
        'type'      => 'text_url',
        'protocols' => array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet'),
	) );

	$cmb_demo->add_field( array(
		'name' =>  esc_html__( 'Color palette', 'gotham' ),
        'id'   => $prefix . 'color_palette',
        'type' => 'checkbox',
        'desc' => esc_html__( 'Add color palette', 'gotham' ),
	) );

	$cmb_demo->add_field( array(
		'name' =>  esc_html__( 'color 1', 'gotham' ),
        'id'   => $prefix . 'color_p1',
        'type' => 'colorpicker',
        'std'  => ''
	) );

	$cmb_demo->add_field( array(
		'name' =>  esc_html__( 'color 2', 'gotham' ),
        'id'   => $prefix . 'color_p2',
        'type' => 'colorpicker',
        'std'  => ''
	) );

	$cmb_demo->add_field( array(
		'name' =>  esc_html__( 'color 3', 'gotham' ),
        'id'   => $prefix . 'color_p3',
        'type' => 'colorpicker',
        'std'  => ''
	) );

	$cmb_demo->add_field( array(
		'name' =>  esc_html__( 'color 4', 'gotham' ),
        'id'   => $prefix . 'color_p4',
        'type' => 'colorpicker',
        'std'  => ''
	) );

	$cmb_demo->add_field( array(
		'name' =>  esc_html__( 'Informations title color', 'gotham' ),
        'desc' => esc_html__( 'Change default color', 'gotham' ),
        'id'   => $prefix . 'information_title_color',
        'type' => 'colorpicker',
        'std'  => ''
	) );

	$cmb_demo->add_field( array(
		'name' =>  esc_html__( 'Informations text color', 'gotham' ),
        'desc' => esc_html__( 'Change default color', 'gotham' ),
        'id'   => $prefix . 'information_text_color',
        'type' => 'colorpicker',
        'std'  => ''
	) );

	$cmb_demo->add_field( array(
		'name' =>  esc_html__( 'Portfolio background color', 'gotham' ),
        'desc' => esc_html__( 'Add background color', 'gotham' ),
        'id'   => $prefix . 'portfolio_background_color',
        'type' => 'colorpicker',
        'std'  => ''
	) );

	$cmb_demo->add_field( array(
		'name' =>  esc_html__( 'Project background color', 'gotham' ),
        'desc' => esc_html__( 'Add background color', 'gotham' ),
        'id'   => $prefix . 'project_background_color',
        'type' => 'colorpicker',
        'std'  => ''
	) );

	/**
     * Meta boxes in post
     */
    
    $cmb_demo = new_cmb2_box( array(
		'id'            => $prefix . 'options_post',
		'title'         => esc_html__( 'Title Area Settings', 'gotham' ),
		'object_types'  => array( 'post' ),
	) );

	$cmb_demo->add_field( array(
	    'name'    => esc_html__( 'Title area', 'gotham' ),
	    'desc'    => esc_html__( 'Select alignment', 'gotham' ),
	    'id'      => $prefix . 'select_title_area',
	    'type'    => 'radio_inline',
	    'default' => 'title_area',
	    'options' => array(
	        'title_area'        => '<img src="'. get_template_directory_uri() .'/inc/cmb2/addons/left-title-area.png" style="height:50px;" alt="left-title-area">',
	        'title_area_center' => '<img src="'. get_template_directory_uri() .'/inc/cmb2/addons/center-title-area.png" style="height:50px;" alt="center-title-area">',
	    ),
	) );

	$cmb_demo->add_field( array(
		'name'    => esc_html__( 'Informations', 'gotham' ),
        'desc'    => esc_html__( 'Show informations (Author name, date, number of comments)', 'gotham' ),
        'id'      => $prefix . 'select_description',
        'type'    => 'select',
        'std'     => 'Yes',
		'options' => array(
			'select_description' => esc_html__( 'Yes', 'cmb2' ),
			'no'                 => esc_html__( 'No', 'cmb2' ),
		),
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Text color', 'gotham' ),
        'desc' => esc_html__( 'Change default color', 'gotham' ),
        'id'   => $prefix . 'title_area_color',
        'type' => 'colorpicker',
        'std'  => ''
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Background parallax', 'gotham' ),
        'desc' => esc_html__( 'Upload background image', 'gotham' ),
        'id'   => $prefix . 'title_area_parallax',
        'type' => 'file',
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Background image', 'gotham' ),
        'desc' => esc_html__( 'Upload background image', 'gotham' ),
        'id'   => $prefix . 'title_area_background_image',
        'type' => 'file',
	) );

	$cmb_demo = new_cmb2_box( array(
		'id'            => $prefix . 'options_format',
		'title'         => esc_html__( 'Format Information for audio, quote, link or video', 'gotham' ),
		'object_types'  => array( 'post' ),
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Video', 'gotham' ),
        'desc' => wp_kses( __( 'Enter youtube, vimeo, twitter, or instagram URL, supports services listed at <a href="http://codex.wordpress.org/Embeds">http://codex.wordpress.org/Embeds</a>.', 'gotham' ), array( 'a' => array( 'href' => array() ) ) ),
        'id'   => $prefix . 'video_post',
        'type' => 'oembed',
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Quote', 'gotham' ),
        'desc' => esc_html__( 'Enter quote', 'gotham' ),
        'id'   => $prefix . 'quote_post',
        'type' => 'text',
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Author quote', 'gotham' ),
        'desc' => esc_html__( 'Enter author', 'gotham' ),
        'id'   => $prefix . 'author_quote_post',
        'type' => 'text',
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Audio', 'gotham' ),
        'desc' => wp_kses( __( 'Enter audio URL, supports services listed at <a href="http://codex.wordpress.org/Embeds">http://codex.wordpress.org/Embeds</a>', 'gotham' ), array( 'a' => array( 'href' => array() ) ) ),
        'id'   => $prefix . 'audio_post',
        'type' => 'oembed',
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Text url', 'gotham' ),
        'desc' => esc_html__( 'Enter text', 'gotham' ),
        'id'   => $prefix . 'link_text',
        'type' => 'text',
	) );

	$cmb_demo->add_field( array(
		'name'      => esc_html__( 'Url', 'gotham' ),
        'desc'      => esc_html__( 'Enter url', 'gotham' ),
        'id'        => $prefix . 'link_post',
        'type'      => 'text_url',
        'protocols' => array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet'),
	) );

	/**
     * Meta boxes in team cpt
     */
    
    $cmb_demo = new_cmb2_box( array(
		'id'            => $prefix . 'options_team',
		'title'         => esc_html__( 'Member Settings', 'gotham' ),
		'object_types'  => array( 'team', ),
	) );

	$cmb_demo->add_field( array(
		'name' =>  esc_html__( 'About the member', 'gotham' ),
        'desc' => esc_html__( 'Add informations', 'gotham' ),
        'id'   => $prefix . 'about_member',
        'type' => 'textarea',
	) );

	$cmb_demo->add_field( array(
		'name'      => 'Twitter',
        'desc'      => esc_html__( 'Enter Twitter url', 'gotham' ),
        'id'        => $prefix . 'twitter',
        'type'      => 'text_url',
        'protocols' => array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet'),
	) );

	$cmb_demo->add_field( array(
		'name'      => 'Facebook',
        'desc'      => esc_html__( 'Enter Facebook url', 'gotham' ),
        'id'        => $prefix . 'facebook',
        'type'      => 'text_url',
        'protocols' => array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet'),
	) );

	$cmb_demo->add_field( array(
		'name'      => 'Google+',
        'desc'      => esc_html__( 'Enter Google+ url', 'gotham' ),
        'id'        => $prefix . 'google',
        'type'      => 'text_url',
        'protocols' => array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet'),
	) );

	$cmb_demo->add_field( array(
		'name'      => 'Dribbble',
        'desc'      => esc_html__( 'Enter Dribbble url', 'gotham' ),
        'id'        => $prefix . 'dribbble',
        'type'      => 'text_url',
        'protocols' => array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet'),
	) );

	$cmb_demo->add_field( array(
		'name'      => 'Github',
        'desc'      => esc_html__( 'Enter Github url', 'gotham' ),
        'id'        => $prefix . 'github',
        'type'      => 'text_url',
        'protocols' => array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet'),
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Email', 'gotham' ),
        'desc' => esc_html__( 'Enter Email address', 'gotham' ),
        'id'   => $prefix . 'email',
        'type' => 'text_email',
	) );

	/**
     * Meta boxes in portfolio slider cpt
     */
    
	function gotham_get_term_options( $taxonomy = 'portfolio_category', $args = array() ) {
		$args['taxonomy'] = $taxonomy;
		$args = wp_parse_args( $args, array( 'taxonomy' => 'portfolio_category' ) );
		$taxonomy = $args['taxonomy'];
		$terms = (array) get_terms( $taxonomy, $args );
		// Initate an empty array
		$term_options = array();
		if ( ! empty( $terms ) ) {
			foreach ( $terms as $term ) {
				if( post_type_exists ('portfolio_slider')){
					$term_options[ strtolower($term->name) ] = strtolower($term->name);
				}
			}
		}
		return $term_options;
	}

	function gotham_get_title_options( $post_type = 'portfolio', $args = array() ) {
	    $titles = get_posts(array('post_type' => 'portfolio', 'posts_per_page'   => -1));
	    $title_options = array();
	    if ( ! empty( $titles ) ) {
	        foreach ( $titles as $title ) {
	            $title_options[ $title->ID ] = $title->post_title;
	        }
	    }
	    return $title_options;
	}
    
    $cmb_demo = new_cmb2_box( array(
		'id'            => $prefix . 'options_portfolio_slider',
		'title'         => esc_html__( 'Slide Settings', 'gotham' ),
		'object_types'  => array( 'portfolio_slider', ),
	) );

	$cmb_demo->add_field( array(
		'name'    => esc_html__( 'Category', 'gotham' ),
        'desc'    => esc_html__( 'Select a portfolio category', 'gotham' ),
        'id'      => $prefix . 'slidecat',
        'type'    => 'select',
		'options' => gotham_get_term_options(),
	) );

	$cmb_demo->add_field( array(
		'name'    => esc_html__( 'Title', 'gotham' ),
        'desc'    => esc_html__( 'Select a portfolio.', 'gotham' ),
        'id'      => $prefix . 'slidetitle',
        'type'    => 'select',
		'options' => gotham_get_title_options(),
	) );

	/**
     * Meta boxes in WooCommerce Slider cpt
     */
    if ( class_exists( 'WooCommerce' ) ) {
		function gotham_get_woocommerce_term_options( $taxonomy = 'product_cat', $args = array() ) {
			$args['taxonomy'] = $taxonomy;
			$args = wp_parse_args( $args, array( 'taxonomy' => 'product_cat' ) );
			$taxonomy = $args['taxonomy'];
			$terms = (array) get_terms( $taxonomy, $args );
			// Initate an empty array
			$term_options = array();
			if ( ! empty( $terms ) ) {
				foreach ( $terms as $term ) {
					if( post_type_exists ('woocommerce_slider')){
						$term_options[ strtolower($term->name) ] = strtolower($term->name);
					}
				}
			}
			return $term_options;
		}

		function gotham_get_woocommerce_title_options( $post_type = 'product', $args = array() ) {
		    $titles = get_posts(array('post_type' => 'product', 'posts_per_page'   => -1));
		    $title_options = array();
		    if ( ! empty( $titles ) ) {
		        foreach ( $titles as $title ) {
		            $title_options[ $title->ID ] = $title->post_title;
		        }
		    }
		    return $title_options;
		}
	    
	    $cmb_demo = new_cmb2_box( array(
			'id'            => $prefix . 'options_woocommerce_slider',
			'title'         => esc_html__( 'Slide Settings', 'gotham' ),
			'object_types'  => array( 'woocommerce_slider', ),
		) );

		$cmb_demo->add_field( array(
			'name'    => esc_html__( 'Category', 'gotham' ),
	        'desc'    => esc_html__( 'Select a product category', 'gotham' ),
	        'id'      => $prefix . 'productcat',
	        'type'    => 'select',
			'options' => gotham_get_woocommerce_term_options(),
		) );

		$cmb_demo->add_field( array(
			'name'    => esc_html__( 'Title', 'gotham' ),
	        'desc'    => esc_html__( 'Select a product.', 'gotham' ),
	        'id'      => $prefix . 'producttitle',
	        'type'    => 'select',
			'options' => gotham_get_woocommerce_title_options(),
		) );
	}
	
	/**
     * Meta boxes in process cpt
     */
    
    $cmb_demo = new_cmb2_box( array(
		'id'            => $prefix . 'options_process',
		'title'         => esc_html__( 'Step Setting', 'gotham' ),
		'object_types'  => array( 'process' ),
	) );

	$cmb_demo->add_field( array(
		'name' =>  esc_html__( 'About the step', 'gotham' ),
        'desc' => esc_html__( 'Add informations', 'gotham' ),
        'id'   => $prefix . 'about_step',
        'type' => 'textarea',
	) );

	$cmb_demo->add_field( array(
		'name'    => esc_html__( 'Icon', 'gotham' ),
        'desc'    => esc_html__( 'Select icon', 'gotham' ),
        'id'      => $prefix . 'select_icon',
        'type'    => 'select',
        'std'  	  => '',
		'options' => array(
			'3d-rotation' => '3d-rotation',
			'access-alarm' => 'access-alarm',
			'access-alarms' => 'access-alarms',
			'access-time' => 'access-time',
			'accessibility' => 'accessibility',
			'account-balance' => 'account-balance',
			'account-balance-wallet' => 'account-balance-wallet',
			'account-box' => 'account-box',
			'account-circle' => 'account-circle',
			'adb' => 'adb',
			'add' => 'add',
			'add-alarm' => 'add-alarm',
			'add-alert' => 'add-alert',
			'add-box' => 'add-box',
			'add-circle' => 'add-circle',
			'add-circle-outline' => 'add-circle-outline',
			'add-shopping-cart' => 'add-shopping-cart',
			'add-to-photos' => 'add-to-photos',
			'adjust' => 'adjust',
			'airline-seat-flat' => 'airline-seat-flat',
			'airline-seat-flat-angled' => 'airline-seat-flat-angled',
			'airline-seat-individual-suite' => 'airline-seat-individual-suite',
			'airline-seat-legroom-extra' => 'airline-seat-legroom-extra',
			'airline-seat-legroom-normal' => 'airline-seat-legroom-normal',
			'airline-seat-legroom-reduced' => 'airline-seat-legroom-reduced',
			'airline-seat-recline-extra' => 'airline-seat-recline-extra',
			'airline-seat-recline-normal' => 'airline-seat-recline-normal',
			'airplanemode-active' => 'airplanemode-active',
			'airplanemode-inactive' => 'airplanemode-inactive',
			'airplay' => 'airplay',
			'alarm' => 'alarm',
			'alarm-add' => 'alarm-add',
			'alarm-off' => 'alarm-off',
			'alarm-on' => 'alarm-on',
			'album' => 'album',
			'android' => 'android',
			'announcement' => 'announcement',
			'apps' => 'apps',
			'archive' => 'archive',
			'arrow-back' => 'arrow-back',
			'arrow-drop-down' => 'arrow-drop-down',
			'arrow-drop-down-circle' => 'arrow-drop-down-circle',
			'arrow-drop-up' => 'arrow-drop-up',
			'arrow-forward' => 'arrow-forward',
			'aspect-ratio' => 'aspect-ratio',
			'assessment' => 'assessment',
			'assignment' => 'assignment',
			'assignment-ind' => 'assignment-ind',
			'assignment-late' => 'assignment-late',
			'assignment-return' => 'assignment-return',
			'assignment-returned' => 'assignment-returned',
			'assignment-turned-in' => 'assignment-turned-in',
			'assistant' => 'assistant',
			'assistant-photo' => 'assistant-photo',
			'attach-file' => 'attach-file',
			'attach-money' => 'attach-money',
			'attachment' => 'attachment',
			'audiotrack' => 'audiotrack',
			'autorenew' => 'autorenew',
			'av-timer' => 'av-timer',
			'backspace' => 'backspace',
			'backup' => 'backup',
			'battery-alert' => 'battery-alert',
			'battery-charging-full' => 'battery-charging-full',
			'battery-full' => 'battery-full',
			'battery-std' => 'battery-std',
			'battery-unknown' => 'battery-unknown',
			'beenhere' => 'beenhere',
			'block' => 'block',
			'bluetooth' => 'bluetooth',
			'bluetooth-audio' => 'bluetooth-audio',
			'bluetooth-connected' => 'bluetooth-connected',
			'bluetooth-disabled' => 'bluetooth-disabled',
			'bluetooth-searching' => 'bluetooth-searching',
			'blur-circular' => 'blur-circular',
			'blur-linear' => 'blur-linear',
			'blur-off' => 'blur-off',
			'blur-on' => 'blur-on',
			'book' => 'book',
			'bookmark' => 'bookmark',
			'bookmark-border' => 'bookmark-border',
			'border-all' => 'border-all',
			'border-bottom' => 'border-bottom',
			'border-clear' => 'border-clear',
			'border-color' => 'border-color',
			'border-horizontal' => 'border-horizontal',
			'border-inner' => 'border-inner',
			'border-left' => 'border-left',
			'border-outer' => 'border-outer',
			'border-right' => 'border-right',
			'border-style' => 'border-style',
			'border-top' => 'border-top',
			'border-vertical' => 'border-vertical',
			'brightness-1' => 'brightness-1',
			'brightness-2' => 'brightness-2',
			'brightness-3' => 'brightness-3',
			'brightness-4' => 'brightness-4',
			'brightness-5' => 'brightness-5',
			'brightness-6' => 'brightness-6',
			'brightness-7' => 'brightness-7',
			'brightness-auto' => 'brightness-auto',
			'brightness-high' => 'brightness-high',
			'brightness-low' => 'brightness-low',
			'brightness-medium' => 'brightness-medium',
			'broken-image' => 'broken-image',
			'brush' => 'brush',
			'bug-report' => 'bug-report',
			'build' => 'build',
			'business' => 'business',
			'cached' => 'cached',
			'cake' => 'cake',
			'call' => 'call',
			'call-end' => 'call-end',
			'call-made' => 'call-made',
			'call-merge' => 'call-merge',
			'call-missed' => 'call-missed',
			'call-received' => 'call-received',
			'call-split' => 'call-split',
			'camera' => 'camera',
			'camera-alt' => 'camera-alt',
			'camera-enhance' => 'camera-enhance',
			'camera-front' => 'camera-front',
			'camera-rear' => 'camera-rear',
			'camera-roll' => 'camera-roll',
			'cancel' => 'cancel',
			'card-giftcard' => 'card-giftcard',
			'card-membership' => 'card-membership',
			'card-travel' => 'card-travel',
			'cast' => 'cast',
			'cast-connected' => 'cast-connected',
			'center-focus-strong' => 'center-focus-strong',
			'center-focus-weak' => 'center-focus-weak',
			'change-history' => 'change-history',
			'chat' => 'chat',
			'chat-bubble' => 'chat-bubble',
			'chat-bubble-outline' => 'chat-bubble-outline',
			'check' => 'check',
			'check-box' => 'check-box',
			'check-box-outline-blank' => 'check-box-outline-blank',
			'check-circle' => 'check-circle',
			'chevron-left' => 'chevron-left',
			'chevron-right' => 'chevron-right',
			'chrome-reader-mode' => 'chrome-reader-mode',
			'class' => 'class',
			'clear' => 'clear',
			'clear-all' => 'clear-all',
			'close' => 'close',
			'closed-caption' => 'closed-caption',
			'cloud' => 'cloud',
			'cloud-circle' => 'cloud-circle',
			'cloud-done' => 'cloud-done',
			'cloud-download' => 'cloud-download',
			'cloud-off' => 'cloud-off',
			'cloud-queue' => 'cloud-queue',
			'cloud-upload' => 'cloud-upload',
			'code' => 'code',
			'collections' => 'collections',
			'collections-bookmark' => 'collections-bookmark',
			'color-lens' => 'color-lens',
			'colorize' => 'colorize',
			'comment' => 'comment',
			'compare' => 'compare',
			'computer' => 'computer',
			'confirmation-number' => 'confirmation-number',
			'contact-phone' => 'contact-phone',
			'contacts' => 'contacts',
			'content-copy' => 'content-copy',
			'content-cut' => 'content-cut',
			'content-paste' => 'content-paste',
			'control-point' => 'control-point',
			'control-point-duplicate' => 'control-point-duplicate',
			'create' => 'create',
			'credit-card' => 'credit-card',
			'crop' => 'crop',
			'crop-16-9' => 'crop-16-9',
			'crop-3-2' => 'crop-3-2',
			'crop-5-4' => 'crop-5-4',
			'crop-7-5' => 'crop-7-5',
			'crop-din' => 'crop-din',
			'crop-free' => 'crop-free',
			'crop-landscape' => 'crop-landscape',
			'crop-original' => 'crop-original',
			'crop-portrait' => 'crop-portrait',
			'crop-square' => 'crop-square',
			'dashboard' => 'dashboard',
			'data-usage' => 'data-usage',
			'dehaze' => 'dehaze',
			'delete' => 'delete',
			'description' => 'description',
			'desktop-mac' => 'desktop-mac',
			'desktop-windows' => 'desktop-windows',
			'details' => 'details',
			'developer-board' => 'developer-board',
			'developer-mode' => 'developer-mode',
			'device-hub' => 'device-hub',
			'devices' => 'devices',
			'dialer-sip' => 'dialer-sip',
			'dialpad' => 'dialpad',
			'directions' => 'directions',
			'directions-bike' => 'directions-bike',
			'directions-boat' => 'directions-boat',
			'directions-bus' => 'directions-bus',
			'directions-car' => 'directions-car',
			'directions-railway' => 'directions-railway',
			'directions-run' => 'directions-run',
			'directions-subway' => 'directions-subway',
			'directions-transit' => 'directions-transit',
			'directions-walk' => 'directions-walk',
			'disc-full' => 'disc-full',
			'dns' => 'dns',
			'do-not-disturb' => 'do-not-disturb',
			'do-not-disturb-alt' => 'do-not-disturb-alt',
			'dock' => 'dock',
			'domain' => 'domain',
			'done' => 'done',
			'done-all' => 'done-all',
			'drafts' => 'drafts',
			'drive-eta' => 'drive-eta',
			'dvr' => 'dvr',
			'edit' => 'edit',
			'eject' => 'eject',
			'email' => 'email',
			'equalizer' => 'equalizer',
			'error' => 'error',
			'error-outline' => 'error-outline',
			'event' => 'event',
			'event-available' => 'event-available',
			'event-busy' => 'event-busy',
			'event-note' => 'event-note',
			'event-seat' => 'event-seat',
			'exit-to-app' => 'exit-to-app',
			'expand-less' => 'expand-less',
			'expand-more' => 'expand-more',
			'explicit' => 'explicit',
			'explore' => 'explore',
			'exposure' => 'exposure',
			'exposure-neg-1' => 'exposure-neg-1',
			'exposure-neg-2' => 'exposure-neg-2',
			'exposure-plus-1' => 'exposure-plus-1',
			'exposure-plus-2' => 'exposure-plus-2',
			'exposure-zero' => 'exposure-zero',
			'extension' => 'extension',
			'face' => 'face',
			'fast-forward' => 'fast-forward',
			'fast-rewind' => 'fast-rewind',
			'favorite' => 'favorite',
			'favorite-border' => 'favorite-border',
			'feedback' => 'feedback',
			'file-download' => 'file-download',
			'file-upload' => 'file-upload',
			'filter' => 'filter',
			'filter-1' => 'filter-1',
			'filter-2' => 'filter-2',
			'filter-3' => 'filter-3',
			'filter-4' => 'filter-4',
			'filter-5' => 'filter-5',
			'filter-6' => 'filter-6',
			'filter-7' => 'filter-7',
			'filter-8' => 'filter-8',
			'filter-9' => 'filter-9',
			'filter-9-plus' => 'filter-9-plus',
			'filter-b-and-w' => 'filter-b-and-w',
			'filter-center-focus' => 'filter-center-focus',
			'filter-drama' => 'filter-drama',
			'filter-frames' => 'filter-frames',
			'filter-hdr' => 'filter-hdr',
			'filter-list' => 'filter-list',
			'filter-none' => 'filter-none',
			'filter-tilt-shift' => 'filter-tilt-shift',
			'filter-vintage' => 'filter-vintage',
			'find-in-page' => 'find-in-page',
			'find-replace' => 'find-replace',
			'flag' => 'flag',
			'flare' => 'flare',
			'flash-auto' => 'flash-auto',
			'flash-off' => 'flash-off',
			'flash-on' => 'flash-on',
			'flight' => 'flight',
			'flight-land' => 'flight-land',
			'flight-takeoff' => 'flight-takeoff',
			'flip' => 'flip',
			'flip-to-back' => 'flip-to-back',
			'flip-to-front' => 'flip-to-front',
			'folder' => 'folder',
			'folder-open' => 'folder-open',
			'folder-shared' => 'folder-shared',
			'folder-special' => 'folder-special',
			'font-download' => 'font-download',
			'format-align-center' => 'format-align-center',
			'format-align-justify' => 'format-align-justify',
			'format-align-left' => 'format-align-left',
			'format-align-right' => 'format-align-right',
			'format-bold' => 'format-bold',
			'format-clear' => 'format-clear',
			'format-color-fill' => 'format-color-fill',
			'format-color-reset' => 'format-color-reset',
			'format-color-text' => 'format-color-text',
			'format-indent-decrease' => 'format-indent-decrease',
			'format-indent-increase' => 'format-indent-increase',
			'format-italic' => 'format-italic',
			'format-line-spacing' => 'format-line-spacing',
			'format-list-bulleted' => 'format-list-bulleted',
			'format-list-numbered' => 'format-list-numbered',
			'format-paint' => 'format-paint',
			'format-quote' => 'format-quote',
			'format-size' => 'format-size',
			'format-strikethrough' => 'format-strikethrough',
			'format-textdirection-l-to-r' => 'format-textdirection-l-to-r',
			'format-textdirection-r-to-l' => 'format-textdirection-r-to-l',
			'format-underlined' => 'format-underlined',
			'forum' => 'forum',
			'forward' => 'forward',
			'forward-10' => 'forward-10',
			'forward-30' => 'forward-30',
			'forward-5' => 'forward-5',
			'fullscreen' => 'fullscreen',
			'fullscreen-exit' => 'fullscreen-exit',
			'functions' => 'functions',
			'gamepad' => 'gamepad',
			'games' => 'games',
			'gesture' => 'gesture',
			'get-app' => 'get-app',
			'gif' => 'gif',
			'gps-fixed' => 'gps-fixed',
			'gps-not-fixed' => 'gps-not-fixed',
			'gps-off' => 'gps-off',
			'grade' => 'grade',
			'gradient' => 'gradient',
			'grain' => 'grain',
			'graphic-eq' => 'graphic-eq',
			'grid-off' => 'grid-off',
			'grid-on' => 'grid-on',
			'group' => 'group',
			'group-add' => 'group-add',
			'group-work' => 'group-work',
			'hd' => 'hd',
			'hdr-off' => 'hdr-off',
			'hdr-on' => 'hdr-on',
			'hdr-strong' => 'hdr-strong',
			'hdr-weak' => 'hdr-weak',
			'headset' => 'headset',
			'headset-mic' => 'headset-mic',
			'healing' => 'healing',
			'hearing' => 'hearing',
			'help' => 'help',
			'help-outline' => 'help-outline',
			'high-quality' => 'high-quality',
			'highlight-off' => 'highlight-off',
			'history' => 'history',
			'home' => 'home',
			'hotel' => 'hotel',
			'hourglass-empty' => 'hourglass-empty',
			'hourglass-full' => 'hourglass-full',
			'http' => 'http',
			'https' => 'https',
			'image' => 'image',
			'image-aspect-ratio' => 'image-aspect-ratio',
			'import-export' => 'import-export',
			'inbox' => 'inbox',
			'indeterminate-check-box' => 'indeterminate-check-box',
			'info' => 'info',
			'info-outline' => 'info-outline',
			'input' => 'input',
			'insert-chart' => 'insert-chart',
			'insert-comment' => 'insert-comment',
			'insert-drive-file' => 'insert-drive-file',
			'insert-emoticon' => 'insert-emoticon',
			'insert-invitation' => 'insert-invitation',
			'insert-link' => 'insert-link',
			'insert-photo' => 'insert-photo',
			'invert-colors' => 'invert-colors',
			'invert-colors-off' => 'invert-colors-off',
			'iso' => 'iso',
			'keyboard' => 'keyboard',
			'keyboard-arrow-down' => 'keyboard-arrow-down',
			'keyboard-arrow-left' => 'keyboard-arrow-left',
			'keyboard-arrow-right' => 'keyboard-arrow-right',
			'keyboard-arrow-up' => 'keyboard-arrow-up',
			'keyboard-backspace' => 'keyboard-backspace',
			'keyboard-capslock' => 'keyboard-capslock',
			'keyboard-hide' => 'keyboard-hide',
			'keyboard-return' => 'keyboard-return',
			'keyboard-tab' => 'keyboard-tab',
			'keyboard-voice' => 'keyboard-voice',
			'label' => 'label',
			'label-outline' => 'label-outline',
			'landscape' => 'landscape',
			'language' => 'language',
			'laptop' => 'laptop',
			'laptop-chromebook' => 'laptop-chromebook',
			'laptop-mac' => 'laptop-mac',
			'laptop-windows' => 'laptop-windows',
			'launch' => 'launch',
			'layers' => 'layers',
			'layers-clear' => 'layers-clear',
			'leak-add' => 'leak-add',
			'leak-remove' => 'leak-remove',
			'lens' => 'lens',
			'library-add' => 'library-add',
			'library-books' => 'library-books',
			'library-music' => 'library-music',
			'link' => 'link',
			'list' => 'list',
			'live-help' => 'live-help',
			'live-tv' => 'live-tv',
			'local-activity' => 'local-activity',
			'local-airport' => 'local-airport',
			'local-atm' => 'local-atm',
			'local-bar' => 'local-bar',
			'local-cafe' => 'local-cafe',
			'local-car-wash' => 'local-car-wash',
			'local-convenience-store' => 'local-convenience-store',
			'local-dining' => 'local-dining',
			'local-drink' => 'local-drink',
			'local-florist' => 'local-florist',
			'local-gas-station' => 'local-gas-station',
			'local-grocery-store' => 'local-grocery-store',
			'local-hospital' => 'local-hospital',
			'local-hotel' => 'local-hotel',
			'local-laundry-service' => 'local-laundry-service',
			'local-library' => 'local-library',
			'local-mall' => 'local-mall',
			'local-movies' => 'local-movies',
			'local-offer' => 'local-offer',
			'local-parking' => 'local-parking',
			'local-pharmacy' => 'local-pharmacy',
			'local-phone' => 'local-phone',
			'local-pizza' => 'local-pizza',
			'local-play' => 'local-play',
			'local-post-office' => 'local-post-office',
			'local-printshop' => 'local-printshop',
			'local-see' => 'local-see',
			'local-shipping' => 'local-shipping',
			'local-taxi' => 'local-taxi',
			'location-city' => 'location-city',
			'location-disabled' => 'location-disabled',
			'location-off' => 'location-off',
			'location-on' => 'location-on',
			'location-searching' => 'location-searching',
			'lock' => 'lock',
			'lock-open' => 'lock-open',
			'lock-outline' => 'lock-outline',
			'looks' => 'looks',
			'looks-3' => 'looks-3',
			'looks-4' => 'looks-4',
			'looks-5' => 'looks-5',
			'looks-6' => 'looks-6',
			'looks-one' => 'looks-one',
			'looks-two' => 'looks-two',
			'loop' => 'loop',
			'loupe' => 'loupe',
			'loyalty' => 'loyalty',
			'mail' => 'mail',
			'map' => 'map',
			'markunread' => 'markunread',
			'markunread-mailbox' => 'markunread-mailbox',
			'memory' => 'memory',
			'menu' => 'menu',
			'merge-type' => 'merge-type',
			'message' => 'message',
			'mic' => 'mic',
			'mic-none' => 'mic-none',
			'mic-off' => 'mic-off',
			'mms' => 'mms',
			'mode-comment' => 'mode-comment',
			'mode-edit' => 'mode-edit',
			'money-off' => 'money-off',
			'monochrome-photos' => 'monochrome-photos',
			'mood' => 'mood',
			'mood-bad' => 'mood-bad',
			'more' => 'more',
			'more-horiz' => 'more-horiz',
			'more-vert' => 'more-vert',
			'mouse' => 'mouse',
			'movie' => 'movie',
			'movie-creation' => 'movie-creation',
			'music-note' => 'music-note',
			'my-location' => 'my-location',
			'nature' => 'nature',
			'nature-people' => 'nature-people',
			'navigate-before' => 'navigate-before',
			'navigate-next' => 'navigate-next',
			'navigation' => 'navigation',
			'network-cell' => 'network-cell',
			'network-locked' => 'network-locked',
			'network-wifi' => 'network-wifi',
			'new-releases' => 'new-releases',
			'nfc' => 'nfc',
			'no-sim' => 'no-sim',
			'not-interested' => 'not-interested',
			'note-add' => 'note-add',
			'notifications' => 'notifications',
			'notifications-active' => 'notifications-active',
			'notifications-none' => 'notifications-none',
			'notifications-off' => 'notifications-off',
			'notifications-paused' => 'notifications-paused',
			'offline-pin' => 'offline-pin',
			'ondemand-video' => 'ondemand-video',
			'open-in-browser' => 'open-in-browser',
			'open-in-new' => 'open-in-new',
			'open-with' => 'open-with',
			'pages' => 'pages',
			'pageview' => 'pageview',
			'palette' => 'palette',
			'panorama' => 'panorama',
			'panorama-fish-eye' => 'panorama-fish-eye',
			'panorama-horizontal' => 'panorama-horizontal',
			'panorama-vertical' => 'panorama-vertical',
			'panorama-wide-angle' => 'panorama-wide-angle',
			'party-mode' => 'party-mode',
			'pause' => 'pause',
			'pause-circle-filled' => 'pause-circle-filled',
			'pause-circle-outline' => 'pause-circle-outline',
			'payment' => 'payment',
			'people' => 'people',
			'people-outline' => 'people-outline',
			'perm-camera-mic' => 'perm-camera-mic',
			'perm-contact-calendar' => 'perm-contact-calendar',
			'perm-data-setting' => 'perm-data-setting',
			'perm-device-information' => 'perm-device-information',
			'perm-identity' => 'perm-identity',
			'perm-media' => 'perm-media',
			'perm-phone-msg' => 'perm-phone-msg',
			'perm-scan-wifi' => 'perm-scan-wifi',
			'person' => 'person',
			'person-add' => 'person-add',
			'person-outline' => 'person-outline',
			'person-pin' => 'person-pin',
			'personal-video' => 'personal-video',
			'phone' => 'phone',
			'phone-android' => 'phone-android',
			'phone-bluetooth-speaker' => 'phone-bluetooth-speaker',
			'phone-forwarded' => 'phone-forwarded',
			'phone-in-talk' => 'phone-in-talk',
			'phone-iphone' => 'phone-iphone',
			'phone-locked' => 'phone-locked',
			'phone-missed' => 'phone-missed',
			'phone-paused' => 'phone-paused',
			'phonelink' => 'phonelink',
			'phonelink-erase' => 'phonelink-erase',
			'phonelink-lock' => 'phonelink-lock',
			'phonelink-off' => 'phonelink-off',
			'phonelink-ring' => 'phonelink-ring',
			'phonelink-setup' => 'phonelink-setup',
			'photo' => 'photo',
			'photo-album' => 'photo-album',
			'photo-camera' => 'photo-camera',
			'photo-library' => 'photo-library',
			'photo-size-select-actual' => 'photo-size-select-actual',
			'photo-size-select-large' => 'photo-size-select-large',
			'photo-size-select-small' => 'photo-size-select-small',
			'picture-as-pdf' => 'picture-as-pdf',
			'picture-in-picture' => 'picture-in-picture',
			'pin-drop' => 'pin-drop',
			'place' => 'place',
			'play-arrow' => 'play-arrow',
			'play-circle-filled' => 'play-circle-filled',
			'play-circle-outline' => 'play-circle-outline',
			'play-for-work' => 'play-for-work',
			'playlist-add' => 'playlist-add',
			'plus-one' => 'plus-one',
			'poll' => 'poll',
			'polymer' => 'polymer',
			'portable-wifi-off' => 'portable-wifi-off',
			'portrait' => 'portrait',
			'power' => 'power',
			'power-input' => 'power-input',
			'power-settings-new' => 'power-settings-new',
			'present-to-all' => 'present-to-all',
			'print' => 'print',
			'public' => 'public',
			'publish' => 'publish',
			'query-builder' => 'query-builder',
			'question-answer' => 'question-answer',
			'queue' => 'queue',
			'queue-music' => 'queue-music',
			'radio' => 'radio',
			'radio-button-checked' => 'radio-button-checked',
			'radio-button-unchecked' => 'radio-button-unchecked',
			'rate-review' => 'rate-review',
			'receipt' => 'receipt',
			'recent-actors' => 'recent-actors',
			'redeem' => 'redeem',
			'redo' => 'redo',
			'refresh' => 'refresh',
			'remove' => 'remove',
			'remove-circle' => 'remove-circle',
			'remove-circle-outline' => 'remove-circle-outline',
			'remove-red-eye' => 'remove-red-eye',
			'reorder' => 'reorder',
			'repeat' => 'repeat',
			'repeat-one' => 'repeat-one',
			'replay' => 'replay',
			'replay-10' => 'replay-10',
			'replay-30' => 'replay-30',
			'replay-5' => 'replay-5',
			'reply' => 'reply',
			'reply-all' => 'reply-all',
			'report' => 'report',
			'report-problem' => 'report-problem',
			'restaurant-menu' => 'restaurant-menu',
			'restore' => 'restore',
			'ring-volume' => 'ring-volume',
			'room' => 'room',
			'rotate-90-degrees-ccw' => 'rotate-90-degrees-ccw',
			'rotate-left' => 'rotate-left',
			'rotate-right' => 'rotate-right',
			'router' => 'router',
			'satellite' => 'satellite',
			'save' => 'save',
			'scanner' => 'scanner',
			'schedule' => 'schedule',
			'school' => 'school',
			'screen-lock-landscape' => 'screen-lock-landscape',
			'screen-lock-portrait' => 'screen-lock-portrait',
			'screen-lock-rotation' => 'screen-lock-rotation',
			'screen-rotation' => 'screen-rotation',
			'sd-card' => 'sd-card',
			'sd-storage' => 'sd-storage',
			'search' => 'search',
			'security' => 'security',
			'select-all' => 'select-all',
			'send' => 'send',
			'settings' => 'settings',
			'settings-applications' => 'settings-applications',
			'settings-backup-restore' => 'settings-backup-restore',
			'settings-bluetooth' => 'settings-bluetooth',
			'settings-brightness' => 'settings-brightness',
			'settings-cell' => 'settings-cell',
			'settings-ethernet' => 'settings-ethernet',
			'settings-input-antenna' => 'settings-input-antenna',
			'settings-input-component' => 'settings-input-component',
			'settings-input-composite' => 'settings-input-composite',
			'settings-input-hdmi' => 'settings-input-hdmi',
			'settings-input-svideo' => 'settings-input-svideo',
			'settings-overscan' => 'settings-overscan',
			'settings-phone' => 'settings-phone',
			'settings-power' => 'settings-power',
			'settings-remote' => 'settings-remote',
			'settings-system-daydream' => 'settings-system-daydream',
			'settings-voice' => 'settings-voice',
			'share' => 'share',
			'shop' => 'shop',
			'shop-two' => 'shop-two',
			'shopping-basket' => 'shopping-basket',
			'shopping-cart' => 'shopping-cart',
			'shuffle' => 'shuffle',
			'signal-cellular-4-bar' => 'signal-cellular-4-bar',
			'signal-cellular-connected-no-internet-4-bar' => 'signal-cellular-connected-no-internet-4-bar',
			'signal-cellular-no-sim' => 'signal-cellular-no-sim',
			'signal-cellular-null' => 'signal-cellular-null',
			'signal-cellular-off' => 'signal-cellular-off',
			'signal-wifi-4-bar' => 'signal-wifi-4-bar',
			'signal-wifi-4-bar-lock' => 'signal-wifi-4-bar-lock',
			'signal-wifi-off' => 'signal-wifi-off',
			'sim-card' => 'sim-card',
			'sim-card-alert' => 'sim-card-alert',
			'skip-next' => 'skip-next',
			'skip-previous' => 'skip-previous',
			'slideshow' => 'slideshow',
			'smartphone' => 'smartphone',
			'sms' => 'sms',
			'sms-failed' => 'sms-failed',
			'snooze' => 'snooze',
			'sort' => 'sort',
			'sort-by-alpha' => 'sort-by-alpha',
			'space-bar' => 'space-bar',
			'speaker' => 'speaker',
			'speaker-group' => 'speaker-group',
			'speaker-notes' => 'speaker-notes',
			'speaker-phone' => 'speaker-phone',
			'spellcheck' => 'spellcheck',
			'star' => 'star',
			'star-border' => 'star-border',
			'star-half' => 'star-half',
			'stars' => 'stars',
			'stay-current-landscape' => 'stay-current-landscape',
			'stay-current-portrait' => 'stay-current-portrait',
			'stay-primary-landscape' => 'stay-primary-landscape',
			'stay-primary-portrait' => 'stay-primary-portrait',
			'stop' => 'stop',
			'storage' => 'storage',
			'store' => 'store',
			'store-mall-directory' => 'store-mall-directory',
			'straighten' => 'straighten',
			'strikethrough-s' => 'strikethrough-s',
			'style' => 'style',
			'subject' => 'subject',
			'subtitles' => 'subtitles',
			'supervisor-account' => 'supervisor-account',
			'surround-sound' => 'surround-sound',
			'swap-calls' => 'swap-calls',
			'swap-horiz' => 'swap-horiz',
			'swap-vert' => 'swap-vert',
			'swap-vertical-circle' => 'swap-vertical-circle',
			'switch-camera' => 'switch-camera',
			'switch-video' => 'switch-video',
			'sync' => 'sync',
			'sync-disabled' => 'sync-disabled',
			'sync-problem' => 'sync-problem',
			'system-update' => 'system-update',
			'system-update-alt' => 'system-update-alt',
			'tab' => 'tab',
			'tab-unselected' => 'tab-unselected',
			'tablet' => 'tablet',
			'tablet-android' => 'tablet-android',
			'tablet-mac' => 'tablet-mac',
			'tag-faces' => 'tag-faces',
			'tap-and-play' => 'tap-and-play',
			'terrain' => 'terrain',
			'text-format' => 'text-format',
			'textsms' => 'textsms',
			'texture' => 'texture',
			'theaters' => 'theaters',
			'thumb-down' => 'thumb-down',
			'thumb-up' => 'thumb-up',
			'thumbs-up-down' => 'thumbs-up-down',
			'time-to-leave' => 'time-to-leave',
			'timelapse' => 'timelapse',
			'timer' => 'timer',
			'timer-10' => 'timer-10',
			'timer-3' => 'timer-3',
			'timer-off' => 'timer-off',
			'toc' => 'toc',
			'today' => 'today',
			'toll' => 'toll',
			'tonality' => 'tonality',
			'toys' => 'toys',
			'track-changes' => 'track-changes',
			'traffic' => 'traffic',
			'transform' => 'transform',
			'translate' => 'translate',
			'trending-down' => 'trending-down',
			'trending-flat' => 'trending-flat',
			'trending-up' => 'trending-up',
			'tune' => 'tune',
			'turned-in' => 'turned-in',
			'turned-in-not' => 'turned-in-not',
			'tv' => 'tv',
			'undo' => 'undo',
			'unfold-less' => 'unfold-less',
			'unfold-more' => 'unfold-more',
			'usb' => 'usb',
			'verified-user' => 'verified-user',
			'vertical-align-bottom' => 'vertical-align-bottom',
			'vertical-align-center' => 'vertical-align-center',
			'vertical-align-top' => 'vertical-align-top',
			'vibration' => 'vibration',
			'video-library' => 'video-library',
			'videocam' => 'videocam',
			'videocam-off' => 'videocam-off',
			'view-agenda' => 'view-agenda',
			'view-array' => 'view-array',
			'view-carousel' => 'view-carousel',
			'view-column' => 'view-column',
			'view-comfy' => 'view-comfy',
			'view-compact' => 'view-compact',
			'view-day' => 'view-day',
			'view-headline' => 'view-headline',
			'view-list' => 'view-list',
			'view-module' => 'view-module',
			'view-quilt' => 'view-quilt',
			'view-stream' => 'view-stream',
			'view-week' => 'view-week',
			'vignette' => 'vignette',
			'visibility' => 'visibility',
			'visibility-off' => 'visibility-off',
			'voice-chat' => 'voice-chat',
			'voicemail' => 'voicemail',
			'volume-down' => 'volume-down',
			'volume-mute' => 'volume-mute',
			'volume-off' => 'volume-off',
			'volume-up' => 'volume-up',
			'vpn-key' => 'vpn-key',
			'vpn-lock' => 'vpn-lock',
			'wallpaper' => 'wallpaper',
			'warning' => 'warning',
			'watch' => 'watch',
			'wb-auto' => 'wb-auto',
			'wb-cloudy' => 'wb-cloudy',
			'wb-incandescent' => 'wb-incandescent',
			'wb-iridescent' => 'wb-iridescent',
			'wb-sunny' => 'wb-sunny',
			'wc' => 'wc',
			'web' => 'web',
			'whatshot' => 'whatshot',
			'widgets' => 'widgets',
			'wifi' => 'wifi',
			'wifi-lock' => 'wifi-lock',
			'wifi-tethering' => 'wifi-tethering',
			'work' => 'work',
			'wrap-text' => 'wrap-text',
			'youtube-searched-for' => 'youtube-searched-for',
			'zoom-in' => 'zoom-in',
			'zoom-out' => 'zoom-out',
		),
	) );

}