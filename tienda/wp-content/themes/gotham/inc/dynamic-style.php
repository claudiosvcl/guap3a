<?php
/** 
* dynamic styles (inline_style)
* Also this file is used to complet custom-css.php
*/

function gotham_dynamic_style() {

    wp_enqueue_style('gotham-dynamic-style', get_template_directory_uri() . '/css/dynamic-style.css');

    if ( ! is_404() ) {

        $title_area_parallax = esc_url( get_post_meta( get_the_ID(), 'gotham_title_area_parallax', true ) );
        $title_area_background_image = esc_url( get_post_meta( get_the_ID(), 'gotham_title_area_background_image', true ) );
        $select_sidebar = get_post_meta( get_the_ID(), 'gotham_select_sidebar', true );
        
        $nextPost = get_next_post();
        if ( $nextPost != '' ) {
          $next_title_color = get_post_meta( $nextPost->ID, 'gotham_title_area_color', true );
          $next_description_color = get_post_meta( $nextPost->ID, 'gotham_title_area_color', true );
          $next_description = get_post_meta( $nextPost->ID, 'gotham_select_description', true );
          $nextportfoliobgimagep = esc_url( get_post_meta( $nextPost->ID, 'gotham_title_area_parallax', true ) );
          $nextportfoliobgimage = esc_url( get_post_meta( $nextPost->ID, 'gotham_title_area_background_image', true ) );
          $nextportfoliobgcolor = get_post_meta( $nextPost->ID, 'gotham_title_area_background_color', true );
          $next_portfoliotcolor = get_post_meta( $nextPost->ID, 'gotham_title_area_color', true );
        }

        $prevPost = get_previous_post();
        if ( $prevPost != '' ) {
          $prevportfoliobgimagep = esc_url( get_post_meta( $prevPost->ID, 'gotham_title_area_parallax', true ) );
          $prevportfoliobgimage = esc_url( get_post_meta( $prevPost->ID, 'gotham_title_area_background_image', true ) );
          $prevportfoliobgcolor = get_post_meta( $prevPost->ID, 'gotham_title_area_background_color', true );
          $prev_portfoliotcolor = get_post_meta( $prevPost->ID, 'gotham_title_area_color', true );
        }  

        $tags = get_the_tags();
        $crthumbnail = '';

        if (has_post_thumbnail()) {
          $crthumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
        }

        $mfontf = get_option('main_font_family');
        $sfontf = get_option('secondary_font_family');

        if ( class_exists( 'WooCommerce' ) ) {
          if(is_shop()||is_product_category()||is_product_tag()){
            $woogetpid = wc_get_page_id('shop');
          } else {
            $woogetpid = get_the_ID();
          }
          $woo_title_area_parallax = esc_url( get_post_meta( $woogetpid, 'gotham_title_area_parallax', true ) );
          $woo_title_area_background_image = esc_url( get_post_meta( $woogetpid, 'gotham_title_area_background_image', true ) );
          $woo_select_sidebar = get_post_meta( $woogetpid, 'gotham_select_sidebar', true );
        }
        else {
          $woo_title_area_parallax = '';
          $woo_title_area_background_image = '';
          $woo_select_sidebar = '';
        }

        $css_metabox = "";

        if ( ( $title_area_parallax != '' )&&(false == get_post_format()) or ( $title_area_parallax != '' )&&(false != get_post_format()) or ( $title_area_parallax != '' )&&(is_singular('portfolio')) ) {

            $css_metabox .= '.h-below:not(.hbidar), .single-screen{';

              $css_metabox .= 'background-image: url('.$title_area_parallax.'); -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; background-repeat: no-repeat; background-color: transparent;';
        
            $css_metabox .= '}';
        }

        /* if customizer live preview */
         if ( is_customize_preview() ) {
          if(get_option('main_font_family') != "") {
            if(strpos(get_option('main_font_family'), " ") !== false) {
              $css_metabox .= 'body{font-family:"'.$mfontf.'" !important;}';
            }
            else {
              $css_metabox .= 'body{font-family:'.$mfontf.' !important;}';
            }
          }
        }

        if ( is_customize_preview() ) {
          if(get_option('secondary_font_family') != "") {
            if(strpos(get_option('secondary_font_family'), " ") !== false) {
              $css_metabox .= '#content .blog-item.full .wrap-icon-text .text_citation p:nth-child(2), 
                                #content>article>.post-content, .blog-big-item .blog-thumb .date a, .blog-big-item .text_citation p:nth-child(2), 
                                .blog-big-item .text_link span, .blog-thumb .excerpt a, .comment-list .comment-text p, 
                                .copyright, .ct_overlay p, .map-information p, 
                                .mwlw > a, .mwlw p, .pagination_after span, 
                                .pagination_prev span, .pclient>h3, .pclient>p, .pdate>p, 
                                .portfolio-category>p, .portfolio-content>p, .portfolio-item .shoot-e .category, 
                                .ppcatp, .s-author-desc, .search_layout .type-page a, 
                                .single-content .text_citation p:nth-child(2), .single-description, .single-h1 span, 
                                .team-info .team-content, .thumb .category a, .title-wr .subtitle,
                                .widget_random_entries .rpwrp_wrap .rptd .rdpcat, 
                                .widget_recent_entries .post-date, .widget_text p, 
                                .wrap-flex .process p, .wrsl-flex .slide-wrap p, 
                                li.blog-item .blog-thumb .date, 
                                li.blog-item .text_link span, #comment, .comment-form-comment textarea,
                                .sh-bgcaption .sh-desc, .dsptbb .category, .wooexcerpt,
                                .woocommerce #reviews #comments ol.commentlist li .comment-text .description, .woocommerce-checkout #payment div.payment_box, .woocommerce-order-received .woocommerce p:not(.woocommerce-thankyou-order-received), .woocommerce-order-received address, .woocommerce-account address, .search-results .woosresult p, .woocommerce .track_order p:first-child{font-family:"'.$sfontf.'" !important;}';
            }
            else {
              $css_metabox .= '#content .blog-item.full .wrap-icon-text .text_citation p:nth-child(2), 
                                #content>article>.post-content, .blog-big-item .blog-thumb .date a, .blog-big-item .text_citation p:nth-child(2), 
                                .blog-big-item .text_link span, .blog-thumb .excerpt a, .comment-list .comment-text p, 
                                .copyright, .ct_overlay p, .map-information p, 
                                .mwlw > a, .mwlw p, .pagination_after span, 
                                .pagination_prev span, .pclient>h3, .pclient>p, .pdate>p, 
                                .portfolio-category>p, .portfolio-content>p, .portfolio-item .shoot-e .category, 
                                .ppcatp, .s-author-desc, .search_layout .type-page a, 
                                .single-content .text_citation p:nth-child(2), .single-description, .single-h1 span, 
                                .team-info .team-content, .thumb .category a, .title-wr .subtitle,
                                .widget_random_entries .rpwrp_wrap .rptd .rdpcat, 
                                .widget_recent_entries .post-date, .widget_text p, 
                                .wrap-flex .process p, .wrsl-flex .slide-wrap p, 
                                li.blog-item .blog-thumb .date, 
                                li.blog-item .text_link span, #comment, .comment-form-comment textarea,
                                .sh-bgcaption .sh-desc, .dsptbb .category, .wooexcerpt,
                                .woocommerce #reviews #comments ol.commentlist li .comment-text .description, .woocommerce-checkout #payment div.payment_box, .woocommerce-order-received .woocommerce p:not(.woocommerce-thankyou-order-received), .woocommerce-order-received address, .woocommerce-account address, .search-results .woosresult p, .woocommerce .track_order p:first-child{font-family:'.$sfontf.' !important;}';
            }
          }
        }

        /* primary menu + search + sidebar */
        if ((has_nav_menu('primary'))&&(get_theme_mod('mgsearch') != 'no')&&($woo_select_sidebar != '')){
          $css_metabox .= '
          @media screen and (min-width: 700px) and (max-width: 991px) {
            body.woocommerce .header-cart, body .header-cart{right:134px;}
          }
          @media only screen and (max-width: 699px) {
            body.woocommerce .header-cart, body .header-cart{right:126.5px;}
          }';
        }

        /* no primary menu */
        if (!has_nav_menu('primary')) {
          $css_metabox .= '
            .h-wrap #searchoverlay{right:31px;}

            body.woocommerce .header-cart, body .header-cart{right:71px;}

            body.woocommerce .header-cart .woocommerce.widget_shopping_cart, body .header-cart .woocommerce.widget_shopping_cart{
              margin-right: -35px;
            }

            body.woocommerce .header-cart .woocommerce.widget_shopping_cart:before, body .header-cart .woocommerce.widget_shopping_cart:before{
              margin-left:231px;
            }

            body.woocommerce .header-cart .woocommerce.widget_shopping_cart:after, body .header-cart .woocommerce.widget_shopping_cart:after{
              margin-left: 231.5px;
            }

            @media screen and (min-width: 700px) and (max-width: 991px) {
              .h-wrap #searchoverlay{right:21px;}
              .column70-30 > .inner2 .more-sdbmb:before, .column70-30 > .inner2_left .more-sdbmb:before{right:60px;}
              body.woocommerce .header-cart, body .header-cart{right:55px;}
            }

            @media only screen and (max-width: 699px) {
              .h-wrap #searchoverlay{right:13px;}
              .column70-30 > .inner2 .more-sdbmb:before, .column70-30 > .inner2_left .more-sdbmb:before{right:52px;}
              body.woocommerce .header-cart, body .header-cart{right:47px;}
            }';
        }

        /* no primary menu + sidebar */
        if ((!has_nav_menu('primary'))&&($woo_select_sidebar != '')) {
          $css_metabox .= '
          @media screen and (min-width: 700px) and (max-width: 991px) {
            body.woocommerce .header-cart, body .header-cart{right:88px;}
          }
          @media only screen and (max-width: 699px) {
            body.woocommerce .header-cart, body .header-cart{right:80.5px;}
          }';
        }

        /* no primary menu + no search */
        if ((!has_nav_menu('primary'))&&(get_theme_mod('mgsearch') == 'no')) {
          $css_metabox .= '
          body.woocommerce .header-cart, body .header-cart{right:24px;}
          
          body.woocommerce .header-cart .woocommerce.widget_shopping_cart, body .header-cart .woocommerce.widget_shopping_cart {
              margin-right: 12.5px;
          }

          body.woocommerce .header-cart .woocommerce.widget_shopping_cart:before, body .header-cart .woocommerce.widget_shopping_cart:before {
            margin-left:277.5px;
          }

          body.woocommerce .header-cart .woocommerce.widget_shopping_cart:after, body .header-cart .woocommerce.widget_shopping_cart:after {
            margin-left: 278.5px;
          }

          @media only screen and (max-width: 991px) {
            .column70-30 > .inner2 .more-sdbmb:before, .column70-30 > .inner2_left .more-sdbmb:before{right: 21px;}
            body.woocommerce .header-cart, body .header-cart{right:15px;}
          }
          @media only screen and (max-width: 699px) {
            .column70-30 > .inner2 .more-sdbmb:before, .column70-30 > .inner2_left .more-sdbmb:before{right: 12px;}
            body.woocommerce .header-cart, body .header-cart{right:7px;}
          }';
        }

        /* no primary menu + no search + sidebar */
        if ((!has_nav_menu('primary'))&&(get_theme_mod('mgsearch') == 'no')&&($woo_select_sidebar != '')) {
          $css_metabox .= '
          @media screen and (min-width: 700px) and (max-width: 991px) {
            body.woocommerce .header-cart, body .header-cart{right:49px;}
          }
          @media only screen and (max-width: 699px) {
            body.woocommerce .header-cart, body .header-cart{right:40.5px;}
          }';
        }

        /* primary menu + no search + sidebar */
        if((has_nav_menu('primary'))&&(get_theme_mod('mgsearch') == 'no')&&($woo_select_sidebar != '')) {
          $css_metabox .= '
          @media screen and (min-width: 700px) and (max-width: 991px) {
            body.woocommerce .header-cart, body .header-cart{right:94px;}
          }
          @media only screen and (max-width: 699px) {
            body.woocommerce .header-cart, body .header-cart{right:86.5px;}
          }';
        }
       
        if ( ($title_area_parallax == '')&&($title_area_background_image != '')&&(false == get_post_format()) or ($title_area_parallax == '')&&($title_area_background_image != '')&&(false != get_post_format()) or ($title_area_parallax == '')&&($title_area_background_image != '')&&(is_singular('portfolio')) ) {

            $css_metabox .= '.h-below:not(.hbidar), .single-screen{';

              $css_metabox .= 'background-image: url('.$title_area_background_image.'); -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; background-color: transparent; background-position: 50% 50%;';

            $css_metabox .= '}';
        }

        if ((is_singular('post'))&&($title_area_parallax == '')&&($title_area_background_image == '')) {
          $css_metabox .= '.single-bh.menu-visible, #content.menu-visible{
                            transform: translate3d(-33%, 0, 0);
                          }
                          @media screen and (min-width: 481px) and (max-width: 800px) {
                            .single-bh.menu-visible, #content.menu-visible{
                              transform: translate3d(-50%, 0, 0);
                            }
                          }

                          @media only screen and (max-width: 480px) {
                            .single-bh.menu-visible, #content.menu-visible{
                              transform: translate3d(-75%, 0, 0);
                            }
                          }';
        }

        if ( ( $title_area_parallax == '' )&&(false == get_post_format())&&($title_area_background_image == '') or ( $title_area_parallax == '' )&&(false != get_post_format())&&($title_area_background_image == '')) {

            $css_metabox .= '
            .single-bh {
              height: auto !important;
              padding: 100px 30px 0px 30px;
              max-width: 800px;
              margin: 0 auto;
              max-height: none;
            }
            .single-thumb {
              width: 100%;
              padding-left: 0;
            }
            .single-title {
              width: 100%;
            }
            .single-screen {
              position: static;
              padding: 0;
            } 
            .single-details {
              padding: 0;
              width: 100%;
            }
            .single-details .blog-thumb {
              padding-left:0;
              width: 100%;
            }
            .single-details .blog-thumb h3 {
              width: 100%;
              font-weight: 700 !important;
              letter-spacing:1px;
            }
            .single-details .single-description {
              width: 100%;
              padding-top: 30px;
              padding-bottom: 0;
            }
            .single-details .breadcrumbs {
              padding-top: 15px;
            }
            #content.single-content article {
              padding-top: 35px;
            }';
        }

         if (( $nextPost != '' )&&( $next_title_color != '#' )){

            $css_metabox .= '.single_next_post a, .single_next_post i.md-icon-navigate-next{';

              $css_metabox .= 'color: '.$next_title_color.' !important;';

            $css_metabox .= '}';

        }

        if (( $nextPost != '' )&&( $next_description_color != '' )&&( $next_description == 'select_description')){

          $css_metabox .= 'p.single-author span, p.single-author span:after, p.single-author strong{';

            $css_metabox .= 'color: '.$next_description_color.';';

          $css_metabox .= '}';

        }

        if (( $nextPost != '' )&&( $next_description == '')){

          $css_metabox .= '.single-author span, .single-author span:after, .single-author strong{';

            $css_metabox .= 'color: '.$next_title_color.' !important;';

          $css_metabox .= '}';

        }

        if(is_singular('portfolio')){

          if( $nextPost != "" ){

            if ($nextportfoliobgimagep != ""){

              $css_metabox .= '
              .wrpga {
                background-image: url('.$nextportfoliobgimagep.');
                background-position:50% 50%;
                -webkit-background-size: cover;
                background-size: cover;
              }';

            }

            if ($next_portfoliotcolor != ""){

              $css_metabox .= '
              .pagination_after:hover span {
                color:'.$next_portfoliotcolor.';
              }';  

            }

            $css_metabox .= '
            @media screen and (max-width: 50em) {
              .wrpga {
                background-image:none !important;
              }
            }';

          }

          if( $prevPost != "" ) {

            if ($prevportfoliobgimagep != ""){

              $css_metabox .= '
              .wrpgp {
                background-image: url('.$prevportfoliobgimagep.');
                background-position:50% 50%;
                -webkit-background-size: cover;
                background-size: cover;
              }';

            }

            if ($prev_portfoliotcolor != ""){

              $css_metabox .= '
              .pagination_prev:hover span {
                color:'.$prev_portfoliotcolor.';
              }';  

            }

            $css_metabox .= '
            @media screen and (max-width: 50em) {
              .wrpgp {
                background-image:none !important;
              }
            }';

          }

          if(( $prevPost == "" )&&( $nextPost != "" )) {
          
            $css_metabox .= '
            .wrpga {
              margin-left: 50%;
              float:none;
              width: 50%;
            }';
          
          }

          if(( $prevPost != "" )&&( $nextPost == "" )) {
          
            $css_metabox .= '
            .wrpgp {
              width: 50%;
            }';
          
          }

        }

        if(is_singular('portfolio')){

          if( $nextPost != "" ){

            if (($nextportfoliobgimagep == "")&&($nextportfoliobgimage != "")){

              $css_metabox .= '
              .wrpga {
                background-image: url('.$nextportfoliobgimage.');
                -webkit-background-size: cover;
                background-size: cover;
                background-position:50% 50%;
              }';

            }

          }

          if( $prevPost != "" ){

            if (($prevportfoliobgimagep == "")&&($prevportfoliobgimage != "")){

              $css_metabox .= '
              .wrpgp {
                background-image: url('.$prevportfoliobgimage.');
                -webkit-background-size: cover;
                background-size: cover;
                background-position:50% 50%;
              }';

            }

          }

        }

        if(is_singular('portfolio')){

          if( $nextPost != "" ){

            if (($nextportfoliobgimagep == "")&&($nextportfoliobgimage == "")&&($nextportfoliobgcolor != "")){

              $css_metabox .= '.wrpga{background: '.$nextportfoliobgcolor.';}';

            }

          }

          if( $prevPost != "" ){

            if (($prevportfoliobgimagep == "")&&($prevportfoliobgimage == "")&&($prevportfoliobgcolor != "")){

              $css_metabox .= '.wrpgp{background: '.$prevportfoliobgcolor.';}';

            }

          }

        }

        if (!empty($tags)) {
          $css_metabox .= '.post-like{float:left;}';
        }

        /* blog posts index page */
        if (is_home()) {
          $css_metabox .='
            .menu-trigger span:before, .menu-trigger span:after, .menu-trigger span {
              background:#000;
            }
            .more-sdbmb:before, .explore a:before, .h-wrap #searchoverlay #searchsubmit,
            .main-nav .uihmenu:before, .h-wrap #searchoverlay .uihsearch:before,
            .explore a:after, .uihpost:before {
              color: #000;
            }
            header.scroll-up {
              background: rgba(255, 255, 255, 0.95);
            }
          ';
        }

        /* for sidebar */
        
        /* more-sdbmb in blog posts index page */
        if (is_home()&&(is_active_sidebar( 'main' ))) {
         $css_metabox .= '@media only screen and (max-width: 991px) {
            #content:not(.menu-visible){transform:none !important;}
          }';
        }

        if ((is_home()&&(!is_active_sidebar( 'main' )))||(!is_home()&&($select_sidebar == ''))) {
          $css_metabox .= '@media only screen and (min-width: 1025px) {
          .blog-big-item .blog-thumb h3{margin-top: 118px;}
          .blog-big-item .blog-thumb.visible h3{margin-top:74px;}
          }';
        }

        if ( is_admin_bar_showing() ) {
          $css_metabox .= '
          @media screen and ( max-width: 991px ) {
            .widget-area.visible {
              margin-top:32px;
            } 
          }
          @media screen and ( max-width: 782px ) {
            .widget-area.visible {
              margin-top:46px;
            }
          }';
        }

        /* admin bar & menu */
        if ( is_admin_bar_showing() ) {
          $css_metabox .= '
          @media only screen and (min-width: 783px) {
            #icon {
              bottom: 32px;
            }

            .menu-wrap {
              height: calc(100% - 93px) !important;
            }
          }

          @media screen and (min-width: 481px)  and (max-width: 782px) {
              #icon {
                bottom: 46px;
              }

              .menu-wrap {
                height: calc(100% - 107px) !important;
              }
          }

          @media only screen and (max-width: 480px) {
            div#icon a {
              height: 60px;
            }
          }
          
          @media only screen and (max-width: 991px) {
            .column70-30>.inner2 .more-sdbmb:before, .column70-30>.inner2_left .more-sdbmb:before {
              top:63.5px;
            }
          }

          @media screen and (min-width: 783px)  and (max-width: 991px) {
            .column70-30>.inner2 .more-sdbmb:before, .column70-30>.inner2_left .more-sdbmb:before {
              top:49.5px;
            }
          }

          ';
        }

        /* header style dark in is_archive and is_search */
        if( (is_archive() && ( ! class_exists( 'WooCommerce' ) ) ) || ( is_archive() && (class_exists( 'WooCommerce' ) && ! is_woocommerce() ) )||(is_search()) ) {
          $css_metabox .= '.menu-trigger span:before, .menu-trigger span:after, .menu-trigger span{background:#202020;}.more-sdbmb:before, .explore a:before{color:#202020;}';
        }

        /* WooCommerce bg_parallax and bg_image */
        if( ( class_exists( 'WooCommerce' ) ) && ( $woo_title_area_parallax != '' ) && ( is_shop()||is_product_category()||is_product_tag() ) ) {
          $css_metabox .= '.h-below:not(.hbidar){background-image: url('.$woo_title_area_parallax.'); -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; background-repeat: no-repeat; background-color: transparent;}'; 
        }

        if( ( class_exists( 'WooCommerce' ) ) && ($woo_title_area_parallax == '') && ( $woo_title_area_background_image != '' ) && ( is_shop()||is_product_category()||is_product_tag() ) ) {
          $css_metabox .= '.h-below:not(.hbidar){background-image: url('.$woo_title_area_background_image.'); -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; background-color: transparent; background-position: 50% 50%;}'; 
        }


        /* custom theme css */
        $custom_theme_css = get_theme_mod('custom_theme_css');
        if (get_theme_mod('custom_theme_css') != '') {
          $css_metabox .= ''.$custom_theme_css.'';
        }

        $custom_css = "$css_metabox";
        wp_add_inline_style( 'gotham-dynamic-style', $custom_css );

    }

}
add_action( 'wp_enqueue_scripts', 'gotham_dynamic_style' );