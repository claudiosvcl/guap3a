<?php

function gotham_flickr_load_widgets() {
	register_widget('gotham_flickr_widget');
}

add_action( 'widgets_init', 'gotham_flickr_load_widgets' );

/* constructor */
class gotham_flickr_widget extends WP_Widget {

	 function __construct() {
		// widget actual processes
	 	$widget_ops = array ( 'classname' => 'gotham-flickr', 'description' => 'Flickr photos' );
		parent::__construct('gotham-flickr', esc_html__('Gotham Flickr', 'gotham'), $widget_ops);
		$this->alt_option_name = 'gotham-flickr';
	}

     function widget( $args, $instance ) {
		// outputs the content of the widget
     	extract($args);

     	$title = apply_filters('widget_title', $instance['title']);
     	$flickrid = $instance['flickrid'];
     	$number = absint($instance['number']);
     	if ( empty($title) ) $title = false;
  

     	?>
		<div class="widget_flickr">
		<h3><?php echo $instance['title']; ?></h3>
		<div class="flickr_wrap">
		<script src="http://www.flickr.com/badge_code_v2.gne?count=<?php echo $number; ?>&amp;display=latest&amp;size=s&amp;layout=x&amp;source=user&amp;user=<?php echo $flickrid; ?>">
		</script>
	    </div>
		</div>
	
	<?php

	}

 	 function form( $instance ) {
		// outputs the options form on admin
 	 	$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
 	 	$flickrid = isset( $instance['flickrid'] ) ? esc_attr( $instance['flickrid'] ) : '';
 	 	$number = isset( $instance['number'] )  ? esc_attr( $instance['number'] ) : '';

 	 	?>

		<p>
            <label for="<?php echo esc_attr( $this->get_field_id('title') ); ?>">
               Title:
            </label>
                <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('title') ); ?>" name="<?php echo esc_attr( $this->get_field_name('title') ); ?>" type="text" value="<?php echo $title; ?>" />
        </p>

		

		<p>
            <label for="<?php echo esc_attr( $this->get_field_id('flickrid') ); ?>">
               Flickr Id:
            </label>
                <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('flickrid') ); ?>" name="<?php echo esc_attr( $this->get_field_name('flickrid') ); ?>" type="text" value="<?php echo $flickrid; ?>" />
               
        </p>

		<p>
            <label for="<?php echo esc_attr( $this->get_field_id('number') ); ?>">
               Number of Photos:
            </label>
                <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('number') ); ?>" name="<?php echo esc_attr( $this->get_field_name('number') ); ?>" type="text" value="<?php echo $number; ?>" />
                <?php $flickidget = 'http://idgettr.com'; ?>
        		<small> <a href="<?php echo esc_url($flickidget); ?>"><?php esc_html_e("If you don't know your ID", "gotham"); ?></a></small>
        </p>



<?php
	}

	 function update( $new_instance, $old_instance ) {
		// processes widget options to be saved
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['flickrid']=$new_instance['flickrid'];
		$instance['number']=$new_instance['number'];
		return $instance;
	}
}


