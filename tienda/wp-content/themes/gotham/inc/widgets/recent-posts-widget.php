<?php

function gotham_recent_posts_load_widgets() {
	register_widget('gotham_Widget_Recent_Posts');
}
add_action( 'widgets_init', 'gotham_recent_posts_load_widgets' );

/**
 * Recent_Posts widget class
 *
 * @since 2.8.0
 */
class gotham_Widget_Recent_Posts extends WP_Widget {

	function __construct() {
		$widget_ops = array('classname' => 'widget_recent_entries', 'description' => esc_html__( "Your site&#8217;s most recent Posts.", "gotham" ) );
		parent::__construct('recent-posts', esc_html__('Recent Posts', 'gotham'), $widget_ops);
		$this->alt_option_name = 'widget_recent_entries';
	}

	function widget($args, $instance) {
		$cache = array();
		if ( ! $this->is_preview() ) {
			$cache = wp_cache_get( 'widget_recent_posts', 'widget' );
		}

		if ( ! is_array( $cache ) ) {
			$cache = array();
		}

		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();
		extract($args);

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : esc_html__( 'Recent Posts', 'gotham' );

		/** This filter is documented in wp-includes/default-widgets.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
		if ( ! $number )
			$number = 5;
		$show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;

		/**
		 * Filter the arguments for the Recent Posts widget.
		 */
		$rctpw = new WP_Query( apply_filters( 'widget_posts_args', array(
			'posts_per_page'      => $number,
			'no_found_rows'       => true,
			'post_status'         => 'publish',
			'ignore_sticky_posts' => true
		) ) );

		if ($rctpw->have_posts()) :
?>
		<?php echo $before_widget; ?>
		<?php if ( $title ) echo $before_title . $title . $after_title; ?>
		<div class="rpwrp_wrap">
		<?php while ( $rctpw->have_posts() ) : $rctpw->the_post(); ?>
		<div class="rpwrp">
			<div class="rpwra">
				<?php $rpthumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
				<?php $rpthumbalt = esc_attr( get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true) ); ?>
				<?php if ($rpthumbnail != ""){ ?>
					<img src="<?php echo $rpthumbnail[0]; ?>" alt="<?php echo $rpthumbalt; ?>">
				<?php } ?>

				<?php $rpbackgroundimage = esc_url( get_post_meta( get_the_ID(), 'gotham_title_area_parallax', true ) ); ?>
				<?php if (($rpthumbnail == "")&&($rpbackgroundimage != "")){?>
					<img src="<?php echo $rpbackgroundimage; ?>" alt="<?php the_title_attribute(); ?>">
				<?php } ?>

				<?php $rpbackgroundpic = esc_url( get_post_meta( get_the_ID(), 'gotham_title_area_background_image', true ) ); ?>
				<?php if (($rpthumbnail == "")&&($rpbackgroundimage == "")&&($rpbackgroundpic != "")){?>
					<img src="<?php echo $rpbackgroundpic; ?>" alt="<?php the_title_attribute(); ?>">
				<?php } ?>

				<?php $rpbackgroundcol = get_theme_mod('fifth'); ?>
				<?php if (($rpthumbnail == "")&&($rpbackgroundimage == "")&&($rpbackgroundpic == "")&&($rpbackgroundcol != "")){?>
					<div class="rpbackgroundcol" style="background:<?php echo $rpbackgroundcol; ?>;"></div>
				<?php } ?>

				<?php if (($rpthumbnail == "")&&($rpbackgroundimage == "")&&($rpbackgroundpic == "")&&($rpbackgroundcol == "")){?>
				<div class="rpbackgroundcol"></div>
				<?php } ?>
			</div>
				<div class="rptd">
				<a href="<?php the_permalink(); ?>"><?php get_the_title() ? the_title() : the_ID(); ?></a>
				<?php if ( $show_date ) : ?>
					<span class="post-date"><?php echo get_the_date(); ?></span>		
				<?php endif; ?>
				</div>
		</div>
		<?php endwhile; ?>
		</div>
		<?php echo $after_widget; ?>
<?php
		// Reset the global $the_post as this query will have stomped on it
		wp_reset_postdata();

		endif;

		if ( ! $this->is_preview() ) {
			$cache[ $args['widget_id'] ] = ob_get_flush();
			wp_cache_set( 'widget_recent_posts', $cache, 'widget' );
		} else {
			ob_end_flush();
		}
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = (int) $new_instance['number'];
		$instance['show_date'] = isset( $new_instance['show_date'] ) ? (bool) $new_instance['show_date'] : false;

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['widget_recent_entries']) )
			delete_option('widget_recent_entries');

		return $instance;
	}

	function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number    = isset( $instance['number'] ) ? esc_attr( absint( $instance['number'] ) ) : 5;
		$show_date = isset( $instance['show_date'] ) ? esc_attr( (bool) $instance['show_date'] ) : false;
?>
		<p><label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php esc_html_e( 'Title:', 'gotham'); ?></label>
		<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" type="text" value="<?php echo $title; ?>" /></p>

		<p><label for="<?php echo esc_attr($this->get_field_id( 'number' )); ?>"><?php esc_html_e( 'Number of posts to show:', 'gotham' ); ?></label>
		<input id="<?php echo esc_attr($this->get_field_id( 'number' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'number' )); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>

		<p><input class="checkbox" type="checkbox" <?php checked( $show_date ); ?> id="<?php echo esc_attr($this->get_field_id( 'show_date' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'show_date' )); ?>" />
		<label for="<?php echo esc_attr($this->get_field_id( 'show_date' )); ?>"><?php esc_html_e( 'Display post date?', 'gotham' ); ?></label></p>
<?php
	}
}