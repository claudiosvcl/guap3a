<?php

function gotham_popular_projects_load_widgets() {
	register_widget('gotham_popular_projects_widget');
}
add_action( 'widgets_init', 'gotham_popular_projects_load_widgets' );

class gotham_popular_projects_widget extends WP_Widget {

	function __construct() {
		$widget_ops = array('classname' => 'widget_popular_projects', 'description' => esc_html__( "Your site&#8217;s most popular Projects.", "gotham" ) );
		parent::__construct('popular-projects', esc_html__('Popular Projects', 'gotham'), $widget_ops);
		$this->alt_option_name = 'widget_popular_projects';
	}

	function widget($args, $instance) {
		extract($args);

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : esc_html__( 'Popular Projects', 'gotham' );

		/** This filter is documented in wp-includes/default-widgets.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
		if ( ! $number )
			$number = 5;

		/**
		 * Filter the arguments for the Popular Projects widget.
		 */
		$vote_count = get_post_meta(get_the_ID(), "votes_count", true);
		$popw = new WP_Query(array(
			'posts_per_page'      => $number,
			'meta_key' 			  => 'votes_count',
			'orderby'			  => 'meta_value_num',
			'post_type' 	      => array('portfolio'),
			'no_found_rows'       => true,
			'post_status'         => 'publish',
			'ignore_sticky_posts' => true
		) );

		if ($popw->have_posts()) :
?>
		<?php echo $before_widget; ?>
		<?php if ( $title ) echo $before_title . $title . $after_title; ?>
		<div class="rpwrp_wrap">
		<?php $ppcount = 1; ?>
		<?php while ( $popw->have_posts() ) : $popw->the_post(); ?>

		<?php $title_Post = get_the_title();
		$the_title_Post = '';
		if ($title_Post != "") {
			if (strlen($title_Post) > 11) {
				$the_title_Post = substr($title_Post,0,11).'...';
			}
			else {
				$the_title_Post=$title_Post;
			}
		} ?>

		<div class="rpwrp">
			<a href="<?php the_permalink(); ?>">
			<div class="rpwra">
				<?php $rpthumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
				<?php $rpthumbalt = esc_attr( get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true) ); ?>
				<?php if ($rpthumbnail != ""){ ?>
					<img src="<?php echo $rpthumbnail[0]; ?>" alt="<?php echo $rpthumbalt; ?>">
				<?php } ?>

				<?php $rpbackgroundimage = esc_url( get_post_meta( get_the_ID(), 'gotham_title_area_parallax', true ) ); ?>
				<?php if (($rpthumbnail == "")&&($rpbackgroundimage != "")){?>
					<img src="<?php echo $rpbackgroundimage; ?>" alt="<?php the_title_attribute(); ?>">
				<?php } ?>

				<?php $rpbackgroundpic = esc_url( get_post_meta( get_the_ID(), 'gotham_title_area_background_image', true ) ); ?>
				<?php if (($rpthumbnail == "")&&($rpbackgroundimage == "")&&($rpbackgroundpic != "")){?>
					<img src="<?php echo $rpbackgroundpic; ?>" alt="<?php the_title_attribute(); ?>">
				<?php } ?>
				<?php $rpbackgroundcol = get_theme_mod('primary'); ?>
				<?php if (($rpthumbnail == "")&&($rpbackgroundimage == "")&&($rpbackgroundpic == "")&&($rpbackgroundcol != "")){?>
					<div class="rpbackgroundcol" style="background:<?php echo $rpbackgroundcol; ?>;"></div>
				<?php } ?>
				<?php if (($rpthumbnail == "")&&($rpbackgroundimage == "")&&($rpbackgroundpic == "")&&($rpbackgroundcol == "")){?>
				<div class="rpbackgroundcol"></div>
				<?php } ?>
			</div>
				<div class="rptd">
				<span><?php echo '0'.$ppcount++; ?></span>
				<div class="ppwrap">
				<p class="pptitlep"><?php echo $the_title_Post; ?></p>
				<p class="ppcatp">
					<?php 
						$custom = wp_get_post_terms(get_the_ID(), 'portfolio_category'); $t = count($custom); $counter = 0;
						foreach($custom as $custom) {
							if ($counter >= 1) break;
							$counter++; 
							echo $custom->name;
						}
					?>
				</p>
				</div>
				</div>
			</a>
		</div>
		<?php endwhile; ?>
		</div>
		<?php echo $after_widget; ?>
<?php
		// Reset the global $the_post as this query will have stomped on it
		wp_reset_postdata();

		endif;

	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = (int) $new_instance['number'];

		return $instance;
	}

	function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number    = isset( $instance['number'] ) ? esc_attr( absint( $instance['number'] ) ) : 5;
?>
		<p><label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php esc_html_e( 'Title:', 'gotham' ); ?></label>
		<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" type="text" value="<?php echo $title; ?>" /></p>

		<p><label for="<?php echo esc_attr($this->get_field_id( 'number' )); ?>"><?php esc_html_e( 'Number of posts to show:', 'gotham' ); ?></label>
		<input id="<?php echo esc_attr($this->get_field_id( 'number' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'number' )); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>
<?php
	}
}