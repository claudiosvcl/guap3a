<?php

function gotham_random_posts_load_widgets() {
	register_widget('gotham_widget_random_posts');
}
add_action( 'widgets_init', 'gotham_random_posts_load_widgets' );

/**
 * Random Posts widget class
 */
class gotham_widget_random_posts extends WP_Widget {

	function __construct() {
		$widget_ops = array('classname' => 'widget_random_entries', 'description' => esc_html__( "Your site&#8217;s most random Posts.", "gotham" ) );
		parent::__construct('random-posts', esc_html__('Random Posts', 'gotham'), $widget_ops);
		$this->alt_option_name = 'widget_random_entries';
	}

	function widget($args, $instance) {
		$cache = array();
		if ( ! $this->is_preview() ) {
			$cache = wp_cache_get( 'widget_random_posts', 'widget' );
		}

		if ( ! is_array( $cache ) ) {
			$cache = array();
		}

		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();
		extract($args);

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : esc_html__( 'Random Posts', 'gotham' );

		/** This filter is documented in wp-includes/default-widgets.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		/**
		 * Filter the arguments for the Random Posts widget.
		 */
		$randpw = new WP_Query( apply_filters( 'widget_posts_args', array(
			'posts_per_page'      => 1,
			'orderby'			  => 'rand',
			'no_found_rows'       => true,
			'post_status'         => 'publish',
			'ignore_sticky_posts' => true
		) ) );

		if ($randpw->have_posts()) :
?>
		<?php echo $before_widget; ?>
		<?php if ( $title ) echo $before_title . $title . $after_title; ?>
		<div class="rpwrp_wrap">
		<?php while ( $randpw->have_posts() ) : $randpw->the_post(); ?>
		<div class="rpwrp">
			<a href="<?php the_permalink(); ?>">
			<div class="rpwra">
				<?php $rpthumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); $rcategory = get_the_category(); ?>
				<?php $rpthumbalt = esc_attr( get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true) ); ?>
				<?php if ($rpthumbnail != ""){ ?>
					<img src="<?php echo $rpthumbnail[0]; ?>" alt="<?php echo $rpthumbalt; ?>">
				<?php } ?>

				<?php $rpbackgroundimage = esc_url( get_post_meta( get_the_ID(), 'gotham_title_area_parallax', true ) ); ?>
				<?php if (($rpthumbnail == "")&&($rpbackgroundimage != "")){?>
					<img src="<?php echo $rpbackgroundimage; ?>" alt="<?php the_title_attribute(); ?>">
				<?php } ?>

				<?php $rpbackgroundpic = esc_url( get_post_meta( get_the_ID(), 'gotham_title_area_background_image', true ) ); ?>
				<?php if (($rpthumbnail == "")&&($rpbackgroundimage == "")&&($rpbackgroundpic != "")){?>
					<img src="<?php echo $rpbackgroundpic; ?>" alt="<?php the_title_attribute(); ?>">
				<?php } ?>

				<?php $rpbackgroundcol = get_theme_mod('primary'); ?>
				<?php if (($rpthumbnail == "")&&($rpbackgroundimage == "")&&($rpbackgroundpic == "")&&($rpbackgroundcol != "")){?>
					<div class="rpbackgroundcol" style="background:<?php echo $rpbackgroundcol; ?>;"></div>
				<?php } ?>

				<?php if (($rpthumbnail == "")&&($rpbackgroundimage == "")&&($rpbackgroundpic == "")&&($rpbackgroundcol == "")){?>
				<div class="rpbackgroundcol"></div>
				<?php } ?>
			</div>
				<div class="rptd">
					<p class="rdptitlep"><?php get_the_title() ? the_title() : the_ID(); ?></p>
						<p class="rdpcat"><?php echo $rcategory[0]->cat_name; ?></p>
				</div>
			</a>
		</div>
		<?php endwhile; ?>
		</div>
		<?php echo $after_widget; ?>
<?php
		// Reset the global $the_post as this query will have stomped on it
		wp_reset_postdata();

		endif;

		if ( ! $this->is_preview() ) {
			$cache[ $args['widget_id'] ] = ob_get_flush();
			wp_cache_set( 'widget_random_posts', $cache, 'widget' );
		} else {
			ob_end_flush();
		}
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['widget_random_entries']) )
			delete_option('widget_random_entries');

		return $instance;
	}

	function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
?>
		<p><label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php esc_html_e( 'Title:', 'gotham'); ?></label>
		<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" type="text" value="<?php echo $title; ?>" /></p>
<?php
	}
}