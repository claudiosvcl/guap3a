<?php
/**
 * WooCommerce
 */

if ( ! function_exists( 'gotham_woocommerce_support' ) ) :
//add woocommerce support
function gotham_woocommerce_support() {
  add_theme_support( 'woocommerce' );
}
endif;
add_action( 'after_setup_theme', 'gotham_woocommerce_support' );


remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);


if ( ! function_exists( 'gotham_wrapper_start' ) ) :
function gotham_wrapper_start() {
  $woogetpid = wc_get_page_id('shop');
  $title_area = get_post_meta( $woogetpid, 'gotham_select_title_area', true );
  $wrapper_start = '<div id="content" class="spec dftpg';
  if (($title_area == "no")||is_product()) {
    $wrapper_start .= ' woowrst';
  }
  $wrapper_start .= '">';
  echo $wrapper_start;
}
endif;
add_action('woocommerce_before_main_content', 'gotham_wrapper_start', 10);


remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);


if ( ! function_exists( 'gotham_wrapper_end' ) ) :
function gotham_wrapper_end() {
  echo '</div>';
}
endif;
add_action('woocommerce_after_main_content', 'gotham_wrapper_end', 10);


// Removing the default WooCommerce stylesheet
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );


if ( ! function_exists( 'gotham_adding_woocommerce_scripts' ) ) :
//WooCommerce styles and scripts
function gotham_adding_woocommerce_scripts(){
  wp_register_style( 'gotham-woocommerce-css', get_template_directory_uri() . '/css/woocommerce.min.css' );
    wp_enqueue_style( 'gotham-woocommerce-css' );
  wp_register_script('gotham-woocommerce-js', get_template_directory_uri() . '/js/gotham-woocommerce.min.js', 'jquery', '',true);
  wp_enqueue_script('gotham-woocommerce-js');
}
endif;
add_action( 'wp_enqueue_scripts', 'gotham_adding_woocommerce_scripts', 11 );

//WooCommerce header cart
if ( ! function_exists( 'gotham_header_cart' ) ) :
  function gotham_header_cart() {
    global $post;
    if(get_theme_mod('header_cart', 'no') == 'yes'||(get_theme_mod('header_cart', 'no') == 'no' && (is_woocommerce()||is_cart()||is_checkout()||is_account_page()||(isset($post->post_content) && has_shortcode( $post->post_content, 'woocommerce_slider'))))) {
    ?>
    <div class="header-cart">
    <a class="cart-contents" href="<?php echo wc_get_cart_url(); ?>" title="<?php esc_attr_e( 'View your shopping cart', 'woocommerce' ); ?>">
    <span><?php echo WC()->cart->get_cart_contents_count(); ?></span>
    </a>
    <?php the_widget( 'WC_Widget_Cart', 'title=' ); ?>
    </div>

    <?php }
  }
endif;

// Ensure cart contents update when products are added to the cart via AJAX
if ( ! function_exists( 'gotham_header_add_to_cart_fragment' ) ) :
  function gotham_header_add_to_cart_fragment( $fragments ) {
    global $woocommerce;
    
    ob_start();
    
    ?>
    <a class="cart-contents" href="<?php echo wc_get_cart_url(); ?>" title="<?php esc_attr_e( 'View your shopping cart', 'woocommerce' ); ?>">
      <span><?php echo WC()->cart->get_cart_contents_count(); ?></span>
    </a>
    <?php
    
    $fragments['a.cart-contents'] = ob_get_clean();
    
    return $fragments;
    
  }
endif;
add_filter('add_to_cart_fragments', 'gotham_header_add_to_cart_fragment');

//WooCommerce breadcrumb
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
add_action('woocommerce_before_single_product', 'woocommerce_breadcrumb');

if ( ! function_exists( 'gotham_woocommerce_breadcrumbs' ) ) :
function gotham_woocommerce_breadcrumbs() {
  return array(
    'delimiter'   => ' &#8226; ',
    'wrap_before' => '<div class="woocommerce-breadcrumb">',
    'wrap_after'  => '</div>',
    'before'      => '',
    'after'       => '',
    'home'        => esc_html_x( 'Home', 'breadcrumb', 'woocommerce' ),
  );
}
endif;
add_filter( 'woocommerce_breadcrumb_defaults', 'gotham_woocommerce_breadcrumbs' );


if ( ! function_exists( 'gotham_remove_product_page_sku' ) ) :
//WooCommerce sku
function gotham_remove_product_page_sku( $enabled ) {
  if ( ! is_admin() && is_product() ) {
    return false;
  }
  return $enabled;
}
endif;
add_filter( 'wc_product_sku_enabled', 'gotham_remove_product_page_sku' );


if ( ! function_exists( 'gotham_show_sku' ) ) :
function gotham_show_sku() {
  global $product;
  if($product->get_sku()) {
    echo '<span class="sku_wrapper">SKU: <span class="sku" itemprop="sku">' . $product->get_sku() . ' </span></span>';
  }
}
endif;
add_action( 'woocommerce_single_product_summary', 'gotham_show_sku', 1 );


//WooCommerce price
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 6 );


//WooCommerce add to cart
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 10 );


//WooCommerce single excerpt
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);


if ( ! function_exists( 'gotham_show_content' ) ) :
function gotham_show_content() {
  if(get_the_excerpt()) {
    echo '<p class="woodesc">Description</p><div class="wooexcerpt">';
    echo '' . the_excerpt() .'</div>';
  }
}
endif;
add_action('woocommerce_single_product_summary', 'gotham_show_content' );


//WooCommerce sale flash
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash' );


//WooCommerce related products
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
add_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 2 );


if ( ! function_exists( 'gotham_related_products_args' ) ) :
function gotham_related_products_args( $args ) {
  if( isset( $args['posts_per_page'] ) ) {
    $args['posts_per_page'] = 4; // 6 related products
  }
  return $args;
}
endif;
add_filter( 'woocommerce_output_related_products_args', 'gotham_related_products_args' );


//WooCommerce up-sells
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
add_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 1 );

//WooCommerce cross-sells
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' , 10 );

//WooCommerce remove add to cart on related
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart' );
//WooCommerce remove on sale image on related
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
//WooCommerce remove star-rating on related
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );

if ( ! function_exists( 'gotham_woocommerce_secondary_thumb' ) ) :
//shop secondary thumb
function gotham_woocommerce_secondary_thumb() {
  global $product;
  $attachment_ids = $product->get_gallery_attachment_ids();
  if($attachment_ids && (isset($attachment_ids['1']))&&(is_shop()||is_product_category()||is_product_tag())) {
    $secondary_image_id = $attachment_ids['1'];
    echo wp_get_attachment_image( $secondary_image_id, 'shop_catalog', '', $attr = array('class' => 'woondary-thumb') );
  }
}
endif;
add_action( 'woocommerce_before_shop_loop_item_title', 'gotham_woocommerce_secondary_thumb' );

if ( ! function_exists( 'gotham_review_tabs' ) ) :
//WooCommerce product tabs
function gotham_review_tabs( $tabs ) {
  global $product;
  if( isset( $tabs['reviews'] ) ) {
    $tabs['reviews']['title'] = esc_html__( 'Reviews & Ratings', 'woocommerce' );
  }
  return $tabs;
}
endif;
add_filter( 'woocommerce_product_tabs', 'gotham_review_tabs', 98 );


if ( ! function_exists( 'gotham_share_for_woocommerce' ) ) :
//WooCommerce sharing
function gotham_share_for_woocommerce() {
  ?>
  <div class="woocommerce share-opf">
    <button class="md-icon-email"></button>
    <button class="iconfa-facebook"></button>
    <button class="iconfa-twitter"></button>
  </div>
  <?php
}
endif;
add_action( 'woocommerce_share', 'gotham_share_for_woocommerce' );


if ( ! function_exists('gotham_loop_columns')) :
// Change number or products per row to 4 in shop
function gotham_loop_columns() {
  return 3; // 3 products per row
}
endif;
add_filter('loop_shop_columns', 'gotham_loop_columns');


if ( ! function_exists('gotham_custom_override_checkout_fields')) :
//WooCommerce checkout billing placeholder
function gotham_custom_override_checkout_fields( $fields ) {
    $fields['billing']['billing_first_name']['placeholder'] = 'First Name';
    $fields['billing']['billing_last_name']['placeholder'] = 'Last Name';
    $fields['billing']['billing_company']['placeholder'] = 'Company Name';
    $fields['billing']['billing_email']['placeholder'] = 'Email Address';
    $fields['billing']['billing_phone']['placeholder'] = 'Phone';
    $fields['billing']['billing_address_1']['placeholder'] = 'Address';
    $fields['billing']['billing_city']['placeholder'] = 'Town / City';
    $fields['billing']['billing_postcode']['placeholder'] = 'ZIP';
    unset($fields['order']['order_comments']['label']);

    return $fields;
}
endif;
add_filter( 'woocommerce_checkout_fields' , 'gotham_custom_override_checkout_fields' );

if ( ! function_exists('gotham_custom_override_my_account_billing_fields')) :
//WooCommerce my account billing placeholder
function gotham_custom_override_my_account_billing_fields( $fields ) {
    $fields['billing_first_name']['placeholder'] = 'First Name';
    $fields['billing_last_name']['placeholder'] = 'Last Name';
    $fields['billing_company']['placeholder'] = 'Company Name';
    $fields['billing_email']['placeholder'] = 'Email Address';
    $fields['billing_phone']['placeholder'] = 'Phone';
    $fields['billing_address_1']['placeholder'] = 'Address';
    $fields['billing_city']['placeholder'] = 'Town / City';
    $fields['billing_postcode']['placeholder'] = 'ZIP';

    return $fields;
}
endif;
add_filter( 'woocommerce_billing_fields' , 'gotham_custom_override_my_account_billing_fields' );

if ( ! function_exists('gotham_custom_override_my_account_shipping_fields')) :
//WooCommerce my account shipping placeholder
function gotham_custom_override_my_account_shipping_fields( $fields ) {
    $fields['shipping_first_name']['placeholder'] = 'First Name';
    $fields['shipping_last_name']['placeholder'] = 'Last Name';
    $fields['shipping_company']['placeholder'] = 'Company Name';
    $fields['shipping_email']['placeholder'] = 'Email Address';
    $fields['shipping_phone']['placeholder'] = 'Phone';
    $fields['shipping_address_1']['placeholder'] = 'Address';
    $fields['shipping_city']['placeholder'] = 'Town / City';
    $fields['shipping_postcode']['placeholder'] = 'ZIP';

    return $fields;
}
endif;
add_filter( 'woocommerce_shipping_fields' , 'gotham_custom_override_my_account_shipping_fields' );