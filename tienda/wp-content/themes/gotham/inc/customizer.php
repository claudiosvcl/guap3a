<?php
/**
 * Gotham Customizer functionnality
 *
 */

function gotham_customize_register( $wp_customize ) {

$google_web_fonts = array(
"ABeeZee" => "ABeeZee",
"Abel" => "Abel",
"Abril Fatface" => "Abril Fatface",
"Aclonica" => "Aclonica",
"Acme" => "Acme",
"Actor" => "Actor",
"Adamina" => "Adamina",
"Advent Pro" => "Advent Pro",
"Aguafina Script" => "Aguafina Script",
"Akronim" => "Akronim",
"Aladin" => "Aladin",
"Aldrich" => "Aldrich",
"Alegreya" => "Alegreya",
"Alegreya SC" => "Alegreya SC",
"Alex Brush" => "Alex Brush",
"Alfa Slab One" => "Alfa Slab One",
"Alice" => "Alice",
"Alike" => "Alike",
"Alike Angular" => "Alike Angular",
"Allan" => "Allan",
"Allerta" => "Allerta",
"Allerta Stencil" => "Allerta Stencil",
"Allura" => "Allura",
"Almendra" => "Almendra",
"Almendra Display" => "Almendra Display",
"Almendra SC" => "Almendra SC",
"Amarante" => "Amarante",
"Amaranth" => "Amaranth",
"Amatic SC" => "Amatic SC",
"Amethysta" => "Amethysta",
"Anaheim" => "Anaheim",
"Andada" => "Andada",
"Andika" => "Andika",
"Angkor" => "Angkor",
"Annie Use Your Telescope" => "Annie Use Your Telescope",
"Anonymous Pro" => "Anonymous Pro",
"Antic" => "Antic",
"Antic Didone" => "Antic Didone",
"Antic Slab" => "Antic Slab",
"Anton" => "Anton",
"Arapey" => "Arapey",
"Arbutus" => "Arbutus",
"Arbutus Slab" => "Arbutus Slab",
"Architects Daughter" => "Architects Daughter",
"Archivo Black" => "Archivo Black",
"Archivo Narrow" => "Archivo Narrow",
"Arimo" => "Arimo",
"Arizonia" => "Arizonia",
"Armata" => "Armata",
"Artifika" => "Artifika",
"Arvo" => "Arvo",
"Asap" => "Asap",
"Asset" => "Asset",
"Astloch" => "Astloch",
"Asul" => "Asul",
"Atomic Age" => "Atomic Age",
"Aubrey" => "Aubrey",
"Audiowide" => "Audiowide",
"Autour One" => "Autour One",
"Average" => "Average",
"Average Sans" => "Average Sans",
"Averia Gruesa Libre" => "Averia Gruesa Libre",
"Averia Libre" => "Averia Libre",
"Averia Sans Libre" => "Averia Sans Libre",
"Averia Serif Libre" => "Averia Serif Libre",
"Bad Script" => "Bad Script",
"Balthazar" => "Balthazar",
"Bangers" => "Bangers",
"Basic" => "Basic",
"Battambang" => "Battambang",
"Baumans" => "Baumans",
"Bayon" => "Bayon",
"Belgrano" => "Belgrano",
"Belleza" => "Belleza",
"BenchNine" => "BenchNine",
"Bentham" => "Bentham",
"Berkshire Swash" => "Berkshire Swash",
"Bevan" => "Bevan",
"Bigelow Rules" => "Bigelow Rules",
"Bigshot One" => "Bigshot One",
"Bilbo" => "Bilbo",
"Bilbo Swash Caps" => "Bilbo Swash Caps",
"Bitter" => "Bitter",
"Black Ops One" => "Black Ops One",
"Bokor" => "Bokor",
"Bonbon" => "Bonbon",
"Boogaloo" => "Boogaloo",
"Bowlby One" => "Bowlby One",
"Bowlby One SC" => "Bowlby One SC",
"Brawler" => "Brawler",
"Bree Serif" => "Bree Serif",
"Bubblegum Sans" => "Bubblegum Sans",
"Bubbler One" => "Bubbler One",
"Buda" => "Buda",
"Buenard" => "Buenard",
"Butcherman" => "Butcherman",
"Butterfly Kids" => "Butterfly Kids",
"Cabin" => "Cabin",
"Cabin Condensed" => "Cabin Condensed",
"Cabin Sketch" => "Cabin Sketch",
"Caesar Dressing" => "Caesar Dressing",
"Cagliostro" => "Cagliostro",
"Calligraffitti" => "Calligraffitti",
"Cambo" => "Cambo",
"Candal" => "Candal",
"Cantarell" => "Cantarell",
"Cantata One" => "Cantata One",
"Cantora One" => "Cantora One",
"Capriola" => "Capriola",
"Cardo" => "Cardo",
"Carme" => "Carme",
"Carrois Gothic" => "Carrois Gothic",
"Carrois Gothic SC" => "Carrois Gothic SC",
"Carter One" => "Carter One",
"Caudex" => "Caudex",
"Cedarville Cursive" => "Cedarville Cursive",
"Ceviche One" => "Ceviche One",
"Changa One" => "Changa One",
"Chango" => "Chango",
"Chau Philomene One" => "Chau Philomene One",
"Chela One" => "Chela One",
"Chelsea Market" => "Chelsea Market",
"Chenla" => "Chenla",
"Cherry Cream Soda" => "Cherry Cream Soda",
"Cherry Swash" => "Cherry Swash",
"Chewy" => "Chewy",
"Chicle" => "Chicle",
"Chivo" => "Chivo",
"Cinzel" => "Cinzel",
"Cinzel Decorative" => "Cinzel Decorative",
"Clicker Script" => "Clicker Script",
"Coda" => "Coda",
"Coda Caption" => "Coda Caption",
"Codystar" => "Codystar",
"Combo" => "Combo",
"Comfortaa" => "Comfortaa",
"Coming Soon" => "Coming Soon",
"Concert One" => "Concert One",
"Condiment" => "Condiment",
"Content" => "Content",
"Contrail One" => "Contrail One",
"Convergence" => "Convergence",
"Cookie" => "Cookie",
"Copse" => "Copse",
"Corben" => "Corben",
"Courgette" => "Courgette",
"Cousine" => "Cousine",
"Coustard" => "Coustard",
"Covered By Your Grace" => "Covered By Your Grace",
"Crafty Girls" => "Crafty Girls",
"Creepster" => "Creepster",
"Crete Round" => "Crete Round",
"Crimson Text" => "Crimson Text",
"Croissant One" => "Croissant One",
"Crushed" => "Crushed",
"Cuprum" => "Cuprum",
"Cutive" => "Cutive",
"Cutive Mono" => "Cutive Mono",
"Damion" => "Damion",
"Dancing Script" => "Dancing Script",
"Dangrek" => "Dangrek",
"Dawning of a New Day" => "Dawning of a New Day",
"Days One" => "Days One",
"Delius" => "Delius",
"Delius Swash Caps" => "Delius Swash Caps",
"Delius Unicase" => "Delius Unicase",
"Della Respira" => "Della Respira",
"Denk One" => "Denk One",
"Devonshire" => "Devonshire",
"Didact Gothic" => "Didact Gothic",
"Diplomata" => "Diplomata",
"Diplomata SC" => "Diplomata SC",
"Domine" => "Domine",
"Donegal One" => "Donegal One",
"Doppio One" => "Doppio One",
"Dorsa" => "Dorsa",
"Dosis" => "Dosis",
"Dr Sugiyama" => "Dr Sugiyama",
"Droid Sans" => "Droid Sans",
"Droid Sans Mono" => "Droid Sans Mono",
"Droid Serif" => "Droid Serif",
"Duru Sans" => "Duru Sans",
"Dynalight" => "Dynalight",
"EB Garamond" => "EB Garamond",
"Eagle Lake" => "Eagle Lake",
"Eater" => "Eater",
"Economica" => "Economica",
"Electrolize" => "Electrolize",
"Elsie" => "Elsie",
"Elsie Swash Caps" => "Elsie Swash Caps",
"Emblema One" => "Emblema One",
"Emilys Candy" => "Emilys Candy",
"Engagement" => "Engagement",
"Englebert" => "Englebert",
"Enriqueta" => "Enriqueta",
"Erica One" => "Erica One",
"Esteban" => "Esteban",
"Euphoria Script" => "Euphoria Script",
"Ewert" => "Ewert",
"Exo" => "Exo",
"Expletus Sans" => "Expletus Sans",
"Fanwood Text" => "Fanwood Text",
"Fascinate" => "Fascinate",
"Fascinate Inline" => "Fascinate Inline",
"Faster One" => "Faster One",
"Fasthand" => "Fasthand",
"Federant" => "Federant",
"Federo" => "Federo",
"Felipa" => "Felipa",
"Fenix" => "Fenix",
"Finger Paint" => "Finger Paint",
"Fjalla One" => "Fjalla One",
"Fjord One" => "Fjord One",
"Flamenco" => "Flamenco",
"Flavors" => "Flavors",
"Fondamento" => "Fondamento",
"Fontdiner Swanky" => "Fontdiner Swanky",
"Forum" => "Forum",
"Francois One" => "Francois One",
"Freckle Face" => "Freckle Face",
"Fredericka the Great" => "Fredericka the Great",
"Fredoka One" => "Fredoka One",
"Freehand" => "Freehand",
"Fresca" => "Fresca",
"Frijole" => "Frijole",
"Fruktur" => "Fruktur",
"Fugaz One" => "Fugaz One",
"GFS Didot" => "GFS Didot",
"GFS Neohellenic" => "GFS Neohellenic",
"Gabriela" => "Gabriela",
"Gafata" => "Gafata",
"Galdeano" => "Galdeano",
"Galindo" => "Galindo",
"Gentium Basic" => "Gentium Basic",
"Gentium Book Basic" => "Gentium Book Basic",
"Geo" => "Geo",
"Geostar" => "Geostar",
"Geostar Fill" => "Geostar Fill",
"Germania One" => "Germania One",
"Gilda Display" => "Gilda Display",
"Give You Glory" => "Give You Glory",
"Glass Antiqua" => "Glass Antiqua",
"Glegoo" => "Glegoo",
"Gloria Hallelujah" => "Gloria Hallelujah",
"Goblin One" => "Goblin One",
"Gochi Hand" => "Gochi Hand",
"Gorditas" => "Gorditas",
"Goudy Bookletter 1911" => "Goudy Bookletter 1911",
"Graduate" => "Graduate",
"Grand Hotel" => "Grand Hotel",
"Gravitas One" => "Gravitas One",
"Great Vibes" => "Great Vibes",
"Griffy" => "Griffy",
"Gruppo" => "Gruppo",
"Gudea" => "Gudea",
"Habibi" => "Habibi",
"Hammersmith One" => "Hammersmith One",
"Hanalei" => "Hanalei",
"Hanalei Fill" => "Hanalei Fill",
"Handlee" => "Handlee",
"Hanuman" => "Hanuman",
"Happy Monkey" => "Happy Monkey",
"Headland One" => "Headland One",
"Henny Penny" => "Henny Penny",
"Herr Von Muellerhoff" => "Herr Von Muellerhoff",
"Holtwood One SC" => "Holtwood One SC",
"Homemade Apple" => "Homemade Apple",
"Homenaje" => "Homenaje",
"IM Fell DW Pica" => "IM Fell DW Pica",
"IM Fell DW Pica SC" => "IM Fell DW Pica SC",
"IM Fell Double Pica" => "IM Fell Double Pica",
"IM Fell Double Pica SC" => "IM Fell Double Pica SC",
"IM Fell English" => "IM Fell English",
"IM Fell English SC" => "IM Fell English SC",
"IM Fell French Canon" => "IM Fell French Canon",
"IM Fell French Canon SC" => "IM Fell French Canon SC",
"IM Fell Great Primer" => "IM Fell Great Primer",
"IM Fell Great Primer SC" => "IM Fell Great Primer SC",
"Iceberg" => "Iceberg",
"Iceland" => "Iceland",
"Imprima" => "Imprima",
"Inconsolata" => "Inconsolata",
"Inder" => "Inder",
"Indie Flower" => "Indie Flower",
"Inika" => "Inika",
"Irish Grover" => "Irish Grover",
"Istok Web" => "Istok Web",
"Italiana" => "Italiana",
"Italianno" => "Italianno",
"Jacques Francois" => "Jacques Francois",
"Jacques Francois Shadow" => "Jacques Francois Shadow",
"Jim Nightshade" => "Jim Nightshade",
"Jockey One" => "Jockey One",
"Jolly Lodger" => "Jolly Lodger",
"Josefin Sans" => "Josefin Sans",
"Josefin Slab" => "Josefin Slab",
"Joti One" => "Joti One",
"Judson" => "Judson",
"Julee" => "Julee",
"Julius Sans One" => "Julius Sans One",
"Junge" => "Junge",
"Jura" => "Jura",
"Just Another Hand" => "Just Another Hand",
"Just Me Again Down Here" => "Just Me Again Down Here",
"Kameron" => "Kameron",
"Karla" => "Karla",
"Kaushan Script" => "Kaushan Script",
"Kavoon" => "Kavoon",
"Keania One" => "Keania One",
"Kelly Slab" => "Kelly Slab",
"Kenia" => "Kenia",
"Khmer" => "Khmer",
"Kite One" => "Kite One",
"Knewave" => "Knewave",
"Kotta One" => "Kotta One",
"Koulen" => "Koulen",
"Kranky" => "Kranky",
"Kreon" => "Kreon",
"Kristi" => "Kristi",
"Krona One" => "Krona One",
"La Belle Aurore" => "La Belle Aurore",
"Lancelot" => "Lancelot",
"Lato" => "Lato",
"League Script" => "League Script",
"Leckerli One" => "Leckerli One",
"Ledger" => "Ledger",
"Lekton" => "Lekton",
"Lemon" => "Lemon",
"Libre Baskerville" => "Libre Baskerville",
"Life Savers" => "Life Savers",
"Lilita One" => "Lilita One",
"Limelight" => "Limelight",
"Linden Hill" => "Linden Hill",
"Lobster" => "Lobster",
"Lobster Two" => "Lobster Two",
"Londrina Outline" => "Londrina Outline",
"Londrina Shadow" => "Londrina Shadow",
"Londrina Sketch" => "Londrina Sketch",
"Londrina Solid" => "Londrina Solid",
"Lora" => "Lora",
"Love Ya Like A Sister" => "Love Ya Like A Sister",
"Loved by the King" => "Loved by the King",
"Lovers Quarrel" => "Lovers Quarrel",
"Luckiest Guy" => "Luckiest Guy",
"Lusitana" => "Lusitana",
"Lustria" => "Lustria",
"Macondo" => "Macondo",
"Macondo Swash Caps" => "Macondo Swash Caps",
"Magra" => "Magra",
"Maiden Orange" => "Maiden Orange",
"Mako" => "Mako",
"Marcellus" => "Marcellus",
"Marcellus SC" => "Marcellus SC",
"Marck Script" => "Marck Script",
"Margarine" => "Margarine",
"Marko One" => "Marko One",
"Marmelad" => "Marmelad",
"Marvel" => "Marvel",
"Mate" => "Mate",
"Mate SC" => "Mate SC",
"Maven Pro" => "Maven Pro",
"McLaren" => "McLaren",
"Meddon" => "Meddon",
"MedievalSharp" => "MedievalSharp",
"Medula One" => "Medula One",
"Megrim" => "Megrim",
"Meie Script" => "Meie Script",
"Merienda" => "Merienda",
"Merienda One" => "Merienda One",
"Merriweather" => "Merriweather",
"Merriweather Sans" => "Merriweather Sans",
"Metal" => "Metal",
"Metal Mania" => "Metal Mania",
"Metamorphous" => "Metamorphous",
"Metrophobic" => "Metrophobic",
"Michroma" => "Michroma",
"Milonga" => "Milonga",
"Miltonian" => "Miltonian",
"Miltonian Tattoo" => "Miltonian Tattoo",
"Miniver" => "Miniver",
"Miss Fajardose" => "Miss Fajardose",
"Modern Antiqua" => "Modern Antiqua",
"Molengo" => "Molengo",
"Molle" => "Molle",
"Monda" => "Monda",
"Monofett" => "Monofett",
"Monoton" => "Monoton",
"Monsieur La Doulaise" => "Monsieur La Doulaise",
"Montaga" => "Montaga",
"Montez" => "Montez",
"Montserrat" => "Montserrat",
"Montserrat Alternates" => "Montserrat Alternates",
"Montserrat Subrayada" => "Montserrat Subrayada",
"Moul" => "Moul",
"Moulpali" => "Moulpali",
"Mountains of Christmas" => "Mountains of Christmas",
"Mouse Memoirs" => "Mouse Memoirs",
"Mr Bedfort" => "Mr Bedfort",
"Mr Dafoe" => "Mr Dafoe",
"Mr De Haviland" => "Mr De Haviland",
"Mrs Saint Delafield" => "Mrs Saint Delafield",
"Mrs Sheppards" => "Mrs Sheppards",
"Muli" => "Muli",
"Mystery Quest" => "Mystery Quest",
"Neucha" => "Neucha",
"Neuton" => "Neuton",
"New Rocker" => "New Rocker",
"News Cycle" => "News Cycle",
"Niconne" => "Niconne",
"Nixie One" => "Nixie One",
"Nobile" => "Nobile",
"Nokora" => "Nokora",
"Norican" => "Norican",
"Nosifer" => "Nosifer",
"Nothing You Could Do" => "Nothing You Could Do",
"Noticia Text" => "Noticia Text",
"Nova Cut" => "Nova Cut",
"Nova Flat" => "Nova Flat",
"Nova Mono" => "Nova Mono",
"Nova Oval" => "Nova Oval",
"Nova Round" => "Nova Round",
"Nova Script" => "Nova Script",
"Nova Slim" => "Nova Slim",
"Nova Square" => "Nova Square",
"Numans" => "Numans",
"Nunito" => "Nunito",
"Odor Mean Chey" => "Odor Mean Chey",
"Offside" => "Offside",
"Old Standard TT" => "Old Standard TT",
"Oldenburg" => "Oldenburg",
"Oleo Script" => "Oleo Script",
"Oleo Script Swash Caps" => "Oleo Script Swash Caps",
"Open Sans" => "Open Sans",
"Open Sans Condensed" => "Open Sans Condensed",
"Oranienbaum" => "Oranienbaum",
"Orbitron" => "Orbitron",
"Oregano" => "Oregano",
"Orienta" => "Orienta",
"Original Surfer" => "Original Surfer",
"Oswald" => "Oswald",
"Over the Rainbow" => "Over the Rainbow",
"Overlock" => "Overlock",
"Overlock SC" => "Overlock SC",
"Ovo" => "Ovo",
"Oxygen" => "Oxygen",
"Oxygen Mono" => "Oxygen Mono",
"PT Mono" => "PT Mono",
"PT Sans" => "PT Sans",
"PT Sans Caption" => "PT Sans Caption",
"PT Sans Narrow" => "PT Sans Narrow",
"PT Serif" => "PT Serif",
"PT Serif Caption" => "PT Serif Caption",
"Pacifico" => "Pacifico",
"Paprika" => "Paprika",
"Parisienne" => "Parisienne",
"Passero One" => "Passero One",
"Passion One" => "Passion One",
"Patrick Hand" => "Patrick Hand",
"Patrick Hand SC" => "Patrick Hand SC",
"Patua One" => "Patua One",
"Paytone One" => "Paytone One",
"Peralta" => "Peralta",
"Permanent Marker" => "Permanent Marker",
"Petit Formal Script" => "Petit Formal Script",
"Petrona" => "Petrona",
"Philosopher" => "Philosopher",
"Piedra" => "Piedra",
"Pinyon Script" => "Pinyon Script",
"Pirata One" => "Pirata One",
"Plaster" => "Plaster",
"Play" => "Play",
"Playball" => "Playball",
"Playfair Display" => "Playfair Display",
"Playfair Display SC" => "Playfair Display SC",
"Podkova" => "Podkova",
"Poiret One" => "Poiret One",
"Poller One" => "Poller One",
"Poly" => "Poly",
"Pompiere" => "Pompiere",
"Pontano Sans" => "Pontano Sans",
"Port Lligat Sans" => "Port Lligat Sans",
"Port Lligat Slab" => "Port Lligat Slab",
"Prata" => "Prata",
"Preahvihear" => "Preahvihear",
"Press Start 2P" => "Press Start 2P",
"Princess Sofia" => "Princess Sofia",
"Prociono" => "Prociono",
"Prosto One" => "Prosto One",
"Puritan" => "Puritan",
"Purple Purse" => "Purple Purse",
"Quando" => "Quando",
"Quantico" => "Quantico",
"Quattrocento" => "Quattrocento",
"Quattrocento Sans" => "Quattrocento Sans",
"Questrial" => "Questrial",
"Quicksand" => "Quicksand",
"Quintessential" => "Quintessential",
"Qwigley" => "Qwigley",
"Racing Sans One" => "Racing Sans One",
"Radley" => "Radley",
"Raleway" => "Raleway",
"Raleway Dots" => "Raleway Dots",
"Rambla" => "Rambla",
"Rammetto One" => "Rammetto One",
"Ranchers" => "Ranchers",
"Rancho" => "Rancho",
"Rationale" => "Rationale",
"Redressed" => "Redressed",
"Reenie Beanie" => "Reenie Beanie",
"Revalia" => "Revalia",
"Ribeye" => "Ribeye",
"Ribeye Marrow" => "Ribeye Marrow",
"Righteous" => "Righteous",
"Risque" => "Risque",
"Roboto" => "Roboto",
"Roboto Condensed" => "Roboto Condensed",
"Roboto Slab" => "Roboto Slab",
"Rochester" => "Rochester",
"Rock Salt" => "Rock Salt",
"Rokkitt" => "Rokkitt",
"Romanesco" => "Romanesco",
"Ropa Sans" => "Ropa Sans",
"Rosario" => "Rosario",
"Rosarivo" => "Rosarivo",
"Rouge Script" => "Rouge Script",
"Ruda" => "Ruda",
"Rufina" => "Rufina",
"Ruge Boogie" => "Ruge Boogie",
"Ruluko" => "Ruluko",
"Rum Raisin" => "Rum Raisin",
"Ruslan Display" => "Ruslan Display",
"Russo One" => "Russo One",
"Ruthie" => "Ruthie",
"Rye" => "Rye",
"Sacramento" => "Sacramento",
"Sail" => "Sail",
"Salsa" => "Salsa",
"Sanchez" => "Sanchez",
"Sancreek" => "Sancreek",
"Sansita One" => "Sansita One",
"Sarina" => "Sarina",
"Satisfy" => "Satisfy",
"Scada" => "Scada",
"Schoolbell" => "Schoolbell",
"Seaweed Script" => "Seaweed Script",
"Sevillana" => "Sevillana",
"Seymour One" => "Seymour One",
"Shadows Into Light" => "Shadows Into Light",
"Shadows Into Light Two" => "Shadows Into Light Two",
"Shanti" => "Shanti",
"Share" => "Share",
"Share Tech" => "Share Tech",
"Share Tech Mono" => "Share Tech Mono",
"Shojumaru" => "Shojumaru",
"Short Stack" => "Short Stack",
"Siemreap" => "Siemreap",
"Sigmar One" => "Sigmar One",
"Signika" => "Signika",
"Signika Negative" => "Signika Negative",
"Simonetta" => "Simonetta",
"Sintony" => "Sintony",
"Sirin Stencil" => "Sirin Stencil",
"Six Caps" => "Six Caps",
"Skranji" => "Skranji",
"Slackey" => "Slackey",
"Smokum" => "Smokum",
"Smythe" => "Smythe",
"Sniglet" => "Sniglet",
"Snippet" => "Snippet",
"Snowburst One" => "Snowburst One",
"Sofadi One" => "Sofadi One",
"Sofia" => "Sofia",
"Sonsie One" => "Sonsie One",
"Sorts Mill Goudy" => "Sorts Mill Goudy",
"Source Code Pro" => "Source Code Pro",
"Source Sans Pro" => "Source Sans Pro",
"Special Elite" => "Special Elite",
"Spicy Rice" => "Spicy Rice",
"Spinnaker" => "Spinnaker",
"Spirax" => "Spirax",
"Squada One" => "Squada One",
"Stalemate" => "Stalemate",
"Stalinist One" => "Stalinist One",
"Stardos Stencil" => "Stardos Stencil",
"Stint Ultra Condensed" => "Stint Ultra Condensed",
"Stint Ultra Expanded" => "Stint Ultra Expanded",
"Stoke" => "Stoke",
"Strait" => "Strait",
"Sue Ellen Francisco" => "Sue Ellen Francisco",
"Sunshiney" => "Sunshiney",
"Supermercado One" => "Supermercado One",
"Suwannaphum" => "Suwannaphum",
"Swanky and Moo Moo" => "Swanky and Moo Moo",
"Syncopate" => "Syncopate",
"Tangerine" => "Tangerine",
"Taprom" => "Taprom",
"Tauri" => "Tauri",
"Telex" => "Telex",
"Tenor Sans" => "Tenor Sans",
"Text Me One" => "Text Me One",
"The Girl Next Door" => "The Girl Next Door",
"Tienne" => "Tienne",
"Tinos" => "Tinos",
"Titan One" => "Titan One",
"Titillium Web" => "Titillium Web",
"Trade Winds" => "Trade Winds",
"Trocchi" => "Trocchi",
"Trochut" => "Trochut",
"Trykker" => "Trykker",
"Tulpen One" => "Tulpen One",
"Ubuntu" => "Ubuntu",
"Ubuntu Condensed" => "Ubuntu Condensed",
"Ubuntu Mono" => "Ubuntu Mono",
"Ultra" => "Ultra",
"Uncial Antiqua" => "Uncial Antiqua",
"Underdog" => "Underdog",
"Unica One" => "Unica One",
"UnifrakturCook" => "UnifrakturCook",
"UnifrakturMaguntia" => "UnifrakturMaguntia",
"Unkempt" => "Unkempt",
"Unlock" => "Unlock",
"Unna" => "Unna",
"VT323" => "VT323",
"Vampiro One" => "Vampiro One",
"Varela" => "Varela",
"Varela Round" => "Varela Round",
"Vast Shadow" => "Vast Shadow",
"Vibur" => "Vibur",
"Vidaloka" => "Vidaloka",
"Viga" => "Viga",
"Voces" => "Voces",
"Volkhov" => "Volkhov",
"Vollkorn" => "Vollkorn",
"Voltaire" => "Voltaire",
"Waiting for the Sunrise" => "Waiting for the Sunrise",
"Wallpoet" => "Wallpoet",
"Walter Turncoat" => "Walter Turncoat",
"Warnes" => "Warnes",
"Wellfleet" => "Wellfleet",
"Wendy One" => "Wendy One",
"Wire One" => "Wire One",
"Yanone Kaffeesatz" => "Yanone Kaffeesatz",
"Yellowtail" => "Yellowtail",
"Yeseva One" => "Yeseva One",
"Yesteryear" => "Yesteryear",
"Zeyada" => "Zeyada",
);

$wp_customize->remove_section( 'static_front_page' );
$wp_customize->remove_section( 'colors' );

// Styles

$wp_customize->add_panel( 'styles', array(
    'title' => esc_html__( 'Styles', 'gotham' ),
    'priority' => 1,
) );

$wp_customize->add_section( 'styles' , array(
    'title'      => esc_html__( 'Styles', 'gotham' ),
    'priority'   => 1,
    'panel' => 'styles',
) );

$wp_customize->add_setting( 'primary' , array(
    'default'     => '',
    'transport'   => 'postMessage',
    'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'primary', array(
	'label'        => esc_html__( 'Primary', 'gotham' ),
	'section'    => 'styles',
)));

$wp_customize->add_setting( 'secondary' , array(
    'default'     => '',
    'transport'   => 'postMessage',
    'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'secondary', array(
	'label'        => esc_html__( 'Secondary', 'gotham' ),
	'section'    => 'styles',
)));

/* Logo */
$wp_customize->add_panel( 'logo', array(
    'title' => esc_html__( 'Logo', 'gotham' ),
    'priority' => 2,
) );

$wp_customize->add_section( 'logo' , array(
    'title'      => esc_html__( 'Logo', 'gotham' ),
    'priority'   => 2,
    'panel'      => 'logo',
) );

/* logo dark */
$wp_customize->add_setting(
	new gotham_Customize_Setting_Image_Data(
	$wp_customize,
	'logo_dark' , array(
	'default' => get_theme_mod( 'logo_dark', get_template_directory_uri() . '/img/gotham.svg' ),
    'transport'   => 'postMessage',
    'sanitize_callback' => 'esc_url_raw',
) ) );

$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo_dark', array(
	'label'        => esc_html__( 'Logo dark', 'gotham' ),
	'section'    => 'logo',
	'extensions' => array( 'jpg', 'jpeg', 'gif', 'png', 'svg' ),
) ) );

$wp_customize->add_setting(
	new gotham_Customize_Setting_Image_Data(
	$wp_customize,
	'logo_retina_dark', array(
    'default'     => get_theme_mod( 'logo_retina_dark' ),
    'transport'   => 'postMessage',
    'sanitize_callback' => 'esc_url_raw',
) ) );

$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo_retina_dark', array(
	'label'        => esc_html__( 'Logo retina dark HD (@2x)', 'gotham' ),
	'section'    => 'logo',
	'extensions' => array( 'jpg', 'jpeg', 'gif', 'png', 'svg' ),
) ) );

/* logo light */
$wp_customize->add_setting(
	new gotham_Customize_Setting_Image_Data(
	$wp_customize,
	'logo_light' , array(
    'default'     => get_theme_mod( 'logo_light', get_template_directory_uri() . '/img/gotham-light.svg' ),
    'transport'   => 'postMessage',
    'sanitize_callback' => 'esc_url_raw',
) ) );

$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo_light', array(
	'label'        => esc_html__( 'Logo light', 'gotham' ),
	'section'    => 'logo',
	'extensions' => array( 'jpg', 'jpeg', 'gif', 'png', 'svg' ),
) ) );

$wp_customize->add_setting(
	new gotham_Customize_Setting_Image_Data(
	$wp_customize,
	'logo_retina_light' , array(
    'default'     => get_theme_mod( 'logo_retina_light' ),
    'transport'   => 'postMessage',
    'sanitize_callback' => 'esc_url_raw',
) ) );

$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo_retina_light', array(
	'label'        => esc_html__( 'Logo retina light HD (@2x)', 'gotham' ),
	'section'    => 'logo',
	'extensions' => array( 'jpg', 'jpeg', 'gif', 'png', 'svg' ),
) ) );

$wp_customize->add_setting( 'logo_height' , array(
    'default'     => '',
    'transport'   => 'postMessage',
    'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'logo_height', array(
	'label'        => esc_html__( 'Logo height', 'gotham' ),
	'section'    => 'logo',
) );

/* Body */
$wp_customize->add_panel( 'body', array(
    'title' => esc_html__( 'Body', 'gotham' ),
    'priority' => 3,
) );
$wp_customize->add_section( 'body' , array(
    'title'      => esc_html__( 'Body', 'gotham' ),
    'priority'   => 3,
    'panel'      => 'body',
) );

$wp_customize->add_setting( 'boxed_layout' , array(
    'default'     => 'no',
    'transport'   => 'refresh',
    'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'boxed_layout', array(
	'label'        => esc_html__( 'Boxed layout', 'gotham' ),
	'section'    => 'body',
	'type' => 'select',
	'choices' => array('no' => 'no', 'yes' => 'yes'),
) );

$wp_customize->add_setting( 'body_bgcolor' , array(
    'default'     => '',
    'transport'   => 'postMessage',
    'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'body_bgcolor', array(
'label'        => esc_html__( 'Body background color', 'gotham' ),
'section'    => 'body',
)));

$wp_customize->add_setting( 'content_bgcolor' , array(
    'default'     => '',
    'transport'   => 'postMessage',
    'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'content_bgcolor', array(
	'label'        => esc_html__( 'Content background color', 'gotham' ),
	'section'    => 'body',
)));

$wp_customize->add_setting(
	new gotham_Customize_Setting_Image_Data(
	$wp_customize,
	'bg_image' , array(
    'default'     => get_theme_mod( 'bg_image', '' ),
    'transport'   => 'postMessage',
    'sanitize_callback' => 'esc_url_raw',
) ) );

$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'bg_image', array(
	'label'        => esc_html__( 'Background image', 'gotham' ),
	'section'    => 'body',
)));

$wp_customize->add_setting(
	new gotham_Customize_Setting_Image_Data(
	$wp_customize,
	'bg_image_pattern' , array(
    'default'     => get_theme_mod( 'bg_image_pattern', '' ),
    'transport'   => 'postMessage',
    'sanitize_callback' => 'esc_url_raw',
) ) );

$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'bg_image_pattern', array(
	'label'        => esc_html__( 'Background image (pattern)', 'gotham' ),
	'section'    => 'body',
)));

/* change default section Site title & tagline */
$wp_customize->add_panel( 'title_tagline', array(
    'title' => esc_html__( 'Site Title & Tagline', 'gotham' ),
) );
$wp_customize->get_section( 'title_tagline' )->panel = 'title_tagline';

/* Navigation */

/* add a section at the menu */
$wp_customize->add_section( 'nav_menus' , array(
    'title'      => esc_html__( 'Nav', 'gotham' ),
    'panel'      => 'nav_menus',
) );

$wp_customize->add_setting(
	new gotham_Customize_Setting_Image_Data(
	$wp_customize,
	'nav_bgimage' , array(
    'default'     => get_theme_mod( 'nav_bgimage', '' ),
    'transport'   => 'refresh',
    'sanitize_callback' => 'esc_url_raw',
) ) );

$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'nav_bgimage', array(
	'label'        => esc_html__( 'Menu background image', 'gotham' ),
	'section'    => 'nav_menus',
)));

$wp_customize->add_setting( 'twi_url' , array(
    'default'     => '',
    'transport'   => 'refresh',
    'sanitize_callback' => 'esc_url',
) );

$wp_customize->add_control( 'twi_url', array(
	'label'        => esc_html__( 'Twitter url', 'gotham' ),
	'section'    => 'nav_menus',
	'type' => 'text',
) );

$wp_customize->add_setting( 'g+_url' , array(
    'default'     => '',
    'transport'   => 'refresh',
    'sanitize_callback' => 'esc_url',
) );

$wp_customize->add_control( 'g+_url', array(
	'label'        => esc_html__( 'Google+ url', 'gotham' ),
	'section'    => 'nav_menus',
	'type' => 'text',
) );

$wp_customize->add_setting( 'fac_url' , array(
    'default'     => '',
    'transport'   => 'refresh',
    'sanitize_callback' => 'esc_url',
) );

$wp_customize->add_control( 'fac_url', array(
	'label'        => esc_html__( 'Facebook url', 'gotham' ),
	'section'    => 'nav_menus',
	'type' => 'text',
) );

/* Footer */
$wp_customize->add_panel( 'footer', array(
    'title' => esc_html__( 'Footer', 'gotham' ),
    'priority' => 4,
    'active_callback' => 'crispy_cust_show_footer',
) );

$wp_customize->add_section( 'footer' , array(
    'title'      => esc_html__( 'Footer', 'gotham' ),
    'priority'   => 4,
    'panel'      => 'footer',
) );

$wp_customize->add_setting( 'enable_footer' , array(
    'default'     => 'yes',
    'transport'   => 'refresh',
    'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'enable_footer', array(
	'label'        => esc_html__( 'Enable footer', 'gotham' ),
	'section'    => 'footer',
	'type' => 'select',
	'choices' => array('yes' => 'yes', 'no' => 'no'),
) );

$wp_customize->add_setting(
	new gotham_Customize_Setting_Image_Data(
	$wp_customize,
	'footer_logo' , array(
    'default'     => get_theme_mod('footer_logo', get_template_directory_uri() . '/img/got-footer.svg'),
    'transport'   => 'postMessage',
    'sanitize_callback' => 'esc_url_raw',
) ) );

$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'footer_logo', array(
	'label'        => esc_html__( 'Logo', 'gotham' ),
	'section'    => 'footer',
) ) );

$wp_customize->add_setting(
	new gotham_Customize_Setting_Image_Data(
	$wp_customize,
	'footer_logo_retina' , array(
    'default'     => get_theme_mod('footer_logo_retina'),
    'transport'   => 'postMessage',
    'sanitize_callback' => 'esc_url_raw',
) ) );

$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'footer_logo_retina', array(
	'label'        => esc_html__( 'Logo retina HD (@2x)', 'gotham' ),
	'section'    => 'footer',
) ) );

$wp_customize->add_setting( 'footer_text' , array(
    'default'     => 'Put Your Text Here',
    'transport'   => 'refresh',
    'sanitize_callback' => 'esc_textarea',
) );

$wp_customize->add_control( 'footer_text', array(
	'label'        => esc_html__( 'Text', 'gotham' ),
	'type' => 'textarea',
	'section'    => 'footer',
));

$wp_customize->add_setting( 'enable_subfooter' , array(
    'default'     => 'yes',
    'transport'   => 'refresh',
    'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'enable_subfooter', array(
	'label'        => esc_html__( 'Enable subfooter', 'gotham' ),
	'section'    => 'footer',
	'type' => 'select',
	'choices' => array('yes' => 'yes', 'no' => 'no'),
) );

$copyright_default = '&copy; Gotham 2016 | Crafted by ThemeCrispy';

$wp_customize->add_setting( 'copyright' , array(
    'default'     => $copyright_default,
    'transport'   => 'refresh',
    'sanitize_callback' => 'esc_textarea',
) );

$wp_customize->add_control( 'copyright', array(
	'label'        => esc_html__( 'Copyright', 'gotham' ),
	'type' => 'textarea',
	'section'    => 'footer',
));

/* Font options */
$wp_customize->add_panel( 'fonts', array(
    'title' => esc_html__( 'Font options', 'gotham' ),
    'priority' => 5,
) );

$wp_customize->add_section( 'general_font_options' , array(
    'title'      => esc_html__( 'General font', 'gotham' ),
    'priority'   => 5,
    'panel'      => 'fonts',
) );

$wp_customize->add_setting( 'main_font_family' , array(
	'transport'   => 'refresh',
	'type' => 'option',
	'default' => 'Montserrat',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'main_font_family', array(
	'label'        => esc_html__( 'Main font family', 'gotham' ),
	'section'      => 'general_font_options',
	'type'         => 'select',
	'choices' => $google_web_fonts,
));

$wp_customize->add_setting( 'secondary_font_family' , array(
	'transport'   => 'refresh',
	'type' => 'option',
	'default' => 'Crimson Text',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'secondary_font_family', array(
	'label'        => esc_html__( 'Secondary font family', 'gotham' ),
	'section'      => 'general_font_options',
	'type'         => 'select',
	'choices' => $google_web_fonts,
));

$wp_customize->add_section( 'pgtitle_font_options' , array(
    'title'      => esc_html__( 'Title area font', 'gotham' ),
    'description' => '',
    'panel'      => 'fonts',
) );

$wp_customize->add_setting( 'pgtitle_font_size' , array(
    'transport'   => 'postMessage',
    'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'pgtitle_font_size', array(
	'label'        => esc_html__( 'Font size', 'gotham' ),
	'section'      => 'pgtitle_font_options',
	'type'         => 'text',
));

$wp_customize->add_setting( 'pgtitle_font_weight' , array(
    'default'     => '',
    'transport'   => 'postMessage',
    'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'pgtitle_font_weight', array(
	'label'        => esc_html__( 'Font weight', 'gotham' ),
	'section'    => 'pgtitle_font_options',
	'type' => 'select',
	'choices' => array(''=>'', '100' => '100', '200' => '200', '300' => '300', '400' => '400', '500' => '500', '600' => '600', '700' => '700', '800' => '800', '900' => '900'),
) );

$wp_customize->add_section( 'post_font_options' , array(
    'title'      => esc_html__( 'Post font', 'gotham' ),
    'panel'      => 'fonts',
    'active_callback' => 'crispy_cust_is_p',
) );

$wp_customize->add_setting( 'post_font_size' , array(
    'transport'   => 'postMessage',
    'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'post_font_size', array(
	'label'        => esc_html__( 'Content font size', 'gotham' ),
	'section'      => 'post_font_options',
	'type'         => 'text',
));

/* Posts */
$wp_customize->add_panel( 'posts', array(
    'title' => esc_html__( 'Posts', 'gotham' ),
    'priority' => 5,
    'active_callback' => 'crispy_cust_is_p',
) );
$wp_customize->add_section( 'posts' , array(
    'title'      => esc_html__( 'Posts', 'gotham' ),
    'priority'   => 5,
    'panel'      => 'posts',
) );

$wp_customize->add_setting( 'post_like' , array(
'transport'   => 'refresh',
'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'post_like', array(
	'label'        => esc_html__( 'Post like', 'gotham' ),
	'section'      => 'posts',
	'type'         => 'select',
	'choices' => array('no' => 'no', 'yes' => 'yes'),
));

$wp_customize->add_setting( 'button_share' , array(
'transport'   => 'refresh',
'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'button_share', array(
	'label'        => esc_html__( 'Button share', 'gotham' ),
	'section'      => 'posts',
	'type'         => 'select',
	'choices' => array('no' => 'no', 'yes' => 'yes'),
));

$wp_customize->add_setting( 'highlight_share' , array(
'transport'   => 'refresh',
'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'highlight_share', array(
	'label'        => esc_html__( 'Highlight share', 'gotham' ),
	'section'      => 'posts',
	'type'         => 'select',
	'choices' => array('no' => 'no', 'yes' => 'yes'),
));

$wp_customize->add_setting( 'upost_return_to' , array(
    'default'     => '',
    'transport'   => 'refresh',
    'sanitize_callback' => 'esc_url',
) );

$wp_customize->add_control( 'upost_return_to', array(
	'label'        => esc_html__( 'Url', 'gotham' ),
	'section'    => 'posts',
	'type' => 'text',
	'description' => 'url for the link (return)',
) );


/* Miscellaneous */
$wp_customize->add_panel( 'miscellaneous', array(
    'title' => esc_html__( 'Miscellaneous', 'gotham' ),
    'priority' => 6,
) );
$wp_customize->add_section( 'miscellaneous' , array(
    'title'      => esc_html__( 'Miscellaneous', 'gotham' ),
    'priority'   => 6,
    'panel'      => 'miscellaneous',
) );

$wp_customize->add_setting( 'mgsearch' , array(
	'transport'   => 'refresh',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'mgsearch', array(
	'label'        => esc_html__( 'Search', 'gotham' ),
	'section'      => 'miscellaneous',
	'type'         => 'select',
	'choices' => array('yes' => 'yes', 'no' => 'no'),
	'description' => 'show search box in the header',
));

/* Before the additionnal css in WordPress 4.7 */
if ( version_compare( $GLOBALS['wp_version'], '4.6.1', '<=' )  || (get_theme_mod('custom_theme_css') != '') ) {
	$wp_customize->add_setting( 'custom_theme_css' , array(
	    'default'     => '',
	    'transport'   => 'refresh',
	    'sanitize_callback' => 'esc_textarea',
	) );

	$wp_customize->add_control( 'custom_theme_css', array(
		'label'        => esc_html__( 'Custom css', 'gotham' ),
		'type' => 'textarea',
		'section'    => 'miscellaneous',
	));
}

$wp_customize->add_setting(
	new gotham_Customize_Setting_Image_Data(
	$wp_customize,
	'err_bg_image' , array(
    'default'     => get_theme_mod( 'err_bg_image', get_template_directory_uri() . '/img/bg-404.jpg'),
    'transport'   => 'postMessage',
    'sanitize_callback' => 'esc_url_raw',
) ) );

$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'err_bg_image', array(
	'label'        => esc_html__( '404 Background image', 'gotham' ),
	'section'    => 'miscellaneous',
)));

/* single portfolio */

$wp_customize->add_panel( 'portfolio', array(
    'title' => esc_html__( 'Portfolio', 'gotham' ),
    'priority' => 5,
    'active_callback' => 'crispy_cust_is_sp',
) );
$wp_customize->add_section( 'portfolio' , array(
    'title'      => esc_html__( 'Portfolio', 'gotham' ),
    'priority'   => 5,
    'panel'      => 'portfolio',
) );

$wp_customize->add_setting( 'title_return_to' , array(
'transport'   => 'refresh',
'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'title_return_to', array(
	'label'        => esc_html__( 'Title', 'gotham' ),
	'section'      => 'portfolio',
	'type'         => 'text',
	'description'  => 'Text for the link (return)',
));

$wp_customize->add_setting( 'url_return_to' , array(
    'default'     => '',
    'transport'   => 'refresh',
    'sanitize_callback' => 'esc_url',
) );

$wp_customize->add_control( 'url_return_to', array(
	'label'        => esc_html__( 'Url', 'gotham' ),
	'section'    => 'portfolio',
	'type' => 'text',
	'description' => 'url for the link (return)',
) );

/* animation layout */
$wp_customize->add_panel( 'anim_layout', array(
    'title' => esc_html__( 'Animation Layout', 'gotham' ),
    'priority' => 7,
) );

$wp_customize->add_section( 'anim_layout' , array(
    'title'      => esc_html__( 'Animation Layout', 'gotham' ),
    'priority'   => 7,
    'panel'      => 'anim_layout',
) );

$wp_customize->add_setting( 'bottom_to_top' , array(
    'default'     => 'yes',
    'transport'   => 'refresh',
    'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bottom_to_top', array(
	'label'        => esc_html__( 'Bottom to top', 'gotham' ),
	'section'    => 'anim_layout',
	'type' => 'select',
	'choices' => array('yes' => 'yes', 'no' => 'no'),
) );

if ( class_exists( 'WooCommerce' ) ) {
	/* WooCommerce */
	$wp_customize->add_panel( 'woocommerce', array(
	    'title' => esc_html__( 'WooCommerce', 'gotham' ),
	    'priority' => 8,
	) );
	$wp_customize->add_section( 'woocommerce' , array(
	    'title'      => esc_html__( 'WooCommerce', 'gotham' ),
	    'priority'   => 8,
	    'panel'      => 'woocommerce',
	) );

	$wp_customize->add_setting( 'header_cart' , array(
		'default'     => 'no',
		'transport'   => 'refresh',
		'sanitize_callback' => 'sanitize_text_field',
	) );

	$wp_customize->add_control( 'header_cart', array(
		'label'        => esc_html__( 'Cart', 'gotham' ),
		'section'      => 'woocommerce',
		'type'         => 'select',
		'choices' => array('no' => 'no', 'yes' => 'yes'),
		'description' => 'show cart in the header on all pages',
	));
}

/* enable live preview */
$wp_customize->get_setting( 'primary' )->transport = 'postMessage';
$wp_customize->get_setting( 'secondary' )->transport = 'postMessage';
$wp_customize->get_setting( 'logo_dark' )->transport = 'postMessage';
$wp_customize->get_setting( 'logo_retina_dark' )->transport = 'postMessage';
$wp_customize->get_setting( 'logo_light' )->transport = 'postMessage';
$wp_customize->get_setting( 'logo_retina_light' )->transport = 'postMessage';
$wp_customize->get_setting( 'logo_height' )->transport = 'postMessage';
$wp_customize->get_setting( 'body_bgcolor' )->transport = 'postMessage';
$wp_customize->get_setting( 'content_bgcolor' )->transport = 'postMessage';
$wp_customize->get_setting( 'bg_image' )->transport = 'postMessage';
$wp_customize->get_setting( 'bg_image_pattern' )->transport = 'postMessage';
$wp_customize->get_setting( 'footer_logo' )->transport = 'postMessage';
$wp_customize->get_setting( 'footer_logo_retina' )->transport = 'postMessage';
$wp_customize->get_setting( 'pgtitle_font_size' )->transport = 'postMessage';
$wp_customize->get_setting( 'pgtitle_font_weight' )->transport = 'postMessage';
$wp_customize->get_setting( 'post_font_size' )->transport = 'postMessage';
$wp_customize->get_setting( 'err_bg_image' )->transport = 'postMessage';


/* post section & post_font_options appear if post type is_singular('post') */
function crispy_cust_is_p() {
	return is_singular('post');
}

/* if this is not portfolio or post */
function crispy_cust_show_footer(){
	if((!is_singular('post'))||(!is_singular('portfolio'))) {
		return(!is_singular('post'))&&(!is_singular('portfolio'));
	}
}

/* portfolio section appear if post type is_singular('portfolio') */

function crispy_cust_is_sp() {
    return is_singular('portfolio');
}


}
add_action( 'customize_register', 'gotham_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function gotham_customize_preview_js() {
	wp_enqueue_script( 'gotham_customizer', get_template_directory_uri() . '/js/customizer.min.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'gotham_customize_preview_js' );

/* Enable svg in media uploader */
if ( ! function_exists( 'gotham_mime_types' ) ) :
function gotham_mime_types( $mimes ){
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
endif;
add_filter( 'upload_mimes', 'gotham_mime_types' );
