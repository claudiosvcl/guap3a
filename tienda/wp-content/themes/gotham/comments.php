<?php
/**
 * The template for displaying comments
 */

if ( post_password_required() )
return;
?>

<div id="comments" class="comments-area">

	<?php if ( have_comments() ) : ?>

	<ul class="comment-list">
		<?php wp_list_comments('type=all&callback=gotham_comment&avatar_size=56'); ?>
	</ul>

	<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
	<nav id="comment-nav-below" class="comment-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php esc_html_e( 'Comment navigation', 'gotham' ); ?></h1>
		<div class="nav-previous"><?php previous_comments_link( esc_html__( '&larr; Older Comments', 'gotham' ) ); ?></div>
		<div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments &rarr;', 'gotham' ) ); ?></div>
	</nav>
	<?php endif; ?>

	<?php endif; ?>

	<?php if ( ! comments_open() && '0' != get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>

	<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'gotham' ); ?></p>
	<?php endif; ?>

	<?php $commenter = wp_get_current_commenter(); $req = get_option( 'require_name_email' ); $aria_req = ( $req ? " aria-required='true'" : '' );
	
	$fields =  array(
		'author' => '<div class="wrap_field"><div class="three-column"><div class="three inner1"><input id="author" name="author" type="text" placeholder="'.esc_attr__( "YOUR NAME", "gotham" ).'" value="' . esc_attr( $commenter['comment_author'] ) .'"' . $aria_req . ' /></div></div>',
		'email' => '<div class="three-column"><div class="three inner2"><input id="email" name="email" type="text" placeholder="'.esc_attr__( "E-MAIL ADDRESS", "gotham" ).'" value="' . esc_attr(  $commenter['comment_author_email'] ) .'"' . $aria_req . ' /></div></div>',
		'url' => '<div class="three-column"><div class="three inner3"><input id="url" name="url" type="text" placeholder="'.esc_attr__( "WEBSITE", "gotham" ).'" value="' . esc_attr( $commenter['comment_author_url'] ) .'"/></div></div></div>',
	);

	$comments_args = array(
		'fields' =>  $fields,
		'comment_field' =>  '<p class="comment-form-comment"><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true" placeholder="'.esc_attr__( "Your Message", "gotham" ).'">' .'</textarea></p>',
		'title_reply'=> ''.esc_html__( "LEAVE A REPLY", "gotham" ).'',
		'cancel_reply_link' => ''.esc_html__( "Cancel Reply", "gotham" ).'',
		'comment_notes_before' => '',
		'comment_notes_after' => '',
		'label_submit' => ''.esc_attr__( "SUBMIT", "gotham" ).''
	);

	comment_form($comments_args);?>

</div>