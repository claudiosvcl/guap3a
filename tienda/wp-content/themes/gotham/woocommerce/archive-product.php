<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

<?php
$woogetpid = wc_get_page_id('shop'); $title_area = get_post_meta( $woogetpid, 'gotham_select_title_area', true );
$title = get_post_meta( $woogetpid, 'gotham_select_title', true ); $breadcrumb = get_post_meta( $woogetpid, 'gotham_select_breadcrumb', true );
$title_area_parallax = get_post_meta( $woogetpid, 'gotham_title_area_parallax', true ); $subtitle = get_post_meta( $woogetpid, 'gotham_subtitle', true );
$arrow_down = get_post_meta( $woogetpid, 'gotham_arrow_down', true ); $select_sidebar = get_post_meta( $woogetpid, 'gotham_select_sidebar', true );
?>

<?php if ($title_area != "no") {?>
<div class="below_header"> 
	<div class="h-below" <?php if ( $title_area_parallax != '' ){ ?> data-stellar-background-ratio="0.5" <?php } ?>>
		<div class="h-belowrapper">
			<div class="title-wr<?php if (($title_area) == "title_area_center"){ ?> twcenter<?php } ?><?php if (is_page_template('modular.php')){ ?> twpom<?php } ?>">
				<?php if ($title != "no") {?> 
					<h1 class="single-h1 pgs"> <?php woocommerce_page_title(); ?> </h1> 
				<?php } ?>
				<?php if($subtitle != "") {?>
					<div class="subtitle"><?php echo esc_textarea( $subtitle ); ?></div> 
				<?php } ?>
				<?php if($breadcrumb != "") {?>
					<div class="breadcrumbs"><?php woocommerce_breadcrumb(); ?></div> 
				<?php } ?>
				<?php if($arrow_down != "") {?>	
					<i class="md-icon-expand-more stdown"><small class="scr-down">SCROLL</small></i>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<?php } ?>


<?php endif; ?>

<?php
/**
* woocommerce_before_main_content hook.
*
* @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
* @hooked woocommerce_breadcrumb - 20
*/
do_action( 'woocommerce_before_main_content' );
?>

<div class="archive-product">

	<?php
	/**
	* woocommerce_archive_description hook.
	*
	* @hooked woocommerce_taxonomy_archive_description - 10
	* @hooked woocommerce_product_archive_description - 10
	*/
	do_action( 'woocommerce_archive_description' );
	?>

	<?php if ( have_posts() ) : ?>

	<?php woocommerce_product_subcategories(); ?>


	<?php switch ($select_sidebar) {
		case 'sidebar_right': ?>
		<section>
			<div class="column70-30">
				<div class="column70-30 inner1">
					<div class="wr-resc-cator">
						<?php woocommerce_result_count(); ?>
						<?php woocommerce_catalog_ordering(); ?>
					</div>
					<?php woocommerce_product_loop_start(); ?>
					<?php while ( have_posts() ) : the_post(); ?>
					<?php wc_get_template_part( 'content', 'product' ); ?>
					<?php endwhile; // end of the loop. ?>
					<?php woocommerce_product_loop_end(); ?>
					<?php woocommerce_pagination(); ?>
				</div>
				<div class="column70-30 inner2">
					<a href="#" class="more-sdbmb"></a>
					<div id="side_main" class="widget-area">
						<?php if ( (is_active_sidebar( 'main' )) || (is_active_sidebar( 'secondary' )) || (is_active_sidebar( 'third' )) || (is_active_sidebar( 'fourth' )) ) {?>
						<?php wp_reset_postdata(); $sidebar_widget_area = get_post_meta( $woogetpid, 'gotham_sidebar_widget_area', true ); ?>
						<div id="sidebar">
							<?php if (($sidebar_widget_area == 'main_sidebar')||(is_home())) {?>
								<?php dynamic_sidebar('main'); ?>
							<?php }
							elseif ($sidebar_widget_area == 'secondary_sidebar') {?>
								<?php dynamic_sidebar('secondary'); ?>
							<?php }
							elseif ($sidebar_widget_area == 'third_sidebar') {?>
								<?php dynamic_sidebar('third'); ?>
							<?php }
							elseif ($sidebar_widget_area == 'fourth_sidebar') {?>
								<?php dynamic_sidebar('fourth'); ?>
							<?php } ?>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</section>
		<?php break; ?>

		<?php case 'sidebar_left': ?>
		<section>
			<div class="column70-30">
				<div class="column70-30 inner1_left">
					<div class="wr-resc-cator">
						<?php woocommerce_result_count(); ?>
						<?php woocommerce_catalog_ordering(); ?>
					</div>
					<?php woocommerce_product_loop_start(); ?>
					<?php while ( have_posts() ) : the_post(); ?>
					<?php wc_get_template_part( 'content', 'product' ); ?>
					<?php endwhile; // end of the loop. ?>
					<?php woocommerce_product_loop_end(); ?>
					<?php woocommerce_pagination(); ?>
				</div>
				<div class="column70-30 inner2_left">
					<a href="#" class="more-sdbmb"></a>
					<div id="side_main" class="widget-area">
						<?php if ( (is_active_sidebar( 'main' )) || (is_active_sidebar( 'secondary' )) || (is_active_sidebar( 'third' )) || (is_active_sidebar( 'fourth' )) ) {?>
						<?php wp_reset_postdata(); $sidebar_widget_area = get_post_meta( $woogetpid, 'gotham_sidebar_widget_area', true ); ?>
						<div id="sidebar">
							<?php if (($sidebar_widget_area == 'main_sidebar')||(is_home())) {?>
								<?php dynamic_sidebar('main'); ?>
							<?php }
							elseif ($sidebar_widget_area == 'secondary_sidebar') {?>
								<?php dynamic_sidebar('secondary'); ?>
							<?php }
							elseif ($sidebar_widget_area == 'third_sidebar') {?>
								<?php dynamic_sidebar('third'); ?>
							<?php }
							elseif ($sidebar_widget_area == 'fourth_sidebar') {?>
								<?php dynamic_sidebar('fourth'); ?>
							<?php } ?>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</section>
		<?php break; ?>
		<?php default: ?>
		<div class="wr-resc-cator">
			<?php woocommerce_result_count(); ?>
			<?php woocommerce_catalog_ordering(); ?>
		</div>
		<?php woocommerce_product_loop_start(); ?>
		<?php while ( have_posts() ) : the_post(); ?>
		<?php wc_get_template_part( 'content', 'product' ); ?>
		<?php endwhile; // end of the loop. ?>
		<?php woocommerce_product_loop_end(); ?>
		<?php woocommerce_pagination(); ?>
	<?php } ?>

	<?php
	/**
	* woocommerce_after_shop_loop hook.
	*
	* @hooked woocommerce_pagination - 10
	*/
	/*do_action( 'woocommerce_after_shop_loop' );*/
	?>

	<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

	<?php wc_get_template( 'loop/no-products-found.php' ); ?>

	<?php endif; ?>

	<?php
	/**
	* woocommerce_after_main_content hook.
	*
	* @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
	*/
	do_action( 'woocommerce_after_main_content' );
	?>

</div>

<?php get_footer( 'shop' ); ?>
