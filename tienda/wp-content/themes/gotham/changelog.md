# Gotham Changelog

v.2.0.1 December 15, 2016
- Improvement: WooCommerce Ajax Cart
- Update: Visual Composer version 5.0.1
- Update: Slider Revolution version 5.3.1.5
For new users with WordPress 4.7+, Custom Css field in the Customizer is replaced by Additional Css.

v.2.0 October 06, 2016
- Added: WooCommerce support
- Added: Demo site and gothamtheme.wordpress.xml with WooCommerce
- New feature: Shop Slider
- Improvement: <br> tag allowed in text field "About this project"

v.1.2 July 06, 2016
- New demos: 
	- Studio
	- Agency
	- Photographer
- New feature: change the height of the title area in projects
- New feature: Multiple videos in projects
- Update: Slider Revolution version 5.2.6
- Fixed: Element icon and text on mobile
- Google Maps Api compatibility

v.1.1.1 June 14, 2016
- Added: Option show/hide search box in the header
- Added: Envato Market plugin - keep Gotham theme and your plugins up to date
- Update: Slider Revolution version 5.2.5.4

v.1.1 June 10, 2016
- New feature: clients in portfolio slider
- New feature: options to modify "latest" filter
- Improvement: boxed layout in customizer
- Update: Visual Composer version 4.12
- Fixed: issue with portfolio tab on mobile
- Fixed: issue with special characters in portfolio slider and portfolio

v.1.0
- First release