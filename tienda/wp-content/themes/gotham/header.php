<?php
/**
 * The header for our theme
 */
?><!DOCTYPE html>
<!--[if IE 9]> <html class="ie9"> <![endif]-->
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>

<?php if ( get_theme_mod('boxed_layout', 'no') == 'yes' ) { ?>
	<!-- boxed layout -->
	<body <?php body_class('spec'); ?>> 
<?php }
else { ?>
	<!-- layout -->
	<body <?php body_class(); ?>>
<?php } ?>

<header>
	<div class="h-wrap">
		<?php $is_retina = ( isset($_COOKIE["device_pixel_ratio"]) )&&( $_COOKIE["device_pixel_ratio"] > 1 ); ?>
		<?php if(!is_singular( array( 'post', 'portfolio' ))||((is_singular( 'post' ))&&(get_theme_mod('upost_return_to') == ""))) {?>
			<?php if ( (get_theme_mod('logo_light', get_template_directory_uri() . '/img/gotham-light.svg') != "")||(get_theme_mod('logo_retina_light') != "")||(get_theme_mod('logo_dark', get_template_directory_uri() . '/img/gotham.svg') != "")||(get_theme_mod('logo_retina_dark') != "")||is_customize_preview() ) {?>
				<div id="logo">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>"> 
						<?php $header_style = get_post_meta( get_the_ID(), 'gotham_header_style', true ); ?>
						<?php 
						if(class_exists( 'WooCommerce' ) && (is_shop()||is_product_category()||is_product_tag()) ) {
							$header_style = get_post_meta( wc_get_page_id('shop'), 'gotham_header_style', true );
						}
						?>
						<?php if ( ($header_style == 'dark')||($header_style == '')||(is_archive() && ! class_exists( 'WooCommerce' ))||is_search()||is_home()||(is_archive() && class_exists( 'WooCommerce' ) && (!is_shop() && !is_product_category() && !is_product_tag()))||( $header_style == 'dark' || $header_style == '' && class_exists( 'WooCommerce' ) && (is_shop()||is_product_category()||is_product_tag()) ) ) {?>
							<?php if( ($is_retina)&&(get_theme_mod('logo_retina_dark') != "") ) {?>
								<img class="retina_logo" src="<?php echo esc_url(get_theme_mod('logo_retina_dark')); ?>" alt="Logo" />
							<?php }
							elseif (get_theme_mod('logo_dark', get_template_directory_uri() . '/img/gotham.svg') != "") {?>
								<img class="standard_logo" src="<?php echo esc_url(get_theme_mod('logo_dark', get_template_directory_uri() . '/img/gotham.svg')); ?>" alt="Logo" />
							<?php }
							elseif ( (!$is_retina)&&(get_theme_mod('logo_dark', get_template_directory_uri() . '/img/gotham.svg') == "")&&(get_theme_mod('logo_retina_dark')!= "") ) {?>
								<img class="retina_logo" src="<?php echo esc_url(get_theme_mod('logo_retina_dark')); ?>" alt="Logo" />
							<?php } ?>
						<?php } ?>
						
						<?php if ( ($header_style == 'light')&&(!is_archive())&&(!is_search())&&(!is_home())||($header_style == 'light' && class_exists( 'WooCommerce' ) && (is_shop()||is_product_category()||is_product_tag())) ) {?>
							<?php if( ($is_retina)&&(get_theme_mod('logo_retina_light') != "") ) {?>
								<img class="retina_logo" src="<?php echo esc_url(get_theme_mod('logo_retina_light')); ?>" alt="Logo" />
							<?php }
							elseif (get_theme_mod('logo_light', get_template_directory_uri() . '/img/gotham-light.svg') != "") {?>
								<img class="standard_logo" src="<?php echo esc_url(get_theme_mod('logo_light', get_template_directory_uri() . '/img/gotham-light.svg')); ?>" alt="Logo" />
							<?php }
							elseif ( (!$is_retina)&&(get_theme_mod('logo_light', get_template_directory_uri() . '/img/gotham-light.svg') == "")&&(get_theme_mod('logo_retina_light')!= "") ) {?>
								<img class="retina_logo" src="<?php echo esc_url(get_theme_mod('logo_retina_light')); ?>" alt="Logo" />
							<?php } ?>
						<?php } ?>
					</a>
				</div>
			<?php } ?>
		<?php } ?>
		<?php if (class_exists( 'WooCommerce' )) {
			gotham_header_cart();
		} ?>
		<?php if(get_theme_mod('mgsearch', 'yes') != 'no') {
			get_search_form(); 
		} ?>
		<?php if (has_nav_menu('primary')) { ?>
			<nav id="menu" class="main-nav">
				<a href="#!" class="menu-trigger"><span></span></a>
				<span class="uihmenu"></span>
				<div class="navigation">
					<div class="menu-bg">
						<h3 class="site-titlem">
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo get_bloginfo();?></a>
						</h3>
					</div>
					<div class="menu-wrap">
							<?php $walker = new gotham_menu; ?>
							<?php wp_nav_menu( array( 
								'theme_location' => 'primary',
								'menu_class' => 'nav-menu',
								'depth' => '2',
								'walker' => $walker,
							)); ?>
						
						<?php if( (get_theme_mod('twi_url') != '')||(get_theme_mod('fac_url') != '')||(get_theme_mod('g+_url') != '') ) {?>
							<div id="icon">
								<?php if(get_theme_mod('twi_url') != '') {?>
									<a href="<?php echo esc_url(get_theme_mod('twi_url'));?>"><i class="iconfa-twitter"></i></a>
								<?php } ?>
								<?php if(get_theme_mod('g+_url') != '') {?>
									<a href="<?php echo esc_url(get_theme_mod('g+_url'));?>"><i class="iconfa-gplus"></i></a>
								<?php } ?>
								<?php if(get_theme_mod('fac_url') != '') {?>
									<a href="<?php echo esc_url(get_theme_mod('fac_url'));?>"><i class="iconfa-facebook"></i></a>
								<?php } ?>
							</div>
						<?php } ?>
					</div>
				
				</div>
			</nav>
		<?php } ?>
	</div>
</header>