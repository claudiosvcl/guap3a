<?php
/**
 * The sidebar containing the main widget area
 */
?>

<?php wp_reset_postdata(); $sidebar_widget_area = get_post_meta( get_the_ID(), 'gotham_sidebar_widget_area', true ); ?>
<div id="sidebar">
	<?php if (($sidebar_widget_area == 'main_sidebar')||(is_home())) {?>
		<?php dynamic_sidebar('main'); ?>
	<?php }
	elseif ($sidebar_widget_area == 'secondary_sidebar') {?>
		<?php dynamic_sidebar('secondary'); ?>
	<?php }
	elseif ($sidebar_widget_area == 'third_sidebar') {?>
		<?php dynamic_sidebar('third'); ?>
	<?php }
	elseif ($sidebar_widget_area == 'fourth_sidebar') {?>
		<?php dynamic_sidebar('fourth'); ?>
	<?php } ?>
</div>