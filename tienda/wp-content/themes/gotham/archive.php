<?php
/**
 * The template for displaying archive pages
 */

get_header(); ?>

<?php $count = $wp_query->found_posts; ?>

<div class="below_header cat"> 
	<div class="h-below hbidar">
		<div class="h-belowrapper">
			<div class="title-wr twcenter">
				<?php if (is_category()) { ?>
					<h1 class="single-h1 archtitle"><?php single_cat_title(); ?><span><?php esc_html_e( 'Category', 'gotham' ); ?></span></h1>
					<div class="numb-posts"><span class="nbpc"><?php echo $count; ?></span><span class="nbpar"><?php if($count > 1){ esc_html_e('ALL RESULTS', 'gotham'); } else { esc_html_e('ALL RESULT', 'gotham'); } ?></span></div>
				<?php } ?>
				<?php if (is_tag()) { ?>
					<h1 class="single-h1 archtitle"><?php single_cat_title(); ?><span><?php esc_html_e( 'Tag', 'gotham' ); ?></span></h1>
					<div class="numb-posts"><span class="nbpc"><?php echo $count; ?></span><span class="nbpar"><?php if($count > 1){ esc_html_e('ALL RESULTS', 'gotham'); } else { esc_html_e('ALL RESULT', 'gotham'); } ?></span></div>
				<?php } ?>
				<?php if (is_author()) { ?>
					<h1 class="single-h1 author archtitle"><?php echo get_the_author(); ?></h1>
					<div class="author-archive"><?php echo get_avatar( get_the_author_meta( 'ID' ), 128 ); ?>
						<div class="author-social">
							<?php if ( get_the_author_meta( 'twitter' ) ) { ?>
								<p class="s-author-twitter"><a href="http://twitter.com/<?php the_author_meta( 'twitter' ); ?>" title="Follow <?php the_author_meta( 'display_name' ); ?> on Twitter" target="_blank"></a></p>
							<?php } ?>
							<?php if ( get_the_author_meta( 'facebook' ) ) { ?>
								<p class="s-author-facebook"><a href="http://facebook.com/<?php the_author_meta( 'facebook' ); ?>" title="Follow <?php the_author_meta( 'display_name' ); ?> on facebook" target="_blank"></a></p>
							<?php } ?>
							<?php if ( get_the_author_meta( 'github' ) ) { ?>
								<p class="s-author-github"><a href="http://github.com/<?php the_author_meta( 'github' ); ?>" title="Follow <?php the_author_meta( 'display_name' ); ?> on github" target="_blank"></a></p>
							<?php } ?>
							<?php if ( get_the_author_meta( 'dribbble' ) ) { ?>
								<p class="s-author-dribbble"><a href="http://dribbble.com/<?php the_author_meta( 'dribbble' ); ?>" title="Follow <?php the_author_meta( 'display_name' ); ?> on dribbble" target="_blank"></a></p>
							<?php } ?>
						</div>
						<?php if ( get_the_author_meta( 'designation' ) ) { ?>
							<p class="s-author-designation"><?php the_author_meta('designation'); ?></p>
						<?php } ?>
						<p class="s-author-desc"><?php echo get_the_author_meta('description'); ?></p>
						<?php if ( get_the_author_meta( 'user_url' ) ) { ?>
						<p class="s-author-link"><?php esc_html_e( 'Website: ', 'gotham' );?><a href="<?php echo get_the_author_meta('user_url'); ?>" target="_blank"><?php echo get_the_author_meta('user_url'); ?></a></p>
						<?php } ?>
						<div class="numb-posts"><span class="nbpc"><?php echo $count; ?></span><span class="nbpar"><?php if($count > 1){ esc_html_e('ALL RESULTS', 'gotham'); } else { esc_html_e('ALL RESULT', 'gotham'); } ?></span></div>
					</div>
				<?php } ?>
				<?php if (is_date()) { ?>
					<h1 class="single-h1 archtitle"><?php single_month_title(' '); ?><span><?php esc_html_e( 'Month', 'gotham' ); ?></span></h1>
					<div class="numb-posts"><span class="nbpc"><?php echo $count; ?></span><span class="nbpar"><?php if($count > 1){ esc_html_e('ALL RESULTS', 'gotham'); } else { esc_html_e('ALL RESULT', 'gotham'); } ?></span></div>
				<?php } ?>

				<?php
				if( post_type_exists ('portfolio')){
					/* portfolio category */
					$pcategory = wp_get_post_terms(get_the_ID(), 'portfolio_category'); 
					if ($pcategory) { ?>
					<h1 class="single-h1 archtitle"><?php echo $pcategory[0]->name; ?><span><?php esc_html_e( 'Portfolio Category', 'gotham' ); ?></span></h1>
					<div class="numb-posts"><span class="nbpc"><?php echo $count; ?></span><span class="nbpar"><?php if($count > 1){ esc_html_e('ALL RESULTS', 'gotham'); } else { esc_html_e('ALL RESULT', 'gotham'); } ?></span></div>
				<?php } ?>
				<?php } ?>
			</div>
		</div>
	</div>
</div>

<?php if ( have_posts() ) : ?>
<div id="content" class="spec indasd">
	<section class="iasec">
		<ul>

		<?php while ( have_posts() ) : the_post(); ?>

		<?php
		$thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ) , 'full' );
		$video = esc_url( get_post_meta( get_the_ID(), 'gotham_video_post', 1 ) ); $quote = get_post_meta( get_the_ID(), 'gotham_quote_post', true ); $author_quote = get_post_meta( get_the_ID(), 'gotham_author_quote_post', true ); $audio = esc_url( get_post_meta( get_the_ID(), 'gotham_audio_post', 1 ) ); $link = get_post_meta( get_the_ID(), 'gotham_link_post', true ); $link_text = get_post_meta( get_the_ID(), 'gotham_link_text', true ); $the_title_Post = ''; 
		$title_Post = get_the_title();

		if ($title_Post != "") {
			if (strlen($title_Post) > 37) {
				$the_title_Post = substr($title_Post,0,37).'...';
			}
			else {
				$the_title_Post=$title_Post;
			}
		}

		$lcategory = get_the_category();
		?>
			<li class="blog-big-item all">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php if ( ($video != "") && (has_post_format("video")) ) { ?> 
						<div class="video_post">
						<?php echo wp_oembed_get( $video ); ?>
						</div>
					<?php } 
					elseif  ( ($quote != "") && (has_post_format("quote")) ) { ?>
						<div class="wrap-icon-text">
							<i class="md-icon-quote"></i>
							<?php echo gotham_getPostLikeLink(get_the_ID());?>
							<a href="<?php the_permalink() ?>">
								<div class="text_citation">
									<p><?php echo esc_html( $quote ); ?></p>
									<p><?php echo esc_html( $author_quote ); ?></p>
								</div>
							</a>
							<div class="links lquote"><a href="<?php the_permalink() ?>"></a></div>
						</div>
					<?php } 
					elseif ( ($audio != "") && (has_post_format("audio")) ) { ?>
						<div class="audio_post">
							<?php echo wp_oembed_get( $audio ); ?>
						</div>
					<?php } 
					elseif ( ($link != "") && (has_post_format("link")) ) { ?>
						<div class="wrap-icon-text">
							<i class="md-icon-link"></i>
							<?php echo gotham_getPostLikeLink(get_the_ID());?>
							<a href="<?php the_permalink() ?>">
								<div class="text_link">
									<p><?php echo esc_html( $link_text ); ?></p>
								</div>
							</a>
							<div class="links"><a href="<?php the_permalink() ?>"></a></div>
							<a href="<?php echo esc_url( $link ); ?>">
								<div class="text_link">
								<span><?php echo esc_html( $link ); ?></span>
								</div>
							</a>
						</div>
					<?php } 
					else { ( (has_post_thumbnail()) && ((has_post_format("link")or(has_post_format("audio")or(has_post_format("quote")or(has_post_format("video") ) )))== "") ) ?>
						<a href="<?php the_permalink() ?>"  title="<?php the_title_attribute(); ?>">
							<div class="fitbgimg" style="background-image:url('<?php echo $thumbnail_src[0]; ?>')"></div>
						</a>
					<?php } ?>

					<?php if( (has_post_format("link")or(has_post_format("quote")))== "" ) {?>
						<div class="blog-thumb">
							<?php echo gotham_getPostLikeLink(get_the_ID());?>
							<a class="more-description"></a>
							<h3><a href="<?php the_permalink() ?>"><?php echo $the_title_Post; ?></a></h3>
							<div class="athcat">
								<p class="author"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php echo get_avatar( get_the_author_meta( 'ID' ), 20 ); ?> <?php the_author(); ?></a></p>
								<span><?php esc_html_e('in', 'gotham'); ?></span>
								<?php if ($lcategory){?><div class="category"><a href="<?php echo get_category_link($lcategory[0]);?>"><?php echo $lcategory[0]->cat_name; ?></a></div><?php } ?>
								<?php if ($pcategory){?><div class="category"><?php echo $pcategory[0]->name; ?></div><?php } ?>
							</div>
							<p class="excerpt"><a href="<?php the_permalink() ?>"><?php echo gotham_custom_excerpt_length(); ?></a></p>
							<div class="links"><a href="<?php the_permalink() ?>"></a></div>
							<p class="date"><a href="<?php the_permalink() ?>"><?php echo get_the_date(); ?></a></p>
							<?php if( (has_post_format("audio")or(has_post_format("video")))== "" ){?>
								<p class="comment"><?php echo gotham_comments_number(); ?></p>
							<?php } ?>	
						</div>
					<?php } ?>
				</article>
			</li>
		<?php endwhile;?> 
		</ul>
	</section>
</div>

<?php get_template_part('content/pagination'); ?>
<?php else: ?>
<div id="content">
	<section class="spec">
		<?php get_template_part('content/error'); ?>
	</section>
</div>
<?php endif; ?>
<?php get_footer(); ?>