<?php 
/**
 * The template for displaying all single posts
 */

get_header(); ?>

<?php if(! post_password_required()) {?>

<?php
$video = esc_url( get_post_meta( get_the_ID(), 'gotham_video_post', 1 ) ); $quote = get_post_meta( get_the_ID(), 'gotham_quote_post', true ); $author_quote = get_post_meta( get_the_ID(), 'gotham_author_quote_post', true ); $audio = esc_url( get_post_meta( get_the_ID(), 'gotham_audio_post', 1 ) ); $link = get_post_meta( get_the_ID(), 'gotham_link_post', true ); $link_text = get_post_meta( get_the_ID(), 'gotham_link_text', true );
$description = get_post_meta( get_the_ID(), 'gotham_select_description', true ); $title_area_parallax = get_post_meta( get_the_ID(), 'gotham_title_area_parallax', true ); $title_area_background_image = get_post_meta( get_the_ID(), 'gotham_title_area_background_image', true ); $title_area = get_post_meta( get_the_ID(), 'gotham_select_title_area', true );
?>

<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
<?php if (get_theme_mod('upost_return_to') != "") {?>
	<a href="<?php echo esc_url(get_theme_mod('upost_return_to'));?>" class="uihpost"></a>
<?php } ?>
<div class="single-bh"> 
	<div class="single-screen" <?php if ( $title_area_parallax != '' ){ ?> data-stellar-background-ratio="0.5" <?php } ?>>
		<div class="single-details">
			<div class="single-thumb<?php if ($title_area == "title_area_center"){ ?> stcenter<?php } ?>">
				<h1 class="single-title"><?php the_title(); ?></h1>
				<?php if($description != "no") {?>
					<div class="single-description">
						<p class="single-author"><a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) )); ?>"><?php echo get_avatar( get_the_author_meta( 'ID' ), 32 ); ?><?php the_author(); ?></a></p>
						<p class="single-date"><?php echo esc_html(get_the_date()); ?></p>
						<p class="single-comment"><a href="<?php comments_link(); ?>"><?php comments_number(); ?></a></p>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>

<div id="content" class="single-content">
	<?php 
	$tags = wp_get_post_tags($post->ID); $nextPost = get_next_post();
	if($tags) {
		$tags_count = $tags[0]->count > 1;
	}
	else {
		$tags_count = '';
	}
	?>
	<?php if ( (get_theme_mod('button_share') == "yes")&&($title_area_parallax != '' )||($title_area_background_image != '')||($tags_count)||($nextPost != "") ) {?>
		<div class="buttonsh-wrap<?php if( ($title_area_parallax == '' )&&($title_area_background_image == '')||(has_post_format("video") )&&($title_area_parallax == '' )&&($title_area_background_image == '')||(has_post_format("quote") )&&($title_area_parallax == '' )&&($title_area_background_image == '')||(has_post_format("audio") )&&($title_area_parallax == '' )&&($title_area_background_image == '')||(has_post_format("link") )&&($title_area_parallax == '' )&&($title_area_background_image == '') ){?> btshbtm<?php } ?>">
			<div class="buttonsh"></div>
			<div class="social-sharing">
				<h3> <?php esc_html_e('Share', 'gotham'); ?> </h3>
				<div class="buttontwi"><?php esc_html_e('Twitter', 'gotham'); ?></div>
				<div class="buttonfac"><?php esc_html_e('Facebook', 'gotham'); ?></div>
				<div class="buttonemail"><?php esc_html_e('Email', 'gotham'); ?></div>
			</div>
		</div>
	<?php } ?>

		 
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php if ( ( $video != "" ) && (has_post_format("video")) ) {?> 
			<div class="video_post">
				<?php echo wp_oembed_get( $video ); ?>
			</div>
		<?php }
		elseif ( ( $quote != "" ) && (has_post_format("quote")) ) {?>
			<div class="wrap-icon-text">
				<i class="md-icon-quote"> </i> 
				<div class="text_citation">
					<p><?php echo esc_html( $quote ); ?></p>
					<p><?php echo esc_html( $author_quote ); ?></p>
				</div>
			</div>
		<?php }
		elseif ( ( $audio != "" ) && (has_post_format("audio")) ) {?>
			<div class="audio_post">
				<?php echo wp_oembed_get( $audio ); ?>
			</div>
		<?php }
		elseif ( ( $link != "" ) && (has_post_format("link")) ) {?>
			<div class="wrap-icon-text">
				<i class="md-icon-link"></i> 
				<a href="<?php echo esc_url( $link ); ?>">
					<div class="text_link">
						<p><?php echo esc_html( $link_text ); ?></p>
					</div>
				</a>
				<a href="<?php echo esc_url( $link ); ?>">
					<div class="text_link2">
						<span><?php echo esc_html( $link ); ?></span>
					</div>
				</a>
			</div>
		<?php } ?>

		<div class="post-content">
			<?php the_content(); ?>
		</div>
		<?php wp_link_pages(array('before' => '<div class="link_pages"><ul>', 'after' => '</ul></div>', 'pagelink' => '<span>%</span>')); ?>
	</article> 
	<?php endwhile; endif; ?>
	<?php if (get_the_author_meta('description') != ""){ ?>
		<div class="s-author-info">
			<p class="s-author-ava"><a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) )); ?>"><?php echo get_avatar( get_the_author_meta( 'ID' ), 100 ); ?></a></p>
			<div class="s-author-wrap">
				<p class="s-author-name"><a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) )); ?>"><?php the_author(); ?></a></p>
				<?php if ( get_the_author_meta( 'designation' ) ) {?>
					<p class="s-author-designation"><?php the_author_meta('designation'); ?></p>
				<?php } ?>
				<p class="s-author-desc"><?php echo esc_html(get_the_author_meta('description')); ?></p>
				<?php if (get_the_author_meta('user_url') ) {?>
					<p class="s-author-link"><?php esc_html_e( 'Website: ', 'gotham' );?><a href="<?php echo esc_url(get_the_author_meta('user_url')); ?>" target="_blank"><?php echo esc_html(get_the_author_meta('user_url')); ?></a></p>
				<?php } ?>
				<div class="s-author-social">
					<?php if ( get_the_author_meta( 'twitter' ) ) {?>
						<p class="s-author-twitter"><a href="http://twitter.com/<?php the_author_meta( 'twitter' ); ?>" title="Follow <?php the_author_meta( 'display_name' ); ?> on Twitter" target="_blank"></a></p>
					<?php } ?>
					<?php if ( get_the_author_meta( 'facebook' ) ) {?>
						<p class="s-author-facebook"><a href="http://facebook.com/<?php the_author_meta( 'facebook' ); ?>" title="Follow <?php the_author_meta( 'display_name' ); ?> on facebook" target="_blank"></a></p>
					<?php } ?>
					<?php if ( get_the_author_meta( 'github' ) ) {?>
						<p class="s-author-github"><a href="http://github.com/<?php the_author_meta( 'github' ); ?>" title="Follow <?php the_author_meta( 'display_name' ); ?> on github" target="_blank"></a></p>
					<?php } ?>
					<?php if ( get_the_author_meta( 'dribbble' ) ) {?>
						<p class="s-author-dribbble"><a href="http://dribbble.com/<?php the_author_meta( 'dribbble' ); ?>" title="Follow <?php the_author_meta( 'display_name' ); ?> on dribbble" target="_blank"></a></p>
					<?php } ?>
				</div>
			</div>
		</div>
	<?php } ?>

	<?php if ( get_theme_mod('post_like') == 'yes' ) {?>
		<?php echo gotham_getPostLikeLink(get_the_ID());?>
	<?php } ?>
	<p class="single-tags"><?php the_tags('', ' '); ?></p>
	<div class="single-category"><?php the_category(); ?></div>
	<?php if ( comments_open() || get_comments_number() ) {?>
		<div class="comments_number">
			<?php comments_number(); ?>
		</div> 
		<?php comments_template(); ?>
	<?php } ?>

</div><!-- end content -->

<?php if( ($tags_count)||($nextPost != "") ) {?><!-- if tags or nextPost -->
	<div class="s-np-rp">
		<?php if($tags_count) {?>
			<div class="s-related-posts <?php if ($nextPost == ""){?> bsrpost<?php } ?>">
				<div class="relatedposts">
					<?php $tags = wp_get_post_tags($post->ID);
					if ($tags) {
						$tag_ids = array();
						foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
						$args = array(
							'tag__in' => $tag_ids,
							'post__not_in' => array($post->ID),
							'posts_per_page' => 1, // Number of related posts to display.
						);

						$my_query = new WP_Query( $args );

						while( $my_query->have_posts() ) {
							$my_query->the_post();
							?>

							<?php $rpauthor = get_avatar( get_the_author_meta( 'ID' ), 36 ); ?>
							<?php $rpauthorname = $post->post_author; ?>
							<?php $rpdate = get_the_date('Y-m-d', $post->ID); ?>
							<h3 class="s-npti"> <?php esc_html_e('Others Posts', 'gotham'); ?> </h3>

							<a rel="external" href="<?php the_permalink();?>">

								<?php $tap = esc_url( get_post_meta( $post->ID, 'gotham_title_area_parallax', true ) ); ?>
								<?php if( ($tap != "") ) {?>
									<div class="srp-bgp">
										<div class="srp-tw">
											<h3 class="srp-tt" style="color:<?php echo get_post_meta( $post->ID, 'gotham_title_area_color', true );?>">
												<?php the_title(); ?>
											</h3>
											<p class="single-author"><?php echo $rpauthor; ?><span style="color:<?php echo get_post_meta( $post->ID, 'gotham_title_area_color', true );?>"><?php esc_html_e( 'by', 'gotham' );?> <?php echo the_author_meta( 'nickname' , $rpauthorname ); ?></span><strong style="color:<?php echo get_post_meta( $post->ID, 'gotham_title_area_color', true );?>"><?php echo $rpdate; ?></strong></p>
											<span class="sp-rp" style="color:<?php echo get_post_meta( $post->ID, 'gotham_title_area_color', true );?>">Related post</span>
											<i class="md-icon-attachment" style="color:<?php echo get_post_meta( $post->ID, 'gotham_title_area_color', true );?>"></i>
										</div>
										<div class="srp-bgpbg" style="background-image: url('<?php echo $tap; ?>');"></div>
									</div>
								<?php } ?>
											
								<?php $tabi = esc_url( get_post_meta( $post->ID, 'gotham_title_area_background_image', true ) ); ?>
								<?php if( ($tabi != "")&&($tap == "") ) {?>
									<div class="srp-bg">
										<div class="srp-tw">
											<h3 class="srp-tt" style="color:<?php echo get_post_meta( $post->ID, 'gotham_title_area_color', true );?>">
												<?php the_title(); ?>
											</h3>
											<p class="single-author"><?php echo $rpauthor; ?><span style="color:<?php echo get_post_meta( $post->ID, 'gotham_title_area_color', true );?>"><?php esc_html_e( 'by', 'gotham' );?> <?php echo the_author_meta( 'nickname' , $rpauthorname ); ?></span><strong style="color:<?php echo get_post_meta( $post->ID, 'gotham_title_area_color', true );?>"><?php echo $rpdate; ?></strong></p>
											<span class="sp-rp" style="color:<?php echo get_post_meta( $post->ID, 'gotham_title_area_color', true );?>">Related post</span>
											<i class="md-icon-attachment" style="color:<?php echo get_post_meta( $post->ID, 'gotham_title_area_color', true );?>"></i>
										</div>
										<div class="srp-bgbg" style="background-image: url('<?php echo $tabi; ?>');"></div>
									</div>
								<?php } ?>

								<?php if( ($tabi == "")&&($tap == "") ) {?>
									<div class="srp-bgc">
										<?php if(get_theme_mod('fifth') != "") {?>
											<div class="s-bgcolor" style="background-color:<?php get_theme_mod('fifth');?>"></div>
										<?php }
										else {?>
											<div class="s-bgcolor"></div>
										<?php } ?>
										<div class="srp-tw">
											<h3 class="srp-tt" style="color:<?php echo get_post_meta( $post->ID, 'gotham_title_area_color', true );?>">
												<?php the_title(); ?>
											</h3>
											<p class="single-author"><?php echo $rpauthor; ?><span style="color:<?php echo get_post_meta( $post->ID, 'gotham_title_area_color', true );?>"><?php esc_html_e( 'by', 'gotham' );?> <?php echo the_author_meta( 'nickname' , $rpauthorname ); ?></span><strong style="color:<?php echo get_post_meta( $post->ID, 'gotham_title_area_color', true );?>"><?php echo $rpdate; ?></strong></p>
											<span class="sp-rp" style="color:<?php echo get_post_meta( $post->ID, 'gotham_title_area_color', true );?>">Related post</span>
											<i class="md-icon-attachment" style="color:<?php echo get_post_meta( $post->ID, 'gotham_title_area_color', true );?>"></i>
										</div>
									</div>
								<?php } ?>
							</a>
						<?php }
					}
					if (!$tags) {?>
						<h3 class="s-nptisp"> <?php esc_html_e('Others Posts', 'gotham'); ?> </h3>
					<?php } ?>

					<?php wp_reset_postdata(); ?>
				</div>
			</div>
		<?php } ?>

		<?php $nextPost = get_next_post(); if ($nextPost != "") {?>
			<?php $nexttitle = $nextPost->post_title; 
			if (strlen($nexttitle) > 42) {
				$shortcut = substr($nexttitle,0,42).'...';
			}
			else {
				$shortcut = $nexttitle;
			}
			?>
			<?php $nextauthor = get_avatar(($nextPost->post_author), 36); $nextauthorname = $nextPost->post_author; $nextdate = get_the_date('Y-m-d', $nextPost->ID); ?>
			<div class="single_next_post<?php if ($tags_count == ""){?> bsnpost<?php } ?>">
				<?php if ($tags_count == ""){?><h3 class="s-nptisp"> <?php esc_html_e('Others Posts', 'gotham'); ?> </h3><?php } ?>

				<?php $nextbackgroundimagep = esc_url( get_post_meta( $nextPost->ID, 'gotham_title_area_parallax', true ) ); ?>
				<?php if (($nextbackgroundimagep != "")) {?>
					<div class="snp-tw">
						<?php next_post_link('%link', ''); ?>
						<h3><?php next_post_link('%link', $shortcut); ?></h3>
						<p class="single-author"><?php echo $nextauthor; ?><span><?php esc_html_e( 'by', 'gotham' );?> <?php echo the_author_meta( 'nickname' , $nextauthorname ); ?></span><strong><?php echo $nextdate; ?></strong></p>
						<div class="rdthp"><?php next_post_link('%link', esc_html__('Read the next post', 'gotham') ); ?></div>
						<i class="md-icon-navigate-next"></i>
					</div>
					<?php $next_dirp = 'style="background-image: url('.$nextbackgroundimagep.');"' ?>
					<?php next_post_link('%link', '<div class="single_next_backgroundimagep" '.$next_dirp.'></div>'); ?>
				<?php } ?>

				<?php $nextbackgroundimage = esc_url( get_post_meta( $nextPost->ID, 'gotham_title_area_background_image', true ) ); ?>
				<?php if (($nextbackgroundimagep == "")&&($nextbackgroundimage != "")) {?>
					<div class="snp-tw">
						<?php next_post_link('%link', ''); ?>
						<h3><?php next_post_link('%link', $shortcut); ?></h3>
						<p class="single-author"><?php echo $nextauthor; ?><span><?php esc_html_e( 'by', 'gotham' );?> <?php echo the_author_meta( 'nickname' , $nextauthorname ); ?></span><strong><?php echo $nextdate; ?></strong></p>
						<div class="rdthp"><?php next_post_link('%link', esc_html__('Read the next post', 'gotham') ); ?></div>
						<i class="md-icon-navigate-next"></i>
					</div>
					<?php $next_dir = 'style="background-image: url('.$nextbackgroundimage.');"' ?>
					<?php next_post_link('%link', '<div class="single_next_backgroundimage" '.$next_dir.'></div>'); ?>
				<?php } ?>

				<?php if (($nextbackgroundimagep == "")&&($nextbackgroundimage == "")) {?>
					<?php $nextbackgroundcolor = get_theme_mod('fifth'); ?>
					<div class="snp-tw">
						<?php next_post_link('%link', ''); ?>
						<h3><?php next_post_link('%link', $shortcut); ?></h3>
						<p class="single-author"><?php echo $nextauthor; ?><span><?php esc_html_e( 'by', 'gotham' );?> <?php echo the_author_meta( 'nickname' , $nextauthorname ); ?></span><strong><?php echo $nextdate; ?></strong></p>
						<div class="rdthp"><?php next_post_link('%link', esc_html__('Read the next post', 'gotham') ); ?></div>
						<i class="md-icon-navigate-next"></i>
					</div>
					<?php if (get_theme_mod('fifth') != "") {?>
						<?php $next_dirc = 'style="background-color: '.$nextbackgroundcolor.';"' ?>
					<?php }
					else {
						$next_dirc = '';
					} ?>
					<?php next_post_link('%link', '<div class="single_next_backgroundcolor" '.$next_dirc.'></div>'); ?>
				<?php } ?>
			</div>
		<?php } ?>

	</div><!-- end <div class="s-np-rp"> -->
<?php } ?><!-- end tags or nextPost -->

<?php if ( get_theme_mod('highlight_share') == 'yes' ) {?>
	<div id="share-button"><button class="iconfa-twitter"></button><button class="iconfa-facebook"></button><span class="ardo"></span></div>
<?php } ?>
<div class="modalmn"></div>

<?php } else { echo get_the_password_form(); }?>

<?php wp_footer();?>
</body>
</html>