<?php
/** 
 * The default template for displaying content
 * in page and modular
 */
?>

<?php $select_sidebar = get_post_meta( get_the_ID(), 'gotham_select_sidebar', true ); ?>

<?php switch ($select_sidebar) {
	case 'sidebar_right': ?>
	<section>
		<div class="column70-30">
			<div class="column70-30 inner1">
				<?php the_content(); ?>
				<?php wp_link_pages(array('before' => '<div class="link_pages"><ul>', 'after' => '</ul></div>', 'pagelink' => '<span>%</span>')); ?>
			</div>
			<div class="column70-30 inner2">
				<a href="#" class="more-sdbmb"></a>
				<div id="side_main" class="widget-area">
				<?php if ( (is_active_sidebar( 'main' )) || (is_active_sidebar( 'secondary' )) || (is_active_sidebar( 'third' )) || (is_active_sidebar( 'fourth' )) ) {?>
				<?php get_sidebar(); ?>
				<?php } ?>
				</div>
			</div>
		</div>
	</section>
	<?php break; ?>

	<?php case 'sidebar_left': ?>
	<section>
		<div class="column70-30">
			<div class="column70-30 inner1_left"> 
				<?php the_content(); ?>
				<?php wp_link_pages(array('before' => '<div class="link_pages"><ul>', 'after' => '</ul></div>', 'pagelink' => '<span>%</span>')); ?>
			</div>
			<div class="column70-30 inner2_left">
				<a href="#" class="more-sdbmb"></a>
				<div id="side_main" class="widget-area">
				<?php if ( (is_active_sidebar( 'main' )) || (is_active_sidebar( 'secondary' )) || (is_active_sidebar( 'third' )) || (is_active_sidebar( 'fourth' )) ) { ?>  
				<?php get_sidebar(); ?>
				<?php } ?>
				</div>
			</div>
		</div>
	</section>
	<?php break; ?>
	<?php default: ?>
	<?php the_content(); ?>
<?php } ?>

<?php wp_link_pages(array('before' => '<div class="link_pages"><ul>', 'after' => '</ul></div>', 'pagelink' => '<span>%</span>')); ?>