<?php 
/**
 * The template for displaying title area
 * in page/modular
 */
?>

<?php
$title_area = get_post_meta( get_the_ID(), 'gotham_select_title_area', true ); $title = get_post_meta( get_the_ID(), 'gotham_select_title', true ); $breadcrumb = get_post_meta( get_the_ID(), 'gotham_select_breadcrumb', true ); $title_area_parallax = get_post_meta( get_the_ID(), 'gotham_title_area_parallax', true ); $subtitle = get_post_meta( get_the_ID(), 'gotham_subtitle', true ); $arrow_down = get_post_meta( get_the_ID(), 'gotham_arrow_down', true );
?>

<?php if ($title_area != "no") {?>
<div class="below_header"> 
	<div class="h-below" <?php if ( $title_area_parallax != '' ){ ?> data-stellar-background-ratio="0.5" <?php } ?>>
		<div class="h-belowrapper">
			<div class="title-wr<?php if (($title_area) == "title_area_center"){ ?> twcenter<?php } ?><?php if (is_page_template('modular.php')){ ?> twpom<?php } ?>">
				<?php if ($title != "no") {?> 
					<h1 class="single-h1 pgs"> <?php the_title(); ?> </h1> 
				<?php } ?>
				<?php if($subtitle != "") {?>
					<div class="subtitle"><?php echo esc_textarea( $subtitle ); ?></div> 
				<?php } ?>
				<?php if($breadcrumb != "") {?>
					<div class="breadcrumbs"><?php gotham_the_breadcrumb(); ?></div> 
				<?php } ?>
				<?php if($arrow_down != "") {?>	
					<i class="md-icon-expand-more stdown"><small class="scr-down">SCROLL</small></i>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<?php } ?>