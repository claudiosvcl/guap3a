<?php 
/**
 * Template for displaying error
 * in archive, index 
*/
?>

<article>
	<h3><?php esc_html_e( 'Nothing Found', 'gotham' ); ?></h3>
	<?php if ( current_user_can( 'publish_posts' ) ) : ?>
	<h4><?php printf( wp_kses( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'gotham' ), array( 'a' => array( 'href' => array() ) ) ), esc_url( admin_url( 'post-new.php' ) ) ); ?></h4>
	<?php endif; ?>
</article>