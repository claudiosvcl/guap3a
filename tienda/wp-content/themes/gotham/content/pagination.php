<?php 
/**
 * The template for displaying pagination
 * in archive/search/index
 */
?>

<?php if (is_archive() || is_search()) : ?>
<div class="wrbnpp">
	<div class="wrpgpb"> <?php previous_posts_link( wp_kses( __('<div class="pagination_pn"><span>PREVIOUS</span></div>', 'gotham'), array( 'div' => array( 'class' => array() ), 'span' => array() ) ), $post->max_num_pages ); ?> </div> <div class="wrpgab"> <?php next_posts_link( wp_kses( __('<div class="pagination_pn"><p>NEXT</p></div>', 'gotham'), array( 'div' => array( 'class' => array() ), 'p' => array() ) ), $post->max_num_pages );?> </div>
</div>
<?php else : ?>
<div class="wrbnpp">
	<div class="wrpgpb"> <?php previous_posts_link( wp_kses( __('<div class="pagination_pn"><span>NEW POSTS</span></div>', 'gotham'), array( 'div' => array( 'class' => array() ) , 'span' => array() ) ), $wp_query->max_num_pages ); ?> </div> <div class="wrpgab"> <?php next_posts_link( wp_kses( __('<div class="pagination_pn"><p>OLDER POSTS</p></div>', 'gotham'), array( 'div' => array( 'class' => array() ), 'p' => array() ) ), $wp_query->max_num_pages );?> </div>
</div>
<?php endif; ?>