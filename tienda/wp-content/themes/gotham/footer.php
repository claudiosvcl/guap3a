<?php
/**
 * The template for displaying the footer
 */
?>
<footer>
	<?php if (get_theme_mod('enable_footer', 'yes') == 'yes'){ ?>
		<div id="footer-wrapper">
			<div class="spec">
				<?php if ( (get_theme_mod('footer_logo', get_template_directory_uri() . '/img/got-footer.svg') != "")||(get_theme_mod('footer_logo_retina') != "")||is_customize_preview() ) {?>
				<div id="footer_logo">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>"> 
							<?php $is_retina = ( isset($_COOKIE["device_pixel_ratio"]) )&&( $_COOKIE["device_pixel_ratio"] > 1 ); ?>

							<?php if( ($is_retina)&&(get_theme_mod('footer_logo_retina') != "") ) {?>
								<img class="retina_logo" src="<?php echo esc_url(get_theme_mod('footer_logo_retina')); ?>" alt="Logo" />
							<?php }
							elseif (get_theme_mod('footer_logo', get_template_directory_uri() . "/img/got-footer.svg") != "") {?>
								<img class="standard_logo" src="<?php echo esc_url(get_theme_mod('footer_logo', get_template_directory_uri() . '/img/got-footer.svg')); ?>" alt="Logo" />
							<?php }
							elseif ( (!$is_retina)&&(get_theme_mod('footer_logo', get_template_directory_uri() . "/img/got-footer.svg") == "")&&(get_theme_mod('footer_logo_retina') != "") ) {?>
								<img class="retina_logo" src="<?php echo esc_url(get_theme_mod('footer_logo_retina')); ?>" alt="Logo" />
							<?php } ?>
						</a>
					</div>
					<?php } ?>
				<p><?php echo esc_html(get_theme_mod('footer_text', 'Put Your Text Here')); ?></p>
			</div>
		</div>
	<?php } ?>

	<?php if ( get_theme_mod('enable_subfooter', 'yes') == 'yes' ) { ?>
		<div id="subfooter">
			<div class="spec">
					<?php $copyright = get_theme_mod('copyright', '&copy; Gotham 2016 | Crafted by ThemeCrispy'); ?>
					<small class="copyright"><?php echo esc_html($copyright); ?></small>
			</div>
		</div>
	<?php } ?> 
</footer>
<div class="modalmn"></div>
<?php wp_footer();?>
</body>
</html>