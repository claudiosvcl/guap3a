(function($) {
	"use strict";

	/* loader */
	var activity = document.createElement('div');
	activity.className = 'loader-activity';
	document.body.insertBefore(activity, document.body.firstChild);
	window.onload = function() {
		$('body').addClass('loader-done');
	}

	/* 
	 * Menu and burger-menu toggle, effect, swipe
	 * menu-item toggle
	 */

	$('.menu-trigger').click(function(event) {
		event.preventDefault();
		$(this).toggleClass('menu-visible');
		$('.navigation, body').toggleClass('menu-visible');
		$('.below_header, #content, .single-bh, .modalmn, .explore, #logo, .h-wrap #searchoverlay, .uihpost').toggleClass('menu-visible');
	});

	function closemenu() {
		$('.navigation, body, .menu-trigger').removeClass('menu-visible');
		$('.below_header, #content, .single-bh, .modalmn, .explore, #logo, .h-wrap #searchoverlay, .uihpost').removeClass('menu-visible');
	}

	$('.modalmn').click(function(event) {
		event.preventDefault();
		closemenu();
	});

	function swr() {
		if ($('.navigation').hasClass('menu-visible')) {
			closemenu();
		}
	}

	$('.modalmn').swipe({
		swipeRight: swr
	});

	// Detecting ie11
	// if it's ie11
	var isIE11 = !!(navigator.userAgent.match(/Trident/) && navigator.userAgent.match(/rv[ :]11/));
	// fix issue translate3d and vw for ie11
	if (isIE11) {
		$('#logo').addClass('ie11');
	}

	// hide submenu
	$('li.menu-item-has-children>div').next('ul').hide();

	// calculate each submenu height
	$('li.menu-item-has-children>div').each(function() {
		var eachsubmheight = $(this).next('ul').height();
		$(this).next('ul').css({'margin-top': '-' + eachsubmheight + 'px'});
	});

	// Toggle submenu onclick
	$('li.menu-item-has-children>div').click(function(event) {
		event.preventDefault();
		$(this).next('ul').height();
		var submheight = $(this).next('ul').height();
		$(this).next('ul').toggleClass('active');

		if ($(this).next('ul').hasClass('active')) {
			$(this).next('ul').css({'margin-top': '0'});
			$(this).next('ul').delay(400).show();
		}
		else {
			$(this).next('ul').css({'margin-top': '-' + submheight + 'px'});
			$(this).next('ul').delay(401).hide(0);
		}
	});

	/* 
	 * min-height dynamic 
	 * work with function gotham_minhdn() 
	 * in functions.php
	 */

	// if php_vars is defined
	if (typeof php_vars !== "undefined") {

		if (php_vars.hpy == "hpys") {
			$('#content').css('min-height', $(window).height() - ($('.below_header').height() + $('footer').height()));
		}
		if (php_vars.hpf == "hpst") {
			$('#content').css('min-height', $(window).height() - ($('footer').height()));
		}

	}

	/**
	 * Show header on scroll up
	 */
	
	var beforeScroll = 0;
	var headerh = $('header').height();
	if (typeof $(".single-screen, .h-below").position() !== 'undefined') {
		var singhbl = $('.single-screen, .h-below')[0].scrollHeight;
	}
	$(window).scroll(function() {
		var scroll = $(this).scrollTop();
		if ((scroll > beforeScroll)||(scroll === 0)||(scroll <= singhbl)) {
				$('body, header').removeClass('scroll-up');
				$('body, header').addClass('scroll-down');
			if (typeof $(".single-screen, .h-below").position() !== 'undefined') {
				if ((scroll > headerh)&&(scroll > beforeScroll)&&(scroll > singhbl + 5)) {
					$('header').css({ 'margin-top': '' + (- headerh) + 'px' });
					$('.explore, .uihpost').css({ 'transform': 'translate3d(0, ' + (- headerh) + 'px, 0)' });
					$('.more-sdbmb').removeClass('mosdbup').addClass('mosdbdown');
				}
			}
			else if (typeof $(".single-screen, .h-below").position() === 'undefined') {
				if ((scroll > headerh)&&(scroll > beforeScroll)) {
					$('header').css({ 'margin-top': '' + (- headerh) + 'px' });
					$('.explore, .uihpost').css({ 'transform': 'translate3d(0, ' + (- headerh) + 'px, 0)' });
					$('.more-sdbmb').removeClass('mosdbup').addClass('mosdbdown');
				}
			}
		}
		else if((scroll <= beforeScroll)&&(scroll > headerh)) {
			$('body, header').removeClass('scroll-down');
			$('body, header').addClass('scroll-up');
			$('header').css({ 'margin-top': '0' });
			$('.explore, .uihpost').css({ 'transform': 'translate3d(0, 0, 0)' });
			$('.more-sdbmb').removeClass('mosdbdown').addClass('mosdbup');
		}
		beforeScroll = scroll;
	});
	// remove navbar when color palette is open
	$('.palette').click(function() {
		$('body, header').removeClass('scroll-up').addClass('scroll-down');
	});

	var mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
	/**
	 * Detecting mobile or pad for navigation
	 */
	if(mobile) {
		$('.navigation').addClass('mbnavd');
		$('body, header').addClass('rmvefh');
	}

	/* bgmenu opacity on scroll */

	$('.menu-wrap').scroll(function() {
		if ($(this).scrollTop() <= 69) {
			$('.menu-bg').removeClass('h-opa');
		} else if ($(this).scrollTop() >= 69) {
			$('.menu-bg').addClass('h-opa');
		}
	});


	/*
	 * if logo > 72 remove top:-4px
	 * when max-width: 991px for alignment
	 * with burger menu
	 */
	
	var hlogo = $('#logo img').height();
	if(hlogo > 72) {
		$('.h-wrap #logo').css('top', 'auto');
	}

	/* 
	 * Logo retina 
	 * work with $is_retina in header.php and footer.php
	 */

	if ((document.cookie.indexOf('device_pixel_ratio') == -1) && ('devicePixelRatio' in window) && (window.devicePixelRatio > 1)) {
		var date = new Date();
		date.setTime(date.getTime() + 3600000);
		document.cookie = 'device_pixel_ratio=' + window.devicePixelRatio + ';' + ' expires=' + date.toUTCString() + '; path=/';
		if (document.cookie.indexOf('device_pixel_ratio') != -1) {
			window.location.reload();
		}
	}

	/* Smooth scroll to down */

	$('.md-icon-expand-more.stdown').click(function(event) {
		event.preventDefault();
		$('html, body').animate({
			scrollTop: $('.single-screen, .h-below')[0].scrollHeight
		}, 1000);
	});

	/* 
	 * Portfolio title and post title opacity on scroll
	 */
	
	var sidet = jQuery('.single-post .single-details');
	var hbewrap = jQuery('.single-portfolio .h-belowrapper');
	var sideth = jQuery('.single-post .single-details').height()/0.5;
	var belwh = jQuery('.single-portfolio .h-belowrapper').height()/0.5;

	jQuery(window).on('scroll', function() {
	   var scroll = jQuery(this).scrollTop();
	   sidet.css({ 'opacity' : (1 - scroll/sideth) });
	   hbewrap.css({ 'opacity' : (1 - scroll/belwh) });
	});

	/* 
	 * more-sidebar in responsive toggle
	 * more-description toggle 
	 * set blog-thumb visible if max-width: 481px
	 */

	$('.more-sdbmb').click(function(event) {
		event.preventDefault();
		$('.more-sdbmb, .widget-area').toggleClass('visible');
	});

	// show/hide .more-sdbmb.visible on scroll
	if (typeof $(".widget-area").position() !== 'undefined') {
		$('.widget-area').scroll(function() {
			if ($(this).scrollTop() <= 15) {
				$('.more-sdbmb.visible').css('opacity', 1);
			} else if ($(this).scrollTop() >= 15) {
				$('.more-sdbmb.visible').css('opacity', 0);
			}
		});
	}


	$('.more-description').click(function() {
		$(this).toggleClass('visible').parent().toggleClass('visible');
	});

	// media query change
	function WidthChange(mq) {
		if (mq.matches) {
			$('.blog-thumb').removeClass('visible');
		} else {
			$('.blog-thumb').addClass('visible');
		}
	}

	// Detecting ie9
	// if it's not ie9
	if (!($('html').is('.ie9'))) {
		// media query event handler
		if (matchMedia) {
			var mq = window.matchMedia("(min-width: 481px)");
			mq.addListener(WidthChange);
			WidthChange(mq);
		}
	}

	/* explore toggle */

	$(window).scroll(function() {
		if (window.matchMedia("(max-width: 480px)").matches) {
			if ($(this).scrollTop() > 140) {
				$('.explore, .ct_overlay').removeClass('visible');
			}
		}
		else {
			$('.explore, .ct_overlay').removeClass('visible');
		}
	});
	$('.explore').click(function(event) {
		$('.explore, .ct_overlay').toggleClass('visible');
		event.preventDefault();
	});

	if(mobile){
		if (window.matchMedia("(max-width: 480px)").matches) {
			$('.ct_overlay').css('position', 'absolute');
		}
	}

	/*
	 * Effect on scroll in single portfolio
	 * with portfolio-home section
	 */

	if (mobile) {
		$('.portfolio-home section').removeClass('liftp');
	} else {
		$(window).scroll(function() {
			if ($(this).scrollTop() >= 200) {
				$('.portfolio-home section').removeClass('liftp');
			} else {
				$('.portfolio-home section').addClass('liftp');
			}
		});
	}

	/*
	 * Colorpalette on single portfolio
	 */

	$('.palette, .colorpalette').click(function() {
		$('.colorpalette').toggleClass('open');
		$('body').toggleClass('palvh');
	});


	/* masonry blog */
	var $container = $('#contain');
	$(window).load(function() {
		var $container = $('#contain');
		var $imgs = $("img");

		// call function blog_thumb_dynamic
		blog_thumb_dynamic();

		$container.isotope({
			itemSelector: 'li.blog-item'
		});
		$imgs.load(function() {
			$container.isotope('layout');
		});
	});
	// function blog masonry blog-thumb dynamic height
	function blog_thumb_dynamic() {
		$('li.blog-item .blog-thumb').each(function() {
			var height = ($(this).children('h3').height() + $(this).children('.description').height() + $(this).children('.excerpt').height() + 223);
			$(this).css('height', height);
			// show hide avatar
			var width = $(this).innerWidth();
			if(width < 350) {
				$('li.blog-item .author a img').hide();
			}
			else {
				$('li.blog-item .author a img').show();
			}
		});
	}
	// call function blog_thumb_dynamic
	$(window).resize(function() {
		blog_thumb_dynamic();
	});

	// infinite scroll show more

	$container.imagesLoaded(function() {
		$container.isotope({
			itemSelector: 'li.blog-item'
		});
	});

	$container.infinitescroll({
			navSelector: '.pagination_load_more', // selector for the paged navigation
			nextSelector: '.pagination_load_more a', // selector for the NEXT link (to page 2)
			itemSelector: 'li.blog-item', // selector for all items youll retrieve
			behavior: 'twitter',
			loading: {
				finishedMsg: '',
				msgText: '',
				speed: 'fast',
				img: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQI12P4zwAAAgEBAKrChTYAAAAASUVORK5CYII='
			}
		},

		// Callback
		function(newElements) {
			var $newElems = $(newElements).css({
				opacity: 0
			});
			$newElems.imagesLoaded(function() {
				$newElems.animate({
					opacity: 1
				});
				$container.isotope('insert', $(newElements));
				// call lazyload as a callback
				$newElems.find("img.lazy").lazyload({
					/*effect: "fadeIn"*/
				});
			});
			$container.infinitescroll('update', { behavior: 'undefined' } );
			$container.infinitescroll('bind');
			$container.infinitescroll('update', { behavior: 'twitter' }, $('.more-description').removeClass('visible') );
			blog_thumb_dynamic();
			$('#infscr-loading').nextAll('.blog-item').find('.more-description').click(function() {
				$(this).toggleClass('visible').parent().toggleClass('visible');
			});
			Post_Like();
			WidthChange(mq);
			// call fitvids video post
			$(".video_post").fitVids();
		}
	);

	//load more for masonry blog, standard blog and masonry portfolio
	$('.pagination_load_more a').on('click', function() {
		$('.pagination_load_more a').toggleClass('rotate');
	});
	$('.pagination_load_more a').click(function() {
		$(this).css('background-color', 'transparent').css('box-shadow', 'none');
		$(window).scroll(function() {
			$('.pagination_load_more a').toggleClass('rotate');
		});
	});


	/* standard blog */
	$('.stblog').infinitescroll({
			navSelector: '.pagination_load_more', // selector for the paged navigation
			nextSelector: '.pagination_load_more a', // selector for the NEXT link (to page 2)
			itemSelector: 'li.blog-big-item', // selector for all items youll retrieve
			behavior: 'twitter',
			loading: {
				finishedMsg: '',
				msgText: '',
				speed: 'fast',
				img: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQI12P4zwAAAgEBAKrChTYAAAAASUVORK5CYII='
			}
		},

		// Callback
		function() {
			$('.stblog').infinitescroll('update', { behavior: 'undefined' } );
			$('.stblog').infinitescroll('bind');
			$('.stblog').infinitescroll('update', { behavior: 'twitter' }, $('.more-description').removeClass('visible') );
			$('#infscr-loading').nextAll('.blog-big-item').find('.more-description').click(function() {
				$(this).toggleClass('visible').parent().toggleClass('visible');
			});
			Post_Like();
			WidthChange(mq);
			// call fitvids video post
			$(".video_post").fitVids();
		}
	);

	/**
	* Portfolio with masonry effect - Isotope,
	* load more toggle,
	* svg effect,
	* filter-line,
	* infinite scroll show more
	*/
	var $container2 = $('#container');
	$(window).load(function() {
		$container2.imagesLoaded(function() {
			$container2.isotope({
				itemSelector: 'li.portfolio-item'
			});

			var $optionSets = $('#options .option-set'),
				$optionLinks = $optionSets.find('a');

			$optionLinks.click(function() {
				var $this = $(this);
				// don't proceed if already selected
				if ($this.hasClass('selected')) {
					return false;
				}
				var $optionSet = $this.parents('.option-set');
				$optionSet.find('.selected').removeClass('selected');
				$this.addClass('selected');

				// make option object dynamically, i.e. { filter: '.my-filter-class' }
				var options = {},
					key = $optionSet.attr('data-option-key'),
					value = $this.attr('data-option-value');
				// parse 'false' as false boolean
				value = value === 'false' ? false : value;
				options[key] = value;
				if (key === 'layoutMode' && typeof changeLayoutMode === 'function') {
					// changes in layout modes need extra logic
					changeLayoutMode($this, options)
				} else {
					// otherwise, apply new options
					$container2.isotope(options);
				}

				return false;
			});
		});
	});


	var is_chrome = navigator.userAgent.indexOf('Chrome') > -1;
	var is_firefox = navigator.userAgent.indexOf('Firefox') > -1;
	var is_safari = navigator.userAgent.indexOf("Safari") > -1;
	var is_Opera = navigator.userAgent.indexOf("Presto") > -1;
	function Svg_Effect(){
		//Svg effect

		if (is_chrome || is_firefox || is_Opera || is_safari) {

			$('.portfolio-item').on('mouseenter', function() {

				var $element = $(this);
				var thisWidth = $element.width();
				var thisHeight = $element.height();

				$('.' + 'top', $element).css({
					'transform': 'translateX(-' + (thisWidth * 2) + 'px)',
					'-webkit-transform': 'translateX(-' + (thisWidth * 2) + 'px)'
				});
				$('.' + 'left', $element).css({
					'transform': 'translateY(' + (thisHeight * 2) + 'px)',
					'-webkit-transform': 'translateY(' + (thisHeight * 2) + 'px)'
				});
				$('.' + 'bottom', $element).css({
					'transform': 'translateX(' + (thisWidth * 2) + 'px)',
					'-webkit-transform': 'translateX(' + (thisWidth * 2) + 'px)'
				});
				$('.' + 'right', $element).css({
					'transform': 'translateY(-' + (thisHeight * 2) + 'px)',
					'-webkit-transform': 'translateY(-' + (thisHeight * 2) + 'px)'
				});
			}).on('mouseleave', function() {

				var $element = $(this);

				$('.' + 'top', $element).css({
					'transform': 'translateX(0px)',
					'-webkit-transform': 'translateX(0px)'
				});
				$('.' + 'left', $element).css({
					'transform': 'translateY(0px)',
					'-webkit-transform': 'translateY(0px)'
				});
				$('.' + 'bottom', $element).css({
					'transform': 'translateX(0px)',
					'-webkit-transform': 'translateX(0px)'
				});
				$('.' + 'right', $element).css({
					'transform': 'translateY(0px)',
					'-webkit-transform': 'translateY(0px)'
				});
			});

		} else {
			// load this script for ie - Velocity

			$('.portfolio-item').on('mouseenter', function() {

				var $element = $(this);
				var thisWidth = $element.width();
				var thisHeight = $element.height();
				$('.top', $element).velocity({
					translateX: -(thisWidth * 2),
					x2: thisWidth * 3
				});
				$('.left', $element).velocity({
					translateY: thisHeight * 2,
					y1: thisHeight,
					y2: -(thisHeight * 2)
				});
				$('.bottom', $element).velocity({
					translateX: thisWidth * 2,
					x1: thisWidth,
					y1: thisHeight,
					x2: -(thisWidth * 2),
					y2: thisHeight
				});
				$('.right', $element).velocity({
					translateY: -(thisHeight * 2),
					x1: thisWidth,
					x2: thisWidth,
					y2: thisHeight * 3
				});
			}).on('mouseleave', function() {

				var $element = $(this);

				$('.' + 'top', $element).css({
					'transform': 'translateX(0px)'
				});
				$('.' + 'left', $element).css({
					'transform': 'translateY(0px)'
				});
				$('.' + 'bottom', $element).css({
					'transform': 'translateX(0px)'
				});
				$('.' + 'right', $element).css({
					'transform': 'translateY(0px)'
				});
			});
		}
	}
	// call Svg_Effect()
	Svg_Effect();


	// filter line in portfolio

	$(window).load(function() {
		var $el, leftPos, newWidth,
			$mainNav = $("section#options ul");

		$mainNav.append("<li id='pt-filter-line'></li>");
		var $magicLine = $("#pt-filter-line");

		// if $("#options li:first-child").position() is defined
		if (typeof $("#options li:first-child").position() !== 'undefined') {

			$magicLine
				.width($("#options li:first-child").width())
				.css("left", $("#options li:first-child").position().left)
				.data("origLeft", $magicLine.position().left)
				.data("origWidth", $magicLine.width());

		}

		$("#filters li").hover(function() {
			$el = $(this);
			leftPos = $el.position().left;
			newWidth = $el.width();
			$("#filters li a").click(function() {
				$magicLine.stop().animate({
					left: leftPos,
					width: newWidth + 24
				});
			});
		}), $("#filters li a").click(function() {
			$magicLine.stop().animate({
				left: $magicLine.data("origLeft"),
				width: $magicLine.data("origWidth")
			});
		});
	});


	// calculate width each li and addition in portfolio shortcode

	var total = 0;
	$('.blopt > ul li').each(function() {

		total += $(this).width() + 27;
		$('#filters').css('width', total);
	});

	// if #content width is more total
	if ($("#content").width() > total) {
		$('#filters').css('width', 'auto');
	} else {
		$('.blopt').css('overflow-x', 'scroll');
	}


	// infinite scroll show more

	$container2.imagesLoaded(function() {
		$container2.isotope({
			itemSelector: '.portfolio-item'
		});
	});

	$container2.infinitescroll({
			navSelector: '.pagination_load_more', // selector for the paged navigation
			nextSelector: '.pagination_load_more a', // selector for the NEXT link (to page 2)
			itemSelector: '.portfolio-item', // selector for all items youll retrieve
			behavior: 'twitter',
			loading: {
				finishedMsg: '',
				msgText: '',
				speed: 'fast',
				img: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQI12P4zwAAAgEBAKrChTYAAAAASUVORK5CYII='
			}
		},

		// call Isotope as a callback
		function(newElements) {
			var $newElems = $(newElements).css({
				opacity: 0
			});
			$newElems.imagesLoaded(function() {
				$newElems.animate({
					opacity: 1
				});
				$container2.isotope('insert', $(newElements));
				// call lazyload as a callback
				$newElems.find("img.lazy").lazyload({
					/*effect: "fadeIn"*/
				});
			});
			$container2.infinitescroll('update', { behavior: 'undefined' } );
			$container2.infinitescroll('bind');
			$container2.infinitescroll('update', { behavior: 'twitter' } );
			// call function Svg_Effect() for load more
			Svg_Effect();
			// call function Post_Like() for load more
			Post_Like();
		}
	);

	/*
	 * Effect on portfolio 1 column
	 */

	// portfolio 1column h3 hover
	if (window.matchMedia("(max-width: 800px)").matches) {
		$('.pc1 .dsptbb h3').removeAttr('style');
	}

	if (window.matchMedia("(min-width: 801px)").matches) {
		var attr_h3 = $('.pc1 .dsptbb h3').attr("style");

		$('.pc1').hover(function() {
			if ($('h3[style]').attr('style')) {
				$('h3[style]').attr('style', '');
			} else {
				$('h3[style]').attr('style', attr_h3);
			}
		});
	}

	/*
	 * Social sharing in single post & single portfolio
	 */

	// post 
	$('.buttonsh').on('click touch', function() {
		$('.buttonsh, .social-sharing, .buttonfac, .buttontwi, .buttonemail').toggleClass('scvisible');
	});

	if (!mobile) {
		$('.buttonsh-wrap').hover(function() {
			$('.buttonfac, .buttontwi, .buttonemail').css('visibility', 'visible');
		}, function() {
			$('.buttonfac, .buttontwi, .buttonemail').css('visibility', 'hidden');
		});
	}


	//portfolio
	$('.share-opf .md-icon-share').on('click touch', function() {
		$('button.md-icon-email, button.iconfa-facebook, button.iconfa-twitter').toggleClass('scvisible');
	});

	//post & portfolio
	$('.buttontwi, .share-opf .iconfa-twitter').on('click touch', function() {
		window.open('https://twitter.com/intent/tweet?text=' + location.href + ' @', '_blank');
		return false;
	});

	$('.buttonemail, .share-opf .md-icon-email').on('click touch', function() {
		window.open('mailto:' + '?' + 'body=' + location.href, '_blank');
		return false;
	});


	$('.buttonfac, .share-opf .iconfa-facebook').on('click touch', function() {
		window.open('https://www.facebook.com/sharer/sharer.php?u=' + location.href, '_blank');
		return false;
	});

	/*
	 * Last works
	 */

	$('.lwbg1').addClass('active');
	$('.lwl1').css('text-decoration', 'underline');

	$('.lwl1').hover(
		function() {
			$('.lwbg2, .lwbg3, .lwbg4, .lwbg5').removeClass('active'); 
			$('.lwbg1').addClass('active');
			$('.lwl2, .lwl3, .lwl4, .lwl5').css('text-decoration', '');
			$(this).css('text-decoration', 'underline');
		}
	);

	$('.lwl2').hover(
		function() {
			$('.lwbg1, .lwbg3, .lwbg4, .lwbg5').removeClass('active'); 
			$('.lwbg2').addClass('active');
			$('.lwl1, .lwl3, .lwl4, .lwl5').css('text-decoration', '');
			$(this).css('text-decoration', 'underline');
		}
	);

	$('.lwl3').hover(
		function() {
			$('.lwbg1, .lwbg2, .lwbg4, .lwbg5').removeClass('active'); 
			$('.lwbg3').addClass('active');
			$('.lwl1, .lwl2, .lwl4, .lwl5').css('text-decoration', '');
			$(this).css('text-decoration', 'underline');

		}
	);

	$('.lwl4').hover(
		function() {
			$('.lwbg1, .lwbg2, .lwbg3, .lwbg5').removeClass('active'); 
			$('.lwbg4').addClass('active');
			$('.lwl1, .lwl2, .lwl3, .lwl5').css('text-decoration', '');
			$(this).css('text-decoration', 'underline');

		}
	);

	$('.lwl5').hover(
		function() {
			$('.lwbg1, .lwbg2, .lwbg3, .lwbg4').removeClass('active'); 
			$('.lwbg5').addClass('active');
			$('.lwl1, .lwl2, .lwl3, .lwl4').css('text-decoration', '');
			$(this).css('text-decoration', 'underline');

		}
	);

	$('.last-works').css('height', $(window).height());
	$(window).resize(function() {
		$('.last-works').css('height', $(window).height());
	});

	/* 
	 * Portfolio slider, process, slider in cpt portfolio - Flexslider
	 */

	// flexslider in custom post type portfolio
	$(window).load(function() {
		$(".portfolio-bg .image_post .flexslider").flexslider({
			controlNav: false,
		});
	});


	// Portfolio slider
	$(window).load(function() {
		$('.wrsl-flex .flexslider').flexslider({
			animation: "slide",
			animationLoop: false,
			directionNav: false,
			manualControls: ".wrsl-flex .flex-control-nav li",
			useCSS: false,
			animationSpeed: 400,
			slideshow: false,
			touch: false,
			start: function() {
				// start isotope
	         	var $container3 = $('.containsl');
				$container3.imagesLoaded(function() {
					$container3.isotope({
						itemSelector: 'li.slidecat',
						transitionDuration: 0
					});
					$('.wrsl-flex .flex-viewport').css('height', ($('.wrsl-flex .flexslider .slides li').height()) + ($('.wrsl-flex .flexslider .slides li .containsl').height()));
				});
    		},
		});
	});

	var $container3 = $('.containsl'), $fltlink = $('#filters a');
	// touchend for mobile
	$fltlink.on('click touchend',function() {
		var filters = [];
		// get filter values
		$(this).each(function() {
			filters.push( $(this).attr("data-option-value") );
		});
		filters = filters.join(', ');
		setTimeout(function() {
			$container3.isotope({ filter: filters });
		}, 300);
	});

	$(window).resize(function() {
		$container3.isotope('layout');
	});

	// calculate width each li and addition in slider shortcode 
	var totali = 0;
	$('.wrsl-flex .flexslider-controls > ol li').each(function() {
		totali += $(this).width() + 22;
		$('.flex-control-nav').width(totali);
	});

	// if window width < totali
	if ($(window).width() < totali) {
		$('.wrsl-flex .flexslider-controls').css('overflow-x', 'scroll');
	}

	// magic line in portfolio slider

	if (!mobile) {
		$(window).load(function() {

			var $el, leftPos, newWidth,
				$mainNav = $(".wrsl-flex ol.flex-control-nav");

			$mainNav.append("<li id='filter-line'></li>");
			var $magicLine = $("#filter-line");

			var totalWidth = 0;
			$('.wrsl-flex ol.flex-control-nav li').each(function() {
				totalWidth += $(this).width();
				$('.wrsl-flex ol.flex-control-nav').css('width', totalWidth+2);
			});

			// if element.position() is defined
			if (typeof $(".wrsl-flex ol.flex-control-nav li:first-child").position() !== 'undefined') {

				$magicLine
					.width($(".wrsl-flex ol.flex-control-nav li:first-child").width())
					.css("left", $(".wrsl-flex ol.flex-control-nav li:first-child").position().left)
					.data("origLeft", $magicLine.position().left)
					.data("origWidth", $magicLine.width());

			}

			$(".wrsl-flex ol.flex-control-nav li").hover(function() {
				$el = $(this);
				leftPos = $el.position().left;
				newWidth = $el.width();
				$(".wrsl-flex ol.flex-control-nav li").click(function() {
					$magicLine.stop().animate({
						left: leftPos,
						width: newWidth
					});
				});
			}), $(".wrsl-flex ol.flex-control-nav li").click(function() {
				$magicLine.stop().animate({
					left: $magicLine.data("origLeft"),
					width: $magicLine.data("origWidth")
				});
			});

		});
	}

	if(mobile) {
		$('.wrsl-flex .flexslider-controls').attr('style', function(i,s) { return s + 'bottom: 0px !important;' });
	}

	// process
	$(window).load(function() {
		$('.wrap-flex .flexslider').flexslider({

			animation: "slide",
			animationLoop: false,
			directionNav: false,
			manualControls: ".wrap-flex .flex-control-nav li",
			useCSS: false,
			animationSpeed: 300,
			start: function() {

				if (($('.flex-control-nav').width()) >= ($('.flexslider-controls').width()) || (window.matchMedia("(max-width: 1000px)").matches)) {
					$('.flex-control-nav').addClass('longer');
				}
				if (($('.flex-control-nav').height()) >= ($('.flexslider-controls').height())) {
					$('.flex-control-nav').addClass('longer');
				}

			},

		});

	});

	// calculate width each li and addition in process shortcode
	var totalip = 0;
	$('.wrap-flex .flexslider-controls > ol li').each(function() {
		totalip += $(this).width() + 10;
		$('.wrap-flex .flex-control-nav').width(totalip);
	});

	// if window width is more totalip
	if ($('.flexslider-controls').width() > totalip) {
		$('.wrap-flex .flex-control-nav').css('width', '100%');
	} else {
		$('.wrap-flex .flexslider-controls, .wrap-flex .flex-control-nav').css('overflow-x', 'scroll');
	}

	// if mobile remove flexslider-controls in process
	if(mobile) {
		$('.wrap-flex .flex-control-nav').hide();
	}


	/* Imagescroll */

	$(window).load(function() {
		function shimgscroll() {
			$('.sh-imgscroll img').each(function() {
				if($(this).height() >= 300) {
					var height =  $(this).height() - 300;
					$(this).hover(function() {
						$(this).css({'transform': 'translate3d(0, -' + height + 'px, 0)', 'transition': '2s'});
					}, function() {
						$(this).css({'transform': 'translate3d(0, 0, 0)'});
					});
				}
			});
		}
		shimgscroll();
		$(window).on('resize', function() {
			shimgscroll();
		});
	});

	/*
	 * Highlight share
	 */

	var savedText = null; // Variable to save the text

	function saveSelection() {
		if (window.getSelection) {
			var sel = window.getSelection();
			if (sel.getRangeAt && sel.rangeCount) {
				return sel.getRangeAt(0);
			}
		} else if (document.selection && document.selection.createRange) {
			return document.selection.createRange();
		}
		return null;
	}

	function restoreSelection(range) {
		if (range) {
			if (window.getSelection) {
				var sel = window.getSelection();
				sel.removeAllRanges();
				sel.addRange(range);
			} else if (document.selection && range.select) {
				range.select();
			}
		}
	}

	var btnWrap = document.getElementById('share-button');
	if (btnWrap) {
		var btnShare = btnWrap.children[0];
		var btnShare2 = btnWrap.children[1];
	}

	// if element is defined
	if (typeof document.getElementsByTagName("article")[0] !== "undefined") {
		document.getElementsByTagName("article")[0].onmouseup = function(e) {

			savedText = saveSelection(); // Save selection on mouse-up

			setTimeout(function() {

				var isEmpty = savedText.toString().length === 0; // Check selection text length

				// set sharing button position
				if (btnWrap) {
					btnWrap.style.top = (isEmpty ? -9999 : e.pageY - btnWrap.offsetHeight - 15) + 'px';
					btnWrap.style.left = ((isEmpty ? -9999 : e.pageX - btnWrap.offsetWidth)) + 'px';
				}
			}, 10);
			if (btnWrap) {
				btnWrap.style.visibility = "visible";
			}
		};
	}



	$('.iconfa-twitter').click(function() {
		if (!savedText) return;

		window.open('https://twitter.com/intent/tweet?text=' + savedText + ' ' + location.href + ' @', 'shareWindow', 'width=300,height=150,top=50,left=50'); // Insert the selected text into sharing URL
		restoreSelection(savedText); // select back the old selected text
		// hide if we are done
		$('#share-button').css({
			'visibility': 'hidden'
		});


		return false;

	});

	$('.iconfa-facebook').click(function() {

		if (!savedText) return;

		window.open('https://www.facebook.com/sharer/sharer.php?u=' + savedText + ' ' + location.href + ' @', 'shareWindow', 'width=300,height=150,top=50,left=50'); // Insert the selected text into sharing URL
		restoreSelection(savedText); // select back the old selected text

		// hide if we are done
		$('#share-button').css({
			'visibility': 'hidden'
		});

		return false;

	});

	/*
	 * Search on mobile
	 * prevent keyboard push up webview in iOS
	 */

	if (mobile) {
		// How to prevent keyboard push up webview in iOS	
		$('input[type=search]').on('focus', function(e) {
			e.preventDefault();
			e.stopPropagation();
			window.scrollTo(0, -100); //the second 0 marks the Y scroll pos. Setting this to i.e. 100 will push the screen up by 100px. 
		});
	}

	/*
	 * Search effect
	 */

	var isAnimating;
	var isOpen;
	var toggleSearch;
	var searchoverlay = document.getElementById('searchoverlay'),
		sidemain = document.getElementById('side_main'),
		bdflw = document.getElementsByTagName('body')[0];
	if (searchoverlay) {
		var input = searchoverlay.querySelector('.h-wrap input#s');
		var ctrlClose = searchoverlay.querySelector('span.search-close');
	}
	isOpen = isAnimating = false,
		// show/hide search area
		toggleSearch = function(evt) {
			// return if open and the input gets focused
			if (evt.type.toLowerCase() === 'focus' && isOpen) return false;
			if (isOpen) {
				classie.remove(searchoverlay, 'open');
				if (sidemain) {
					classie.remove(sidemain, 'open');
				}
				classie.remove(bdflw, 'htbdflw');
				// trick to hide input text once the search overlay closes 
				// todo: hardcoded times, should be done after transition ends
				if (input.value !== '') {
					setTimeout(function() {
						classie.add(searchoverlay, 'hideInput');
						setTimeout(function() {
							classie.remove(searchoverlay, 'hideInput');
							input.value = '';
						}, 300);
					}, 500);
				}

				input.blur();
			} else {
				classie.add(searchoverlay, 'open');
				if (sidemain) {
					classie.add(sidemain, 'open');
				}
				classie.add(bdflw, 'htbdflw');
			}
			isOpen = !isOpen;
		};
	// events
	if (input) {
		input.addEventListener('focus', toggleSearch);
	}
	if (ctrlClose) {
		ctrlClose.addEventListener('click', toggleSearch);
	}
	// esc key closes search overlay
	// keyboard navigation events
	document.addEventListener('keydown', function(ev) {
		var keyCode = ev.keyCode || ev.which;
		if (keyCode === 27 && isOpen) {
			toggleSearch(ev);
		}
	});

	/**
	 * widget categories with hierarchy
	 */

	if ($('.widget_categories ul').hasClass('children')) {
		$('.widget_categories li').css('display', 'block');
	}

	/*
	 * Post like work with post-like.php and 
	 * function gotham_adding_scripts() -> wp_localize_script 
	 * in functions.php
	 */

	function Post_Like() {
		$(".post-like a").click(function() {

			var heart = $(this);

			var post_id = heart.data("post_id");

			$.ajax({
				type: "post",
				url: ajax_var.url,
				data: "action=post-like&nonce=" + ajax_var.nonce + "&post_like=&post_id=" + post_id,
				success: function(count) {
					if (count != "already") {
						heart.addClass("voted");
						heart.siblings(".count").text(count);
					}
				}
			});

			return false;
		});
	}
	// call Post_Like()
	Post_Like();

	/* 
	 * Autocomplete work with autocomplete.php and
	 * function gotham_adding_scripts() -> wp_localize_script 
	 * in functions.php
	 */
	
	var acs_action = 'gotham_acsearch';
	$(".h-wrap #s").autocomplete({
		appendTo: ".h-wrap #searchoverlay",
		source: function(req, response) {
			$.getJSON(gotacsearch.url+'?callback=?&action='+acs_action, req, function( data, status, xhr ) {
				var counts = 0;
				for(var i = 0; i < data.length; i++) {
					counts++;
				}
				if ($('.srcuwrap').length) {
					$('.srcuwrap').empty().append("<span class='sercount'>"+ counts + (counts > 1 ? ' results' : ' result ') +"</span>");
				}
				else {
					$("#searchform").after("<div class='srcuwrap'>");
					$('.srcuwrap').empty().append("<span class='sercount'>"+ counts + (counts > 1 ? ' results' : ' result ') +"</span>");
				}
				response( data.slice(0, 6) );
			});
		},
		messages: {
			noResults: '',
			results : function(count) {
			}
		},
		response: function (event, ui) {
			var len = ui.content.length;
			$('.srcuwrap').append('<span class="curcount">' + len + ' of  ' + '</span>');
		},
		open: function( event, ui ) {
			var time = 0.35;
			$('li.search-info').each(function() {
				time += 0.10;
				$(this).css('opacity', '1').css('transform', 'translate3d(0,0,0)').css({'transition': 'transform '+time+'s, opacity '+time+'s'});
			});
			$("#s").keyup(function() {
				srcuwrapsh();
			});
			srcuwrapsh();
		},
		close: function( event, ui ) {
			$("#s").keyup(function() {
				srcuwrapsh();
			});
			srcuwrapsh();
  		}
	})
	.autocomplete( "instance" )._renderItem = function( ul, item ) {
		return $( "<li class='search-info'>")
		.append( "<div class='sewrim'>" + item.thumbnail + "</div><h6>" + item.label + "</h6><div class='gepoid'>" + item.post_type + "</div><span class='gedase'>" +  item.date + "</span>" ).wrapInner("<a href=" + item.link + ">")
		.appendTo( ul );
	};

	function srcuwrapsh() {
		if(($('.ui-autocomplete').css('display') == 'none')&&($('#s').val() === '')) {
			$('.srcuwrap').hide();
		}
		else {
			$('.srcuwrap').show();
		}
	}

	/*
	 * Contact form 7
	 */

	$(document).mouseup(function(event) {
		var elemf = $(".wpcf7-form p .wpcf7-form-control-wrap input.wpcf7-text");
		var elemf2 = $(".wpcf7-form p .wpcf7-form-control-wrap textarea.wpcf7-textarea");
		var elemfi = $(".wpcf7-form p .wpcf7-form-control-wrap input.wpcf7-text").parent().parent();
		var elemfi2 = $(".wpcf7-form p .wpcf7-form-control-wrap textarea.wpcf7-textarea").parent().parent();

		if (!elemf.is(event.target) && elemf.has(event.target).length === 0 || elemf.is(event.target) > 0) {
			$(elemf).parent('span').removeClass('filled');
		}

		if (!elemf2.is(event.target) && elemf2.has(event.target).length === 0) {
			$(elemf2).parent('span').removeClass('filled');
		}

		if (!elemfi.is(event.target) && elemfi.has(event.target).length === 0 || elemf.is(event.target) > 0) {
			$(elemfi).removeClass('fontf');
		}

		if (!elemfi2.is(event.target) && elemfi2.has(event.target).length === 0 || elemf.is(event.target) > 0) {
			$(elemfi2).removeClass('fontf');
		}

		$(elemf).each(function() {
			var valueA = $(this).val();
			if (valueA.length > 0) {
				$(this).parent('span').addClass('filled');
				$(this).parent().parent().addClass('fontf');
			}
		});

		$(elemf2).each(function() {
			var valueB = $(this).val();
			if (valueB.length > 0) {
				$(this).parent('span').addClass('filled');
				$(this).parent().parent().addClass('fontf');
			}
		});
	});

	$('.wpcf7-form p .wpcf7-form-control-wrap').click(function() {
		$(this).addClass('filled');
	});

	$('.wpcf7-form input.wpcf7-text').click(function() {
		$(this).parent().parent().addClass('fontf');
	});

	$('.wpcf7-form textarea.wpcf7-textarea').click(function() {
		$(this).parent().parent().addClass('fontf');
	});

	$('.wpcf7-form input[type="submit"]').click(function() {
		$('.wpcf7-form p').css('overflow', 'visible');
	});

	// remove line
	$('.wpcf7-form p span:not(:has(input.wpcf7-text ,textarea.wpcf7-textarea))').addClass('nopsel');

	/*
	 * Parallax - Stellar
	 */
	
	if(!mobile) {
		$.stellar({

			horizontalScrolling: false,
			verticalOffset: 40,
			responsive: true

		});
	}

	if (mobile) {
		$('.parallax, .h-below').addClass('mobpara');
	}

	/*
	 * Responsive media - Fitvids
	 */

	$("#content").fitVids();
	$(".video_post").fitVids();

	/*
	 * Fix arrow stdown align right in IE 11
	 */
		
	if (isIE11) {
		var curwidth = $(window).width() - $('.title-wr').width();
		var curspace = curwidth / 2;
		var curleft = ($('.title-wr').width()) + curspace;
		$('.title-wr .md-icon-expand-more.stdown').css('left', curleft);
		$('.title-wr.twcenter .md-icon-expand-more.stdown').css('left', '0');
	}

})(jQuery);