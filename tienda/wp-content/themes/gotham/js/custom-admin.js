/**
 * Toggle metaboxes 
 */
jQuery(document).ready(function($) {
  "use strict";

  /** 
   * post format in post
   */
  $("#gotham_options_format .cmb2-id-gotham-video-post, #gotham_options_format .cmb2-id-gotham-audio-post, #gotham_options_format .cmb2-id-gotham-link-text, #gotham_options_format .cmb2-id-gotham-link-post, #gotham_options_format .cmb2-id-gotham-quote-post, #gotham_options_format .cmb2-id-gotham-author-quote-post").hide();
  var selectedElt = $("input[name=post_format]:checked").attr("value");
  if(selectedElt) {
    $("#gotham_options_format .cmb2-id-gotham-" + selectedElt + "-post").fadeIn();
  }
  if(selectedElt == "link") {
    $("#gotham_options_format .cmb2-id-gotham-link-text").fadeIn();
  }
  if(selectedElt == "quote") {
    $("#gotham_options_format .cmb2-id-gotham-author-quote-post").fadeIn();
  }

  $("input[name=post_format]").change(function() {
    $("#gotham_options_format .cmb2-id-gotham-video-post, #gotham_options_format .cmb2-id-gotham-audio-post, #gotham_options_format .cmb2-id-gotham-link-text, #gotham_options_format .cmb2-id-gotham-link-post, #gotham_options_format .cmb2-id-gotham-quote-post, #gotham_options_format .cmb2-id-gotham-author-quote-post").hide();
    var selectedElt = $("input[name=post_format]:checked").attr("value");
    if(selectedElt) {
      $("#gotham_options_format .cmb2-id-gotham-" + selectedElt + "-post").fadeIn();
    }
    if(selectedElt == "link") {
      $("#gotham_options_format .cmb2-id-gotham-link-text").fadeIn();
    }
    if(selectedElt == "quote") {
      $("#gotham_options_format .cmb2-id-gotham-author-quote-post").fadeIn();
    }
  });

  /**
   * sidebar in page
   */
  if ($('select#gotham_select_sidebar').val() == '') {
    $('select#gotham_select_sidebar').closest(".cmb-row").nextAll().slice(0, 1).hide();
  }
  $('select#gotham_select_sidebar').change(
    function() {
      if ($(this).val() == 'sidebar_right') {
        $(this).closest(".cmb-row").nextAll().slice(0, 1).show();
      }
      else if($(this).val() == 'sidebar_left') {
        $(this).closest(".cmb-row").nextAll().slice(0, 1).show();
      }
      else {
        $(this).closest(".cmb-row").nextAll().slice(0, 1).hide();
      }
    }
  );

  /** 
   * title area in page and portfolio
   */
  var $no_title_area = $('input:radio[id=gotham_select_title_area3]');
  // if radio is checked
  if($no_title_area.is(':checked') === true) {
    $('#cmb2-metabox-gotham_options_page .cmb2-id-gotham-select-title-area input.cmb2-option').closest(".cmb-row").nextAll().slice(0, 10).hide();
    $('#cmb2-metabox-gotham_options_portfolio .cmb2-id-gotham-select-title-area input.cmb2-option').closest(".cmb-row").nextAll().slice(0, 10).hide();
  }
  // if value == no 
  if ($('#cmb2-metabox-gotham_options_page .cmb2-id-gotham-select-title-area input.cmb2-option').val() == 'no') {
    $('#cmb2-metabox-gotham_options_page .cmb2-id-gotham-select-title-area input.cmb2-option').closest(".cmb-row").nextAll().slice(0, 10).hide();
  }
  if ($('#cmb2-metabox-gotham_options_portfolio .cmb2-id-gotham-select-title-area input.cmb2-option').val() == 'no') {
    $('#cmb2-metabox-gotham_options_portfolio .cmb2-id-gotham-select-title-area input.cmb2-option').closest(".cmb-row").nextAll().slice(0, 10).hide();
  }
  // input radio change
  $('#cmb2-metabox-gotham_options_page .cmb2-id-gotham-select-title-area input.cmb2-option').change(
    function() {
      //if value == no 
      if ($(this).val() == 'no') {
        $(this).closest(".cmb-row").nextAll().slice(0, 10).hide();
      }
      //else
      else {
        $(this).closest(".cmb-row").nextAll().slice(0, 7).show();
      }
    }
  );
  // input radio change
  $('#cmb2-metabox-gotham_options_portfolio .cmb2-id-gotham-select-title-area input.cmb2-option').change(
    function() {
      //if value == no 
      if ($(this).val() == 'no') {
        $(this).closest(".cmb-row").nextAll().slice(0, 10).hide();
      }
      //else
      else {
        $(this).closest(".cmb-row").nextAll().slice(0, 6).show();
      }
    }
  );

  /**
   * background in pages and portfolio
   */
  var radio = '#cmb2-metabox-gotham_options_page .cmb2-id-gotham-options-bg input.cmb2-option';
  var inpbgi = '#cmb2-metabox-gotham_options_page .cmb2-id-gotham-title-area-background-image';
  var inpbgp = '#cmb2-metabox-gotham_options_page .cmb2-id-gotham-title-area-parallax';
  var inpbgc = '#cmb2-metabox-gotham_options_page .cmb2-id-gotham-title-area-background-color';

  var pradio = '#cmb2-metabox-gotham_options_portfolio .cmb2-id-gotham-options-bg input.cmb2-option';
  var pinpbgi = '#cmb2-metabox-gotham_options_portfolio .cmb2-id-gotham-title-area-background-image';
  var pinpbgp = '#cmb2-metabox-gotham_options_portfolio .cmb2-id-gotham-title-area-parallax';
  var pinpbgc = '#cmb2-metabox-gotham_options_portfolio .cmb2-id-gotham-title-area-background-color';

  // hide bg_image, bg_parallax, bg_color
  $(inpbgi+ ", " +inpbgp+ ", " +inpbgc+ ", " +pinpbgi+ ", " +pinpbgp+ ", " +pinpbgc).hide();

  var $bg1 = $('input:radio[id=gotham_options_bg1]');
  // if bg1 is checked
  if($bg1.is(':checked') === true) {
    $(radio).closest(".cmb-row").nextAll().slice(1, 3).hide();
    $(radio).closest(".cmb-row").nextAll().slice(0, 1).show();
    $(pradio).closest(".cmb-row").nextAll().slice(1, 3).hide();
    $(pradio).closest(".cmb-row").nextAll().slice(0, 1).show();
  }

  var $bg2 = $('input:radio[id=gotham_options_bg2]');
  // if bg2 is checked
  if($bg2.is(':checked') === true) {
    $(radio).closest(".cmb-row").nextAll(radio).hide();
    $(radio).closest(".cmb-row").nextAll().slice(1, 2).show();
    $(pradio).closest(".cmb-row").nextAll(pradio).hide();
    $(pradio).closest(".cmb-row").nextAll().slice(1, 2).show();
  }

  var $bg3 = $('input:radio[id=gotham_options_bg3]');
  // if bg3 is checked
  if($bg3.is(':checked') === true) {
    $(radio).closest(".cmb-row").nextAll().slice(0,2).hide();
    $(radio).closest(".cmb-row").nextAll().slice(2, 3).show();
    $(pradio).closest(".cmb-row").nextAll().slice(0,2).hide();
    $(pradio).closest(".cmb-row").nextAll().slice(2, 3).show();
  }

  // if radio is checked
  if($no_title_area.is(':checked') === true) {
    $(inpbgi+ ", " +inpbgp+ ", " +inpbgc+ ", " +pinpbgi+ ", " +pinpbgp+ ", " +pinpbgc).hide();
  }

  // input radio change
  $('#cmb2-metabox-gotham_options_page .cmb2-id-gotham-select-title-area input.cmb2-option').change(
    function() {
      //if value != no 
      if ($(this).val() != 'no') {
        $(this).closest(".cmb-row").nextAll().slice(0, 7).show();
        // if bg1 is checked
        if($bg1.is(':checked') === true) {
          $(radio).closest(".cmb-row").nextAll().slice(1, 3).hide();
          $(radio).closest(".cmb-row").nextAll().slice(0, 1).show();
        }
        // if bg2 is checked
        if($bg2.is(':checked') === true) {
          $(radio).closest(".cmb-row").nextAll(radio).hide();
          $(radio).closest(".cmb-row").nextAll().slice(1, 2).show();
        }
        // if bg3 is checked
        if($bg3.is(':checked') === true) {
          $(radio).closest(".cmb-row").nextAll().slice(0,2).hide();
          $(radio).closest(".cmb-row").nextAll().slice(2, 3).show();
        }
      }
    }
  );

  $('#cmb2-metabox-gotham_options_portfolio .cmb2-id-gotham-select-title-area input.cmb2-option').change(
    function() {
      //if value != no 
      if ($(this).val() != 'no') {
        $(this).closest(".cmb-row").nextAll().slice(0, 5).show();
        // if bg1 is checked
        if($bg1.is(':checked') === true) {
          $(pradio).closest(".cmb-row").nextAll().slice(1, 3).hide();
          $(pradio).closest(".cmb-row").nextAll().slice(0, 1).show();
        }
        // if bg2 is checked
        if($bg2.is(':checked') === true) {
          $(pradio).closest(".cmb-row").nextAll(pradio).hide();
          $(pradio).closest(".cmb-row").nextAll().slice(1, 2).show();
        }
        // if bg3 is checked
        if($bg3.is(':checked') === true) {
          $(pradio).closest(".cmb-row").nextAll().slice(0,2).hide();
          $(pradio).closest(".cmb-row").nextAll().slice(2, 3).show();
        }
      }
    }
  );

  // input radio change
  $(radio).change(
    function() {
      //if value == bg_image 
      if ($(this).val() == 'bg_image') {
        $(this).closest(".cmb-row").nextAll().slice(1, 3).hide();
        $(this).closest(".cmb-row").nextAll().slice(0, 1).show();
      }
    }
  );

  // input radio change
  $(radio).change(
    function() {
      //if value == bg_para 
      if($(this).val() == 'bg_para') {
        $(this).closest(".cmb-row").nextAll(".cmb2-id-gotham-title-area-background-image, .cmb-row.cmb2-id-gotham-title-area-background-color").hide();
        $(this).closest(".cmb-row").nextAll().slice(1, 2).show();
      }
    }
  );
  
  // input radio change
  $(radio).change(
    function() {
      //if value == bg_color 
      if($(this).val() == 'bg_color') {
        $(this).closest(".cmb-row").nextAll().slice(0,2).hide();
        $(this).closest(".cmb-row").nextAll().slice(2, 3).show();
      }
    }
  );

  // input radio change
  $(pradio).change(
    function() {
      //if value == bg_image 
      if ($(this).val() == 'bg_image') {
        $(this).closest(".cmb-row").nextAll().slice(1, 3).hide();
        $(this).closest(".cmb-row").nextAll().slice(0, 1).show();
      }
    }
  );

  // input radio change
  $(pradio).change(
    function() {
      //if value == bg_para 
      if($(this).val() == 'bg_para') {
        $(this).closest(".cmb-row").nextAll(".cmb2-id-gotham-title-area-background-image, .cmb-row.cmb2-id-gotham-title-area-background-color").hide();
        $(this).closest(".cmb-row").nextAll().slice(1, 2).show();
      }
    }
  );

  // input radio change
   $(pradio).change(
    function() {
      //if value == bg_color 
      if($(this).val() == 'bg_color') {
        $(this).closest(".cmb-row").nextAll().slice(0,2).hide();
        $(this).closest(".cmb-row").nextAll().slice(2, 3).show();
      }
    }
  );

  /**
   * color palette in portfolio
   */
  $("input#gotham_color_palette").each(function() {
    if(!$(this).is(":checked")) {
      $(this).closest(".cmb-row").nextAll().slice(0, 4).hide();
    }
  });

  $("input#gotham_color_palette").click(function() {
    $(this).closest(".cmb-row").nextAll().slice(0, 4).fadeToggle("slow");
  });

});