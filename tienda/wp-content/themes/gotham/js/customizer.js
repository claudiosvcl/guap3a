/**
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */
(function($) {
	"use strict";
	/* primary */
	wp.customize('primary', function(value) {
		value.bind(function(newval) {
			$('#sidebar #wp-calendar #today, #sidebar .widget_calendar caption, .wrap-flex .primg, .team-info .team-social .send a, .map-information .md-icon-expand-more, .link_pages ul li.active, mark, ins, .woocommerce .header-cart a.cart-contents span, .header-cart a.cart-contents span').css('background', newval);

			$('#filter-line, #pt-filter-line, .numb-posts .nbpar, .woocommerce-account .woowrap-account .woocommerce-MyAccount-navigation ul li.is-active').css('border-color', newval);

			$('#submit, .woocommerce div.product form.cart button.button, .woocommerce-cart .woocommerce .cart_totals a.button.alt, .woocommerce-checkout .woocommerce .check-order .woocommerce-checkout-payment input.button.alt, .woocommerce-account .woowrap-account .woocommerce-MyAccount-content .woocommerce-info a.button, .woocommerce-account .woowrap-account input.button, .woocommerce .woowrap-account .woocommerce-Pagination a.button, .woocommerce .widget-area #sidebar .woocommerce.widget_price_filter .button, .woocommerce div.summary p.cart .single_add_to_cart_button, div.woocommerce input.button').hover(function() {
					$('#submit, .woocommerce div.product form.cart button.button, .woocommerce-cart .woocommerce .cart_totals a.button.alt, .woocommerce-checkout .woocommerce .check-order .woocommerce-checkout-payment input.button.alt, .woocommerce-account .woowrap-account .woocommerce-MyAccount-content .woocommerce-info a.button, .woocommerce-account .woowrap-account input.button, .woocommerce .woowrap-account .woocommerce-Pagination a.button, .woocommerce .widget-area #sidebar .woocommerce.widget_price_filter .button, .woocommerce div.summary p.cart .single_add_to_cart_button, div.woocommerce input.button').css('background', newval);
				},
				function() {
					$('#submit, .woocommerce div.product form.cart button.button, .woocommerce-cart .woocommerce .cart_totals a.button.alt, .woocommerce-checkout .woocommerce .check-order .woocommerce-checkout-payment input.button.alt, .woocommerce-account .woowrap-account .woocommerce-MyAccount-content .woocommerce-info a.button, .woocommerce-account .woowrap-account input.button, .woocommerce .woowrap-account .woocommerce-Pagination a.button, .woocommerce .widget-area #sidebar .woocommerce.widget_price_filter .button, .woocommerce div.summary p.cart .single_add_to_cart_button, div.woocommerce input.button').css('background', '');
				}
			);

			$('.flex-direction-nav a').hover(function() {
					$(this).css('background', newval);
				},
				function() {
					$(this).css('background', '');
				}
			);

		});
	});

	/* body bgcolor */
	wp.customize('body_bgcolor', function(value) {
		value.bind(function(newval) {
			$('body, body.tc_blog, body.page.woocommerce-page, body.archive.woocommerce-page, body.single-product.woocommerce').css('background', newval);
		});
	});

	/* content bgcolor */
	wp.customize('content_bgcolor', function(value) {
		value.bind(function(newval) {
			$('#content, body.tc_blog #content').css('background', newval);
		});
	});

	/* bgimage */
	wp.customize('bg_image', function(value) {
		value.bind(function(to) {

			0 === $.trim(to).length ?
				$('body').css('background-image', '') :
				$('body').css('background-image', 'url( ' + to + ')').css('background-repeat', 'no-repeat').css('background-position', 'center 0px').css('background-attachment', 'fixed');

		});
	});

	/* bgimage pattern */
	wp.customize('bg_image_pattern', function(value) {
		value.bind(function(to) {
			0 === $.trim(to).length ?
				$('body').css('background-image', '') :
				$('body').css('background-image', 'url( ' + to + ')').css('background-repeat', 'repeat').css('background-position', '0px 0px');
		});
	});

	/* logo dark */
	wp.customize('logo_dark', function(value) {
		value.bind(function(to) {
			if (0 === $.trim(to).length) {
				$('#logo a').hide();
			} else {
				$('#logo a').show();
				$('#logo a').html('<img src=' + to + '>');
			}
		});
	});


	/* logo retina dark */
	wp.customize('logo_retina_dark', function(value) {
		value.bind(function(to) {
			$('#logo a').html('<img src=' + to + '>');
		});
	});


	/* logo height */
	wp.customize('logo_height', function(value) {
		value.bind(function(newval) {
			$('#logo img').css('height', newval);
		});
	});

	/* footer logo */
	wp.customize('footer_logo', function(value) {
		value.bind(function(to) {
			$('#footer_logo a').html('<img src=' + to + '>');
		});
	});


	/* footer logo retina */
	wp.customize('footer_logo_retina', function(value) {
		value.bind(function(to) {
			$('#footer_logo a').html('<img src=' + to + '>');
		});
	});



	wp.customize('blogname', function(value) {
		value.bind(function(to) {
			$('.site-title a').text(to);
		});
	});
	wp.customize('blogdescription', function(value) {
		value.bind(function(to) {
			$('.site-description').text(to);
		});
	});

	/* Header text color */
	wp.customize('header_textcolor', function(value) {
		value.bind(function(to) {
			if ('blank' === to) {
				$('.site-title, .site-description').css({
					'clip': 'rect(1px, 1px, 1px, 1px)',
					'position': 'absolute'
				});
			} else {
				$('.site-title, .site-description').css({
					'clip': 'auto',
					'color': to,
					'position': 'relative'
				});
			}
		});
	});

	/* body font options */

	wp.customize('pgtitle_font_size', function(value) {
		value.bind(function(newval) {
			$('.single-h1:not(.archtitle), .single-title').css('font-size', newval + 'px');
		});
	});

	wp.customize('pgtitle_font_weight', function(value) {
		value.bind(function(newval) {
			$('.single-h1, .single-title').css('font-weight', newval);
		});
	});

	wp.customize('post_font_size', function(value) {
		value.bind(function(newval) {
			$('#content>article>.post-content').css('font-size', newval + 'px');
		});
	});

	/* miscellaneous */
	wp.customize('err_bg_image', function(value) {
		value.bind(function(to) {

			0 === $.trim(to).length ?
				$('.error404 #content').css('background-image', '') :
				$('.error404 #content').css('background-image', 'url( ' + to + ')');

		});
	});


})(jQuery);