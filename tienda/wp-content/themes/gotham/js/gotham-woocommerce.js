/*
 * Zoom 1.7.15
 * license: MIT
 * Copyright (c) 2015 Jack Moore
 * http://www.jacklmoore.com/zoom
 */
(function($){var defaults={url:false,callback:false,target:false,duration:120,on:"mouseover",touch:true,onZoomIn:false,onZoomOut:false,magnify:1};$.zoom=function(target,source,img,magnify){var targetHeight,targetWidth,sourceHeight,sourceWidth,xRatio,yRatio,offset,$target=$(target),position=$target.css("position"),$source=$(source);$target.css("position",/(absolute|fixed)/.test(position)?position:"relative");$target.css("overflow","hidden");img.style.width=img.style.height="";$(img).addClass("zoomImg").css({position:"absolute",top:0,left:0,opacity:0,width:img.width*magnify,height:img.height*magnify,border:"none",maxWidth:"none",maxHeight:"none"}).appendTo(target);return{init:function(){targetWidth=$target.outerWidth();targetHeight=$target.outerHeight();if(source===$target[0]){sourceWidth=targetWidth;sourceHeight=targetHeight}else{sourceWidth=$source.outerWidth();sourceHeight=$source.outerHeight()}xRatio=(img.width-targetWidth)/sourceWidth;yRatio=(img.height-targetHeight)/sourceHeight;offset=$source.offset()},move:function(e){var left=e.pageX-offset.left,top=e.pageY-offset.top;top=Math.max(Math.min(top,sourceHeight),0);left=Math.max(Math.min(left,sourceWidth),0);img.style.left=left*-xRatio+"px";img.style.top=top*-yRatio+"px"}}};$.fn.zoom=function(options){return this.each(function(){var settings=$.extend({},defaults,options||{}),target=settings.target||this,source=this,$source=$(source),$target=$(target),img=document.createElement("img"),$img=$(img),mousemove="mousemove.zoom",clicked=false,touched=false,$urlElement;if(!settings.url){$urlElement=$source.find("img");if($urlElement[0]){settings.url=$urlElement.data("src")||$urlElement.attr("src")}if(!settings.url){return}}(function(){var position=$target.css("position");var overflow=$target.css("overflow");$source.one("zoom.destroy",function(){$source.off(".zoom");$target.css("position",position);$target.css("overflow",overflow);$img.remove()})})();img.onload=function(){var zoom=$.zoom(target,source,img,settings.magnify);function start(e){zoom.init();zoom.move(e);$img.stop().fadeTo($.support.opacity?settings.duration:0,1,$.isFunction(settings.onZoomIn)?settings.onZoomIn.call(img):false)}function stop(){$img.stop().fadeTo(settings.duration,0,$.isFunction(settings.onZoomOut)?settings.onZoomOut.call(img):false)}if(settings.on==="grab"){$source.on("mousedown.zoom",function(e){if(e.which===1){$(document).one("mouseup.zoom",function(){stop();$(document).off(mousemove,zoom.move)});start(e);$(document).on(mousemove,zoom.move);e.preventDefault()}})}else if(settings.on==="click"){$source.on("click.zoom",function(e){if(clicked){return}else{clicked=true;start(e);$(document).on(mousemove,zoom.move);$(document).one("click.zoom",function(){stop();clicked=false;$(document).off(mousemove,zoom.move)});return false}})}else if(settings.on==="toggle"){$source.on("click.zoom",function(e){if(clicked){stop()}else{start(e)}clicked=!clicked})}else if(settings.on==="mouseover"){zoom.init();$source.on("mouseenter.zoom",start).on("mouseleave.zoom",stop).on(mousemove,zoom.move)}if(settings.touch){$source.on("touchstart.zoom",function(e){e.preventDefault();if(touched){touched=false;stop()}else{touched=true;start(e.originalEvent.touches[0]||e.originalEvent.changedTouches[0])}}).on("touchmove.zoom",function(e){e.preventDefault();zoom.move(e.originalEvent.touches[0]||e.originalEvent.changedTouches[0])}).on("touchend.zoom",function(e){e.preventDefault();if(touched){touched=false;stop()}})}if($.isFunction(settings.callback)){settings.callback.call(img)}};img.src=settings.url})};$.fn.zoom.defaults=defaults})(window.jQuery);

/*
 * WooCommerce Quantity Increment
 * number-polyfill
 */
(function(){(function(e){var t,n;return t=document.createElement("input"),t.setAttribute("type","number"),"text"!==t.type?(e.fn.inputNumber=function(){return e(this)},void 0):(e.fn.inputNumber=function(){return e(this).filter(function(){var t;return t=e(this),t.is('input[type="number"]')&&!(t.parent().is("span")&&t.next().is("div.number-spin-btn-container")&&t.next().children().first().is("div.number-spin-btn-up")&&t.next().children().eq(1).is("div.number-spin-btn-down"))}).each(function(){n.polyfills.push(new n(this))}),e(this)},n=function(t){var i,r,o,s,a=this;if(this.elem=e(t),!(this.elem.is(":root *")&&this.elem.height()>0))throw Error("Element must be in DOM and displayed so that its height can be measured.");s=this.elem.outerHeight()/2+"px",this.upBtn=e("<div/>",{"class":"number-spin-btn number-spin-btn-up",style:"height: "+s}),this.downBtn=e("<div/>",{"class":"number-spin-btn number-spin-btn-down",style:"height: "+s}),this.btnContainer=e("<div/>",{"class":"number-spin-btn-container"}),i=e("<span/>",{style:"white-space: nowrap"}),this.upBtn.appendTo(this.btnContainer),this.downBtn.appendTo(this.btnContainer),this.elem.wrap(i),this.btnContainer.insertAfter(this.elem),this.elem.on({focus:function(){a.elem.on({DOMMouseScroll:n.domMouseScrollHandler,mousewheel:n.mouseWheelHandler},{p:a})},blur:function(){a.elem.off({DOMMouseScroll:n.domMouseScrollHandler,mousewheel:n.mouseWheelHandler})}}),this.elem.on({keypress:n.elemKeypressHandler,change:n.elemChangeHandler},{p:this}),this.upBtn.on("mousedown",{p:this,func:"increment"},n.elemBtnMousedownHandler),this.downBtn.on("mousedown",{p:this,func:"decrement"},n.elemBtnMousedownHandler),this.elem.css("textAlign","right"),this.attrMutationHandler("class"),"undefined"!=typeof WebKitMutationObserver&&null!==WebKitMutationObserver||r!==void 0&&null!==r?("undefined"==typeof WebKitMutationObserver||null===WebKitMutationObserver||void 0!==r&&null!==r||(r=WebKitMutationObserver),o=new r(function(e){var t,n,i;for(n=0,i=e.length;i>n;n++)t=e[n],"attributes"===t.type&&a.attrMutationHandler(t.attributeName,t.oldValue,a.elem.attr(t.attributeName))}),o.observe(t,{attributes:!0,attributeOldValue:!0,attributeFilter:["class","style","min","max","step"]})):"undefined"!=typeof MutationEvent&&null!==MutationEvent&&this.elem.on("DOMAttrModified",function(e){a.attrMutationHandler(e.originalEvent.attrName,e.originalEvent.prevValue,e.originalEvent.newValue)})},n.polyfills=[],n.isNumber=function(e){return null!=e&&"function"==typeof e.toString?/^-?\d+(?:\.\d+)?$/.test(""+e):!1},n.isFloat=function(e){return null!=e&&"function"==typeof e.toString?/^-?\d+\.\d+$/.test(""+e):!1},n.isInt=function(e){return null!=e&&"function"==typeof e.toString?/^-?\d+$/.test(""+e):!1},n.isNegative=function(e){return null!=e&&"function"==typeof e.toString?/^-\d+(?:\.\d+)?$/.test(""+e):!1},n.raiseNum=function(e){var t,i,r;if("number"==typeof e||"object"==typeof e&&e instanceof Number)return e%1?{num:""+e,precision:0}:n.raiseNum(""+e);if("string"==typeof e||"object"==typeof e&&e instanceof String){if(n.isFloat(e))return e=e.replace(/(\.\d)0+$/,"$1"),r=n.getPrecision(e),i=e.slice(0,-(r+1))+e.slice(-r),i=i.replace(/^(-?)0+(\d+)/,"$1$2"),t={num:i,precision:r};if(n.isInt(e))return{num:e,precision:0}}},n.raiseNumPrecision=function(e,n){var i,r;if(n>e.precision){for(t=i=r=e.precision;n>=r?n>i:i>n;t=n>=r?++i:--i)e.num+="0";e.precision=n}},n.lowerNum=function(e){if(e.precision>0){for(;e.num.length<e.precision+1;)e.num=n.isNegative(e.num)?e.num.slice(0,1)+"0"+e.num.slice(1):"0"+e.num;return(e.num.slice(0,-e.precision)+"."+e.num.slice(-e.precision)).replace(/\.?0+$/,"").replace(/^(-?)(\.)/,"$10$2")}return e.num},n.preciseAdd=function(e,t){var i,r,o;if(("number"==typeof e||"object"==typeof e&&e instanceof Number)&&("number"==typeof t||"object"==typeof t&&t instanceof Number))return 0===e%1&&0===t%1?""+(e+t):n.preciseAdd(""+e,""+t);if(("string"==typeof e||"object"==typeof e&&e instanceof String)&&("string"==typeof t||"object"==typeof t&&t instanceof String)){if(n.isNumber(e)){if(n.isNumber(t)){if(n.isInt(e)){if(n.isInt(t))return n.preciseAdd(parseInt(e,10),parseInt(t,10));n.isFloat(t)&&(e+=".0")}else n.isFloat(e)&&n.isInt(t)&&(t+=".0");if(i=n.raiseNum(e),r=n.raiseNum(t),i.precision<r.precision?n.raiseNumPrecision(i,r.precision):i.precision>r.precision&&n.raiseNumPrecision(r,i.precision),o=""+(parseInt(i.num,10)+parseInt(r.num,10)),i.precision>0){if(n.isNegative(o))for(;i.precision>o.length-1;)o="-0"+o.slice(1);else for(;i.precision>o.length;)o="0"+o;o=n.lowerNum({num:o,precision:i.precision})}return o=o.replace(/^(-?)\./,"$10."),n.isFloat(o)&&(o=o.replace(/0+$/,"")),o}throw new SyntaxError('Argument "'+t+'" is not a number.')}throw new SyntaxError('Argument "'+e+'" is not a number.')}return n.preciseAdd(""+e,""+t)},n.preciseSubtract=function(e,t){return"number"==typeof t||"object"==typeof t&&t instanceof Number?n.preciseAdd(e,-t):"string"==typeof t||"object"==typeof t&&t instanceof String?n.isNegative(t)?n.preciseAdd(e,t.slice(1)):n.preciseAdd(e,"-"+t):void 0},n.getPrecision=function(e){var t,i;if("number"==typeof e){for(t=0,i=e;i!==Math.floor(i);)i=e*Math.pow(10,++t);return t}return"string"==typeof e&&n.isNumber(e)?n.isFloat(e)?/^-?\d+(?:\.(\d+))?$/.exec(e)[1].length:0:void 0},n.prototype.getParams=function(){var e,t,i,r;return i=this.elem.attr("step"),t=this.elem.attr("min"),e=this.elem.attr("max"),r=this.elem.val(),n.isNumber(i)||(i=null),n.isNumber(t)||(t=null),n.isNumber(e)||(e=null),n.isNumber(r)||(r=t||0),{min:null!=t?t:null,max:null!=e?e:null,step:null!=i?i:"1",val:null!=r?r:null}},n.prototype.clipValues=function(e,t,n){return null!=n&&parseFloat(e)>parseFloat(n)?n:null!=t&&parseFloat(e)<parseFloat(t)?t:e},n.prototype.stepNormalize=function(e){var t,i,r,o,s;return r=this.getParams(),s=r.step,i=r.min,null==s?e:(s=n.raiseNum(s),t=n.raiseNum(e),t.precision>s.precision?n.raiseNumPrecision(s,t.precision):t.precision<s.precision&&n.raiseNumPrecision(t,s.precision),null!=i&&(t=n.raiseNum(n.preciseSubtract(e,i)),n.raiseNumPrecision(t,s.precision)),0===parseFloat(t.num)%parseFloat(s.num)?e:(t=n.lowerNum({num:""+Math.round(parseFloat(t.num)/(o=parseFloat(s.num)))*o,precision:t.precision}),null!=i&&(t=n.preciseAdd(t,i)),t))},n.domMouseScrollHandler=function(e){var t;t=e.data.p,e.preventDefault(),0>e.originalEvent.detail?t.increment():t.decrement()},n.mouseWheelHandler=function(e){var t;t=e.data.p,e.preventDefault(),e.originalEvent.wheelDelta>0?t.increment():t.decrement()},n.elemKeypressHandler=function(e){var t,n,i;t=e.data.p,38===e.keyCode?t.increment():40===e.keyCode?t.decrement():8!==(n=e.keyCode)&&9!==n&&35!==n&&36!==n&&37!==n&&39!==n&&46!==n&&45!==(i=e.which)&&46!==i&&48!==i&&49!==i&&50!==i&&51!==i&&52!==i&&53!==i&&54!==i&&55!==i&&56!==i&&57!==i&&e.preventDefault()},n.elemChangeHandler=function(e){var t,i,r,o;r=e.data.p,""!==r.elem.val()&&(n.isNumber(r.elem.val())?(o=r.getParams(),i=r.clipValues(o.val,o.min,o.max),i=r.stepNormalize(i),""+i!==r.elem.val()&&r.elem.val(i).change()):(t=r.elem.attr("min"),r.elem.val(null!=t&&n.isNumber(t)?t:"0").change()))},n.elemBtnMousedownHandler=function(t){var n,i,r,o,s=this;i=t.data.p,n=t.data.func,i[n](),o=function(){i[n](),i.timeoutID=window.setTimeout(o,10)},r=function(){window.clearTimeout(i.timeoutID),e(document).off("mouseup",r),e(s).off("mouseleave",r)},e(document).on("mouseup",r),e(this).on("mouseleave",r),i.timeoutID=window.setTimeout(o,700)},n.prototype.attrMutationHandler=function(e){var n,i,r,o,s;if("class"===e||"style"===e){for(i={},n=null,s=["opacity","visibility","-moz-transition-property","-moz-transition-duration","-moz-transition-timing-function","-moz-transition-delay","-webkit-transition-property","-webkit-transition-duration","-webkit-transition-timing-function","-webkit-transition-delay","-o-transition-property","-o-transition-duration","-o-transition-timing-function","-o-transition-delay","transition-property","transition-duration","transition-timing-function","transition-delay"],r=0,o=s.length;o>r;r++)t=s[r],(n=this.elem.css(t))!==this.btnContainer.css(t)&&(i[t]=n);i.display="none"===this.elem.css("display")?"none":"inline-block",this.btnContainer.css(i)}else("min"===e||"max"===e||"step"===e)&&this.elem.change()},n.prototype.increment=function(){var e,t;this.elem.is(":disabled")||this.elem.is("[readonly]")||(t=this.getParams(),e=n.preciseAdd(t.val,t.step),null!=t.max&&parseFloat(e)>parseFloat(t.max)&&(e=t.max),e=this.stepNormalize(e),this.elem.val(e).change())},n.prototype.decrement=function(){var e,t;this.elem.is(":disabled")||this.elem.is("[readonly]")||(t=this.getParams(),e=n.preciseSubtract(t.val,t.step),null!=t.min&&parseFloat(e)<parseFloat(t.min)&&(e=t.min),e=this.stepNormalize(e),this.elem.val(e).change())},e(function(){e('input[type="number"]').inputNumber()}),void 0)})(jQuery)}).call(this);

/*
 * WooCommerce Quantity Increment
 */
jQuery( function( $ ) {
	if ( ! String.prototype.getDecimals ) {
		String.prototype.getDecimals = function() {
			var num = this,
				match = ('' + num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
			if ( ! match ) {
				return 0;
			}
			return Math.max( 0, ( match[1] ? match[1].length : 0 ) - ( match[2] ? +match[2] : 0 ) );
		}
	}

	function wcqi_refresh_quantity_increments(){
		$( '.single-product div.quantity:not(.buttons_added), .single-product td.quantity:not(.buttons_added)' ).addClass( 'buttons_added' ).append( '<div class="plus" />' ).prepend( '<div class="minus" />' );
	}

	$( document ).on( 'updated_wc_div', function() {
		wcqi_refresh_quantity_increments();
	} );

	$( document ).on( 'click', '.plus, .minus', function() {
		// Get values
		var $qty		= $( this ).closest( '.quantity' ).find( '.qty'),
			currentVal	= parseFloat( $qty.val() ),
			max			= parseFloat( $qty.attr( 'max' ) ),
			min			= parseFloat( $qty.attr( 'min' ) ),
			step		= $qty.attr( 'step' );

		// Format values
		if ( ! currentVal || currentVal === '' || currentVal === 'NaN' ) currentVal = 0;
		if ( max === '' || max === 'NaN' ) max = '';
		if ( min === '' || min === 'NaN' ) min = 0;
		if ( step === 'any' || step === '' || step === undefined || parseFloat( step ) === 'NaN' ) step = 1;

		// Change the value
		if ( $( this ).is( '.plus' ) ) {
			if ( max && ( currentVal >= max ) ) {
				$qty.val( max );
			} else {
				$qty.val( ( currentVal + parseFloat( step )).toFixed( step.getDecimals() ) );
			}
		} else {
			if ( min && ( currentVal <= min ) ) {
				$qty.val( min );
			} else if ( currentVal > 0 ) {
				$qty.val( ( currentVal - parseFloat( step )).toFixed( step.getDecimals() ) );
			}
		}

		// Trigger change event
		$qty.trigger( 'change' );
	});

	wcqi_refresh_quantity_increments();
});

/*
 * WooCommerce slider, zoom, effect
 */
(function($) {
	"use strict";

	/* slider */
	$(window).load(function() {

		//product thumbnails
		$('.single-product .thumbnails').flexslider({
			animation: "fade",
			controlNav: false,
			animationLoop: false,
			slideshow: false,
			itemWidth: 60,
			animationSpeed: 100,
			asNavFor: '.flexslider'
		});

		//product image
		$(".single-product .image_post .flexslider").flexslider({
			controlNav: false,
			directionNav: false,
			slideshow: false,
			animationSpeed: 100,
			sync: ".thumbnails",
		});


		//if thumbnails > slides
		var $sprodthumb = $('.single-product div.product div.images div.thumbnails').height();
		var $sprodslide = $('.single-product div.product .flexslider .slides li').height();
		if ($sprodthumb > $sprodslide) {
			$('.single-product div.product div.images div.thumbnails').css({
				'height': $sprodslide,
				'overflow': 'hidden',
				'overflow-y': 'scroll',
			});
		}


	});


	/* zoom */
	var mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
	if(!mobile){
		$('.single-product .flexslider .slides li a').zoom({
			magnify:1
		});
	}

	// remove zoom if img is too small
	$(window).load(function() {
		var $horimg = $('.single-product img.attachment-shop_single').height();
		var $worimg = $('.single-product img.attachment-shop_single').width();
		var $hzoimg = $('.single-product img.zoomImg').height();
		var $wzoimg = $('.single-product img.zoomImg').width();

		if(($horimg + $worimg + 5) > ($hzoimg + $wzoimg)) {
			$('.single-product img.zoomImg').remove();
		}
	});

	// zoom after select
	$(window).load(function() {
		$('.single-product form.variations_form').on( 'found_variation', function() {
			var $horimg = $('.single-product img.attachment-shop_single').height();
			var $worimg = $('.single-product img.attachment-shop_single').width();
			var $hzoimg = $('.single-product img.zoomImg').height();
			var $wzoimg = $('.single-product img.zoomImg').width();

			$('.single-product .flexslider .slides li a').trigger('zoom.destroy');
			// zoom if img is large enough
			if(($horimg + $worimg + 5) < ($hzoimg + $wzoimg)) {
				$('.single-product .flexslider .slides li a').zoom({
					magnify:1
				});
			}
		});
		//add alt attribute for zoomImg
		$('.single-product .zoomImg').attr('alt', '');
	});

	//arrow and pp_gallery > pp_full_res
	var $arrowtp = $(window).height()/2;
	$(".single-product div.product .flexslider .slides li a").click(function() {
		setTimeout(function() {
			if($('.single-product .pp_pic_holder')){
				$('.single-product a.pp_next, .single-product a.pp_previous').css('top', $arrowtp).css('opacity', '1');
			}

			//if pp_gallery > pp_full_res
			var $sprettygal = $('.single-product .pp_gallery').height();
			var $sprettyfull = $('.single-product #pp_full_res').height();
			if($sprettygal > $sprettyfull) {
				$('.single-product .pp_gallery').css({
					'height': $sprettyfull,
					'overflow': 'hidden',
					'overflow-y': 'scroll',
				});
			}

		}, 100);
	});

	$(window).resize(function() {
		var $arrowtp = $(window).height()/2;
		$('.single-product a.pp_next, .single-product a.pp_previous').css('top', $arrowtp).css('opacity', '1');
	});


	/* smooth scrolling when clicking woocommerce-review-link */
	$('.single-product a.woocommerce-review-link').click(function(event) {
		if( window.location.hash ) {
			// Vars
			var tab = window.location.hash.replace('#', '');
			var tab_content = 'tab-' + tab;

			// Tabs
			$( 'li.description_tab' ).removeClass( 'active' );
			$( 'li.' + tab + '_tab' ).addClass( 'active' );

			// Tabs content
			$( '#tab-description' ).hide();
			$( '#' + tab_content ).show();
		}
		event.preventDefault();
		$('html, body').animate({
			scrollTop: $( $.attr(this, 'href') ).offset().top
		}, 700);
	});

	/* shop secondary thumb */
	$('.woocommerce .archive-product ul.products li.product').hover(
		function() {
			$(this).find('img.woondary-thumb').stop().fadeIn(300);
		},
		function () {
			$(this).find('img.woondary-thumb').stop().fadeOut(300);
 		}
	);

	/* header cart */
	$('.header-cart').hover(
		function() {
			$(this).find('.woocommerce.widget_shopping_cart').stop().fadeIn(300);
		},
		function () {
			$(this).find('.woocommerce.widget_shopping_cart').stop().fadeOut(300);
 		}
	);


})(jQuery);