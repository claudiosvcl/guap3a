<?php
/**
 * Gotham functions
 *
 * Set up the theme and provides some helper functions.
 * 
 */

/* widgets */
require get_template_directory() . '/inc/widgets/flickr-widget.php';
require get_template_directory() . '/inc/widgets/recent-posts-widget.php';
require get_template_directory() . '/inc/widgets/popular-projects-widget.php';
require get_template_directory() . '/inc/widgets/random-posts-widget.php';
/* post like */
require get_template_directory() . '/inc/post-like.php';
/* autocomplete */
require get_template_directory() . '/inc/autocomplete.php';
/* Customizer additions */
require get_template_directory() . '/inc/customize-setting-image-data.php';
require get_template_directory() . '/inc/customizer.php';
/* extends Walker_Nav_Menu */
require get_template_directory() . '/inc/menu.php';
/* meta boxes */
require get_template_directory() . '/inc/cmb2/custom-meta-boxes.php';
require get_template_directory() . '/inc/cmb2/init.php';
/* WooCommerce */
if ( class_exists( 'WooCommerce' ) ) {
  require get_template_directory() . '/inc/woocommerce-functions.php';
}

if ( ! function_exists( 'gotham_setup' ) ) :
/** 
 * gotham_setup
 */
function gotham_setup() {
  // Set the content width based on the theme's design and stylesheet.
  if ( ! isset( $content_width ) ) {
   $content_width = 1070; /* pixels */
  }
  // Make theme available for translation.
  load_theme_textdomain( 'gotham', get_template_directory() . '/languages' );
  // Enable support for Post Thumbnails on posts and pages.
  add_theme_support( 'post-thumbnails' );
  // This theme uses wp_nav_menu() in one location.
  register_nav_menus( array(
  'primary' => esc_html__( 'Primary Menu', 'gotham' ),
  ) );
  // Add default posts and comments RSS feed links to head.
  add_theme_support( 'automatic-feed-links' );
  // Let WordPress manage the document title
  add_theme_support( 'title-tag' );
  // Enable support for Post Formats.
  add_theme_support( 'post-formats', array(
  'audio', 'link', 'video', 'quote'
  ) );
  // Switch default core markup for search form, comment form, and comments to output valid HTML5.
  add_theme_support( 'html5', array(
    'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
  ) );
  
  // Add editor styles
  add_editor_style( array( 'css/editor-style.css', gotham_google_fonts() ) );

}
endif;
add_action( 'after_setup_theme', 'gotham_setup' );

/**
 * title-tag add backwards compatibility for older versions
 */
if ( version_compare( $GLOBALS['wp_version'], '4.1', '<' ) ) {
  if ( ! function_exists( '_wp_render_title_tag' ) ) {
    function gotham_theme_slug_render_title() { ?>
      <title><?php wp_title( '|', true, 'right' ); ?></title>
    <?php }
    add_action( 'wp_head', 'gotham_theme_slug_render_title' );
  }
}

if ( ! function_exists( 'gotham_widgets_init' ) ) :
/**
 * register widget area
 */
function gotham_widgets_init() {

  register_sidebar( array(
  'name' => esc_html__( 'Main', 'gotham' ),
  'id' => 'main',
  'description' => esc_html__( 'Add widgets here to appear in your sidebar. To display this sidebar, select "Main" in the options page.', 'gotham' ),
  'before_widget' => '<div id="%1$s" class="%2$s">',
  'after_widget' => '</div>',
  'before_title' => '<h3>',
  'after_title' => '</h3>',
  ) );

  register_sidebar( array(
  'name' => esc_html__( 'Secondary', 'gotham' ),
  'id' => 'secondary',
  'description' => esc_html__( 'Add widgets here to appear in your sidebar. To display this sidebar, select "Secondary" in the options page.', 'gotham' ),
  'before_widget' => '<div id="%1$s" class="%2$s">',
  'after_widget' => '</div>',
  'before_title' => '<h3>',
  'after_title' => '</h3>',
  ) );

  register_sidebar( array(
  'name' => esc_html__( 'Third', 'gotham' ),
  'id' => 'third',
  'description' => esc_html__( 'Add widgets here to appear in your sidebar. To display this sidebar, select "Third" in the options page.', 'gotham' ),
  'before_widget' => '<div id="%1$s" class="%2$s">',
  'after_widget' => '</div>',
  'before_title' => '<h3>',
  'after_title' => '</h3>',
  ) );

  register_sidebar( array(
  'name' => esc_html__( 'Fourth', 'gotham' ),
  'id' => 'fourth',
  'description' => esc_html__( 'Add widgets here to appear in your sidebar. To display this sidebar, select "Fourth" in the options page.', 'gotham' ),
  'before_widget' => '<div id="%1$s" class="%2$s">',
  'after_widget' => '</div>',
  'before_title' => '<h3>',
  'after_title' => '</h3>',
  ) );

}
endif;
add_action( 'widgets_init', 'gotham_widgets_init' );

if ( ! function_exists( 'gotham_google_fonts' ) ) :
/** 
 * Google fonts
 */
function gotham_google_fonts() {
  $google_web_fonts = '';
  $fonts_weight_style = ':100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic';

  $mfontf = get_option('main_font_family', 'Montserrat').''.$fonts_weight_style.'';
  if(( is_customize_preview() )&&(get_option('main_font_family') == '')||is_admin()) {
    $mfontf = 'Montserrat';
  }

  $sfontf = get_option('secondary_font_family', 'Crimson Text').''.$fonts_weight_style.'';
  if(( is_customize_preview() )&&(get_option('secondary_font_family') == '')) {
    $sfontf = 'Crimson Text';
  }

  if ( 'off' !== _x( 'on', 'Google font: on or off', 'gotham' ) ) {
    $google_web_fonts = add_query_arg( 'family', urlencode( ''.$mfontf.'|'.$sfontf.'' ), "//fonts.googleapis.com/css" );
  }

  return $google_web_fonts;
}
endif;


if ( ! function_exists( 'gotham_adding_scripts' ) ) :
/**
 * add scripts and styles
 */
function gotham_adding_scripts() {

  wp_register_script('gotham-plugins', get_template_directory_uri() . '/js/plugins.min.js', 'jquery', '',true);
  wp_register_script('gotham-main-js', get_template_directory_uri() . '/js/main.min.js', array('jquery', 'gotham-plugins'), '',true);

  wp_enqueue_script('jquery-ui-autocomplete');
  wp_enqueue_script('gotham-plugins');
  wp_enqueue_script('gotham-main-js');

  // post like
  wp_localize_script('gotham-main-js', 'ajax_var', array(
  'url' => admin_url('admin-ajax.php'),
  'nonce' => wp_create_nonce('ajax-nonce') ));

  // autocomplete
  wp_localize_script('gotham-main-js', 'gotacsearch', array(
  'url' => admin_url('admin-ajax.php')));


  // thread comments
  if ( is_singular() && comments_open() && get_option('thread_comments') ) {
    wp_enqueue_script( "comment-reply" );
  }

  //css

  wp_register_style('gotham-fonts', get_template_directory_uri() . '/css/fonts.min.css');
  wp_register_style('gotham-main', get_template_directory_uri() . '/css/main.min.css');

  wp_enqueue_style('gotham-gfonts', gotham_google_fonts(), array(), null);
  wp_enqueue_style('gotham-fonts');
  wp_enqueue_style('gotham-main');

}
endif;
add_action('wp_enqueue_scripts', 'gotham_adding_scripts');

if ( ! function_exists( 'gotham_custom_css' ) ) :
/**
 * dynamic styles with ajax
 */
function gotham_custom_css() {
  $postidcs = get_the_ID();

  if(class_exists( 'WooCommerce' ) && (is_shop()||is_product_category()||is_product_tag())) {
    $postidcs = wc_get_page_id('shop');
  }

  wp_enqueue_style( 'gotham-custom', admin_url('admin-ajax.php').'?action=custom-css&post-id='.$postidcs.'' );
}
endif;
add_action( 'wp_enqueue_scripts', 'gotham_custom_css' );

if ( ! function_exists( 'gotham_dynamic_css' ) ) :
function gotham_dynamic_css() {
  require(get_template_directory().'/css/custom-css.php');
  exit;
}
endif;
add_action('wp_ajax_custom-css', 'gotham_dynamic_css');
add_action('wp_ajax_nopriv_custom-css', 'gotham_dynamic_css');

if ( ! function_exists( 'gotham_custom_admin_scripts' ) ) :
/**
 * Scripts for back end
 */
function gotham_custom_admin_scripts() {
  wp_register_script('gotham-custom-admin', get_template_directory_uri() . '/js/custom-admin.min.js', 'jquery');
  wp_enqueue_script('gotham-custom-admin');
}
endif;
add_action('admin_enqueue_scripts', 'gotham_custom_admin_scripts');

if ( ! function_exists( 'gotham_minhdn' ) ) :
/**
 * min-height dynamic
 */
function gotham_minhdn() {
  $title_area = get_post_meta( get_the_ID(), 'gotham_select_title_area', true );

  if ( $title_area != "no") {   
   $settings = array( 'hpy' => 'hpys' );
  }
  else {
    $settings = array( 'hpf' => 'hpst' );
  }

  if((!is_singular('post'))&&(!is_singular('portfolio'))) {
     wp_localize_script( 'gotham-main-js', 'php_vars', $settings );
  }
}
endif;
add_action('wp_enqueue_scripts', 'gotham_minhdn');

/**
 * dynamic styles (inline_style)
 */
require get_template_directory() . '/inc/dynamic-style.php';

if ( ! function_exists( 'gotham_custom_excerpt_length' ) ) :
/**
 * excerpt length in index.php, archive.php, search.php
 * standard blog, masonry blog
 */
function gotham_custom_excerpt_length(){
  $get_excerpt = get_the_excerpt();
    $the_excerpt = '';
    if ($get_excerpt != "") {
      if (strlen($get_excerpt) > 116) {
        $the_excerpt = substr($get_excerpt,0,116).'...';
        return $the_excerpt;
      }
      else {
        $the_excerpt=$get_excerpt;
        return $the_excerpt;
      }
    }
}
endif;

if ( ! function_exists( 'gotham_excerpt_more' ) ) :
/** 
 * The excerpt "read more"
 */
function gotham_excerpt_more($more) {
  return '...';
}
endif;
add_filter('excerpt_more', 'gotham_excerpt_more');

if ( ! function_exists( 'gotham_the_breadcrumb' ) ) :
/**
 * breadcrumbs
 */
function gotham_the_breadcrumb() {
  if (!is_home()) {
    echo '<a href="'; 
    echo home_url(); 
    echo '">'; bloginfo('name'); 
    echo "</a> <i class='cr-bullet'></i> "; 
    if (is_page()) {
    echo the_title(); 
    }
  } 
}
endif;
add_action('breadcrumb', 'gotham_the_breadcrumb');

if ( ! function_exists( 'gotham_comment' ) ) :
/**
 * comment-list
 */
function gotham_comment($comment, $args, $depth) {
  $GLOBALS['comment'] = $comment;
  extract($args, EXTR_SKIP);
  ?>
  <li <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
  <div id="div-comment-<?php comment_ID() ?>" class="comment-global">
    <div class="avatar">
      <?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['avatar_size'] ); ?>
    </div>
    <div class="comment-text"> 
      <?php printf(wp_kses( __('<cite class="fn">By %s</cite>', 'gotham'), array( 'cite' => array( 'class' => array() ) ) ), get_comment_author_link()) ?>
      <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
        <?php
        /* translators: 1: date, 2: time */
        printf( esc_html__('%1$s at %2$s', 'gotham'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link('(Edit)','  ','' );
        ?>
      </div>
      <?php if ($comment->comment_approved == '0') : ?>
      <em class="comment-awaiting-moderation"><?php esc_html_e('Your comment is awaiting moderation.', 'gotham') ?></em>
      <br />
      <?php endif; ?>
      <?php comment_text() ?>
    </div>
    <div class="reply">
      <?php comment_reply_link(array_merge( $args, array('add_below' => 'div-comment', 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
    </div>
  </div>
  <?php

}
endif;

if ( ! function_exists( 'gotham_comments_number' ) ) :
/**
 * number of comments in index.php, archive.php
 * standard blog, masonry blog
 */
function gotham_comments_number() {

  $num_comments = get_comments_number(); // get_comments_number returns only a numeric value

if ( comments_open() ) {
  if ( $num_comments == 0 ) {
    $comments = esc_html__('No Comments', 'gotham');
  } elseif ( $num_comments > 1 ) {
    $comments = $num_comments . esc_html__(' Comments', 'gotham');
  } else {
    $comments = esc_html__('1 Comment', 'gotham');
  }
  return '<a href="' . get_comments_link() .'">'. $comments.'</a>';
} else {
  return  '<a href="#">'.esc_html__('Comments off', 'gotham').'</a>';
}

}
endif;

if ( ! function_exists( 'gotham_tag_cloud' ) ) :
/**
 * tags cloud
 */
function gotham_tag_cloud($tag_arg) {
  return preg_replace("/style='font-size:.+pt;'/", '', $tag_arg);
}
endif;
add_filter ('wp_generate_tag_cloud', 'gotham_tag_cloud');

if ( ! function_exists( 'gotham_searchfilter' ) ) :
/** 
 * search results
 */
function gotham_searchfilter($query) {
  if ($query->is_search) {
    $query->set('post_type', array('post', 'page', 'portfolio'));
    if( class_exists( 'WooCommerce' ) ) {
      $query->set('post_type', array('post', 'page', 'portfolio', 'product'));
    }
  }
  return $query;
}
endif;
add_filter('pre_get_posts','gotham_searchfilter');

if ( ! function_exists( 'gotham_wp_link_pages_modif' ) ) :
/**
 * add element in wp_link_pages
 */
function gotham_wp_link_pages_modif($link) {
  if(strpos($link, '<a')!== false) {
    $link = '<li>' .$link . '</li>';
  }
  else {
    $link = '<li class="active"><a href="#">'.$link.'</a></li>';
  }
  return $link;
}
endif;
add_filter('wp_link_pages_link', 'gotham_wp_link_pages_modif');

/**
 * add option user profile
 */
global $current_user; // Use global
wp_get_current_user(); // Make sure global is set, if not set it.
//if it's not suscriber
if ( ! user_can( $current_user, "subscriber" ) ) {
add_action( 'show_user_profile', 'gotham_custom_user_profile_field' );
add_action( 'edit_user_profile', 'gotham_custom_user_profile_field' );
if ( ! function_exists( 'gotham_custom_user_profile_field' ) ) :
function gotham_custom_user_profile_field( $user ) { ?>

  <h3>Extra profile information</h3>

  <table class="form-table">

    <tr>
      <th><label for="designation">Designation</label></th>

      <td>
        <input type="text" name="designation" id="designation" value="<?php echo esc_attr( get_the_author_meta( 'designation', $user->ID ) ); ?>" class="regular-text" /><br />
        <span class="description">Enter designation username.</span>
      </td>
    </tr>

    <tr>
      <th><label for="twitter">twitter</label></th>

      <td>
        <input type="text" name="twitter" id="twitter" value="<?php echo esc_attr( get_the_author_meta( 'twitter', $user->ID ) ); ?>" class="regular-text" /><br />
        <span class="description">Enter twitter username.</span>
      </td>
    </tr>

    <tr>
      <th><label for="facebook">facebook</label></th>

      <td>
        <input type="text" name="facebook" id="facebook" value="<?php echo esc_attr( get_the_author_meta( 'facebook', $user->ID ) ); ?>" class="regular-text" /><br />
        <span class="description">Enter facebook username.</span>
      </td>
    </tr>

    <tr>
      <th><label for="github">github</label></th>

      <td>
        <input type="text" name="github" id="github" value="<?php echo esc_attr( get_the_author_meta( 'github', $user->ID ) ); ?>" class="regular-text" /><br />
        <span class="description">Enter github username.</span>
      </td>
    </tr>

    <tr>
      <th><label for="dribbble">dribbble</label></th>

      <td>
        <input type="text" name="dribbble" id="dribbble" value="<?php echo esc_attr( get_the_author_meta( 'dribbble', $user->ID ) ); ?>" class="regular-text" /><br />
        <span class="description">Enter dribbble username.</span>
      </td>
    </tr>

  </table>
<?php }
endif;

add_action( 'personal_options_update', 'gotham_update_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'gotham_update_extra_profile_fields' );
if ( ! function_exists( 'gotham_update_extra_profile_fields' ) ) :
function gotham_update_extra_profile_fields( $user_id ) {
  if ( current_user_can('edit_user',$user_id) ) {

  /* Copy and paste this line for additional fields. Make sure to change 'designation' to the field ID. */
  update_user_meta( $user_id, 'designation', $_POST['designation'] );
  update_user_meta( $user_id, 'twitter', $_POST['twitter'] );
  update_user_meta( $user_id, 'facebook', $_POST['facebook'] );
  update_user_meta( $user_id, 'github', $_POST['github'] );
  update_user_meta( $user_id, 'dribbble', $_POST['dribbble'] );
  }
}
endif;
}


if ( ! function_exists( 'gotham_add_tc_classes' ) ) :
/**
* Add body class
**/
function gotham_add_tc_classes( $classes ) {
  global $post;
  if (isset($post->post_content) && has_shortcode( $post->post_content, 'blog')) {
    $classes[] .= 'tc_blog';
  }
  if (isset($post->post_content) && strpos( $post->post_content, 'layout="masonry_blog"')) {
    $classes[] .= 'tc_masonry_blog';
  }
  return $classes;
}
endif;
add_filter( 'body_class', 'gotham_add_tc_classes' );

if ( ! function_exists( 'gotham_my_password_form' ) ) :
/**
 * password form text
 */
function gotham_my_password_form() {
  global $post;
  $label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
  $o = '<form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" class="post-password-form" method="post">
  <div class="ppfodt"><div class="ppfodtc"><div class="ppfodsq"><p>' . esc_html__( 'This content is password protected. To view it please enter your password below:', 'gotham' ) . '</p>
  <p><input name="post_password" id="' . $label . '" type="password" size="20" placeholder="PASSWORD" class="pswfor" /><input type="submit" name="Submit" class="post-password-submit" value="' . esc_attr__( 'Submit', 'gotham' ) . '" /></p></div></div></div></form>
  ';
  return $o;
}
endif;
add_filter( 'the_password_form', 'gotham_my_password_form' );

if ( ! function_exists( 'gotham_add_ul_ol_class' ) ) :
/* add ul, ol class in post content (wp-editor) */
function gotham_add_ul_ol_class( $data, $postarr ) {
    $data['post_content'] = str_replace('<ul>', '<ul class="elul">', $data['post_content'] );
    $data['post_content'] = str_replace('<ol>', '<ol class="elol">', $data['post_content'] );
    return $data;
}
endif;
add_filter('wp_insert_post_data', 'gotham_add_ul_ol_class', '99', 2 );

if ( ! function_exists( 'gotham_comment_text_ulol' ) ) :
/* add ul, ol class in comment text */
function gotham_comment_text_ulol( $comment_text_ulol ) {
  $comment_text_ulol = str_replace('<ul>', '<ul class="elul">', $comment_text_ulol);
  $comment_text_ulol = str_replace('<ol>', '<ol class="elol">', $comment_text_ulol);
  return $comment_text_ulol;
};
endif;
add_filter( 'get_comment_text', 'gotham_comment_text_ulol' );

/**
 * Include the TGM_Plugin_Activation class.
 */
require_once dirname( __FILE__ ) . '/inc/class-tgm-plugin-activation.php';
/**
 * Include gotham-register-plugins.php
 */
require get_template_directory() . '/inc/gotham-register-plugins.php';

/**
 * Force Visual Composer to initialize as "built into the theme". This will hide certain tabs under the Settings->Visual Composer page
 */
if(function_exists('vc_set_as_theme')) vc_set_as_theme();

/**
 * Change default template directory for vc-templates
 */
if (class_exists('WPBakeryVisualComposerAbstract')) {
  $dir = get_stylesheet_directory() . '/inc/vc_templates';
  vc_set_shortcodes_templates_dir( $dir );
}

/**
 * remove plugin update notifications (for vc)
 */
if ( ! function_exists( 'gotham_filter_plugin_updates' ) ) :
function gotham_filter_plugin_updates( $value ) {
  $remove_update = array('js_composer/js_composer.php');
  foreach( $remove_update as $plugin )
  if( !empty( $value->response ) && array_key_exists( $plugin, $value->response ) )
  unset( $value->response[$plugin] );
  return $value;
}
endif;
add_filter( 'site_transient_update_plugins', 'gotham_filter_plugin_updates' );