<?php
/**
 * The template for displaying all single portfolio
 */

get_header(); ?>

<?php if(! post_password_required()) {?>

<?php
$video1 = get_post_meta( get_the_ID(), 'gotham_video1', 1 ); $image = apply_filters( '', get_post_meta( get_the_ID(), 'gotham_image', true ) ); $project_title = get_post_meta( get_the_ID(), 'gotham_project_title', true ); $about_project = get_post_meta( get_the_ID(), 'gotham_about_project', true ); $portfolio_presentation = get_post_meta( get_the_ID(), 'gotham_portfolio_presentation', true ); $portfolio_date = get_post_meta( get_the_ID(), 'gotham_portfolio_date', true ); $portfolio_url = get_post_meta( get_the_ID(), 'gotham_portfolio_url', true ); $portfolio_client = get_post_meta( get_the_ID(), 'gotham_portfolio_client', true ); $title_area = get_post_meta( get_the_ID(), 'gotham_select_title_area', true ); $title = get_post_meta( get_the_ID(), 'gotham_select_title', true ); $arrow_down = get_post_meta( get_the_ID(), 'gotham_arrow_down', true ); $share_port = get_post_meta( get_the_ID(), 'gotham_share_port', true ); $title_area_parallax = get_post_meta( get_the_ID(), 'gotham_title_area_parallax', true ); $colorpalette = get_post_meta( get_the_ID(), 'gotham_color_palette', true ); $colorp1 = get_post_meta( get_the_ID(), 'gotham_color_p1', true ); $colorp2 = get_post_meta( get_the_ID(), 'gotham_color_p2', true ); $colorp3 = get_post_meta( get_the_ID(), 'gotham_color_p3', true ); $colorp4 = get_post_meta( get_the_ID(), 'gotham_color_p4', true );

	$pjct_title = '';
	if ($project_title != "") {
		if (strlen($project_title) > 30) {
			$pjct_title = substr($project_title,0,30);
		}
		else {
			$pjct_title = $project_title;
		}
	}
?>

<div class="explore"><a href="#!"></a></div>
<div class="ct_overlay">
	<div class="spec">
		<div class="back-to"><a href="<?php echo esc_url(get_theme_mod('url_return_to'));?>"><span><?php echo esc_html(get_theme_mod('title_return_to', 'BACK TO GALLERY')); ?></span></a></div>
		<ul>
			<?php $crispy_the_query = new WP_Query( array(
				'showposts' => 10,
				'post_type' => array('portfolio'),
			)); ?>
			<?php while ($crispy_the_query -> have_posts()) : $crispy_the_query -> the_post(); ?>
			<li>
				<div class="explore_img">
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
					<?php the_post_thumbnail(); ?>
					</a>
				</div>
				<p><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></p>
			</li>
			<?php endwhile;?>
		</ul>
	</div>
</div>

<?php wp_reset_postdata(); ?>
<?php if ($title_area != "no") {?>
	<div class="below_header full"> 
		<div class="h-below full" <?php if ( $title_area_parallax != '' ){ ?> data-stellar-background-ratio="0.5" <?php } ?>>
			<div class="h-belowrapper">
				<div class="title-wr<?php if ($title_area == "title_area_center"){ ?> twcenter<?php } ?>">
					<?php if ($title != "no") {?>
						<h1 class="single-h1 sptf"> <?php the_title(); ?> </h1>
					<?php } ?>
					<?php if ($portfolio_client != "") { ?>
						<div class="pclient">
							<h3> <?php esc_html_e( "Client:", "gotham" ); ?> </h3>
							<p> <?php echo esc_html($portfolio_client); ?> </p>
						</div>
					<?php } ?>
					<?php if($arrow_down != "") {?>	
						<i class="md-icon-expand-more stdown"><small class="scr-down">SCROLL</small></i>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
<?php } ?>

<div class="portfolio-bg">
	<div id="content" class="spec portfolio-home">
		<section class="<?php if ( ($portfolio_presentation == "small_image")||($portfolio_presentation == "small_slider") ){ ?>rempad <?php } ?>
			<?php if ( ($portfolio_presentation == "big_image")||($portfolio_presentation == "big_slider") ){ ?>bibs <?php } ?>liftp">
			<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
			<?php if ($project_title != "") {?>
				<div class="project_title"> <p><?php echo esc_html($pjct_title); ?></p>
					<?php echo gotham_getPostLikeLink(get_the_ID());?>
				</div>
			<?php } ?>
			<?php if ($portfolio_presentation == "big_image") {?>
			<!-- Big Image -->
				<div class="image_post sbimg">
					<ul>
						<?php if (is_array($image)) : ?>
						<?php foreach($image as $attachment_id => $image): ?>
						<li>
							<img class="lazy" data-original="<?php echo esc_url(wp_get_attachment_url($attachment_id)); ?>" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQI12P4zwAAAgEBAKrChTYAAAAASUVORK5CYII=" title="<?php echo esc_attr(get_the_title($attachment_id));?>" alt="<?php echo esc_attr(get_post_meta($attachment_id, '_wp_attachment_image_alt', true)); ?>">
						</li>
						<?php  endforeach; ?>
						<?php  endif; ?>
						<?php if (is_array($video1)) : ?>
						<?php foreach($video1 as $video => $video1) : ?>
							<li>
								<div class="video_post">
									<?php echo wp_oembed_get(esc_url($video1)); ?>
								</div> 
							</li>
						<?php endforeach; ?>
						<?php elseif($video1 != "") : /* old version */?>
						<li>
							<div class="video_post">
								<?php echo wp_oembed_get(esc_url($video1)); ?>
							</div> 
						</li>
						<?php ?>
						<?php endif;?>
					</ul>
					<?php if ($share_port != "no") {?>
						<div class="share-opf">
							<button class="md-icon-email"></button>
							<button class="iconfa-facebook"></button>
							<button class="iconfa-twitter"></button>
							<button class="md-icon-share"></button>
						</div>
					<?php } ?>
				</div>

				<div class="column75-25 bibs">
					<div class="column75-25 inner1">
						<div class="portfolio-content">
							<h3> <?php esc_html_e( "ABOUT THIS PROJECT", "gotham" ) ?> </h3>
							<?php if ($about_project != "") {?>
								<p><?php echo wp_kses($about_project, array('br'=> array(), 'strong' => array())); ?></p>
							<?php } ?>
						</div>
						<?php wp_link_pages(array('before' => '<div class="link_pages"><ul>', 'after' => '</ul></div>', 'pagelink' => '<span>%</span>')); ?>
						<?php if ($portfolio_url != "") {?>
							<div class="purl">
								<a href="<?php echo esc_url($portfolio_url); ?>"> <?php esc_html_e( "VISIT WEBSITE", "gotham" ); ?> </a>
							</div>
						<?php } ?>
						<?php if ($colorpalette != "") {?>
							<div class="palette">
								<span> <?php esc_html_e( "PALETTE", "gotham" ); ?> </span>
							</div>
						<?php } ?>
					</div>
					<div class="column75-25 inner2 meta-area">
						<?php if ($portfolio_date != "") { ?> 
						<div class="pdate">
							<h3> <?php esc_html_e( "Date", "gotham" ); ?> </h3>
							<p> <?php echo esc_html($portfolio_date); ?> </p>
						</div>
						<?php } ?>

						<div class="portfolio-category">
							<h3> <?php esc_html_e( "Category", "gotham" ); ?> </h3>
							<p><?php $custom = wp_get_post_terms(get_the_ID(), 'portfolio_category'); $t = count($custom); $counter = 0;
							foreach($custom as $custom) {
								$counter++; 
								echo $custom->name; if ($counter < $t) echo ', ';
							}?>
							</p>
						</div>
					</div>
				</div>
			<?php }
			elseif ($portfolio_presentation == "big_slider") {?>
			<!-- Big slider -->
				<div class="image_post">
					<div class="flexslider">
						<ul class="slides">
							<?php if (is_array($image)) : ?>
							<?php foreach($image as $attachment_id => $image): ?>
							<li>
								<img src="<?php echo esc_url(wp_get_attachment_url($attachment_id)); ?>" title="<?php echo esc_attr(get_the_title($attachment_id)); ?>" alt="<?php echo esc_attr(get_post_meta($attachment_id, '_wp_attachment_image_alt', true)); ?>">
							</li>
							<?php endforeach; ?>
							<?php endif; ?>
							<?php if (is_array($video1)) : ?>
							<?php foreach($video1 as $video => $video1) : ?>
								<li>
									<div class="video_post">
										<?php echo wp_oembed_get(esc_url($video1)); ?>
									</div> 
								</li>
							<?php endforeach; ?>
							<?php elseif($video1 != "") : /* old version */?>
							<li>
								<div class="video_post">
									<?php echo wp_oembed_get(esc_url($video1)); ?>
								</div> 
							</li>
							<?php ?>
							<?php endif;?>
						</ul>
						<?php if ($share_port != "no") {?>
						<div class="share-opf">
							<button class="md-icon-email"></button>
							<button class="iconfa-facebook"></button>
							<button class="iconfa-twitter"></button>
							<button class="md-icon-share"></button>
						</div>
						<?php } ?>
					</div>
				</div>

				<div class="column75-25 bibs">
					<div class="column75-25 inner1">
						<div class="portfolio-content">
							<h3> <?php esc_html_e( "ABOUT THIS PROJECT", "gotham" ); ?> </h3>
							<?php if ($about_project != "") {?>
								<p><?php echo wp_kses($about_project, array('br'=> array(), 'strong' => array())); ?></p>
							<?php } ?>
						</div>
						<?php wp_link_pages(array('before' => '<div class="link_pages"><ul>', 'after' => '</ul></div>', 'pagelink' => '<span>%</span>')); ?>
						<?php if ($portfolio_url != "") {?>
							<div class="purl">
								<a href="<?php echo esc_url($portfolio_url); ?>"> <?php esc_html_e( "VISIT WEBSITE", "gotham" ); ?> </a>
							</div>
						<?php } ?>
						<?php if ($colorpalette != ""){?>
							<div class="palette">
								<span> <?php esc_html_e( "PALETTE", "gotham" ); ?> </span>
							</div>
						<?php } ?>
					</div>
					<div class="column75-25 inner2 meta-area">
						<?php if ($portfolio_date != "") { ?> 
						<div class="pdate">
							<h3> <?php esc_html_e( "Date", "gotham" ); ?> </h3>
							<p> <?php echo esc_html($portfolio_date); ?> </p>
						</div>
						<?php } ?>

						<div class="portfolio-category" > 
							<h3> <?php esc_html_e( "Category", "gotham" ) ?> </h3>
							<p><?php $custom = wp_get_post_terms(get_the_ID(), 'portfolio_category'); $t = count($custom); $counter = 0;
							foreach($custom as $custom) {
								$counter++; 
								echo $custom->name; if ($counter < $t) echo ', ';
							}?>
							</p>
						</div>
					</div>
				</div>
			<?php }
			elseif ($portfolio_presentation == "small_image"){ ?>
			<!-- Small Image -->
				<div class="tiers_column_inverse">
					<div class="tiers_column_inverse inner1 smallslimg">
						<div class="image_post sbimg smsl">
							<ul>
								<?php if (is_array($image)) : ?>
								<?php foreach($image as $attachment_id => $image): ?>
								<li>
									<img class="lazy" data-original="<?php echo esc_url(wp_get_attachment_url($attachment_id)); ?>" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQI12P4zwAAAgEBAKrChTYAAAAASUVORK5CYII=" title="<?php echo esc_attr(get_the_title($attachment_id)); ?>" alt="<?php echo esc_attr(get_post_meta($attachment_id, '_wp_attachment_image_alt', true)); ?>">
								</li>
								<?php endforeach; ?>
								<?php endif; ?>
								<?php if (is_array($video1)) : ?>
								<?php foreach($video1 as $video => $video1) : ?>
									<li>
										<div class="video_post">
											<?php echo wp_oembed_get(esc_url($video1)); ?>
										</div> 
									</li>
								<?php endforeach; ?>
								<?php elseif($video1 != "") : /* old version */?>
								<li>
									<div class="video_post">
										<?php echo wp_oembed_get(esc_url($video1)); ?>
									</div> 
								</li>
								<?php ?>
								<?php endif;?>
							</ul>
							<?php if ($share_port != "no") {?>
								<div class="share-opf">
									<button class="md-icon-email"></button>
									<button class="iconfa-facebook"></button>
									<button class="iconfa-twitter"></button>
									<button class="md-icon-share"></button>
								</div>
							<?php } ?>
						</div>
					</div>

					<div class="tiers_column_inverse inner2 meta-area smallslimg">
						<div class="portfolio-content">
							<h3> <?php esc_html_e( "ABOUT THIS PROJECT", "gotham" ); ?> </h3>
							<?php if ($about_project != "") {?>
								<p><?php echo wp_kses($about_project, array('br'=> array(), 'strong' => array())); ?></p>
							<?php } ?>
						</div>
						<?php if ($portfolio_url != "") {?>
							<div class="purl">
								<a href="<?php echo esc_url($portfolio_url); ?>"> <?php esc_html_e( "VISIT WEBSITE", "gotham" ); ?> </a>
							</div>
						<?php } ?>
						<?php if ($colorpalette != "") {?>
							<div class="palette">
								<span> <?php esc_html_e( "PALETTE", "gotham" ); ?> </span>
							</div>
						<?php } ?>
						<?php if ($portfolio_date != "") {?> 
							<div class="pdate">
								<h3> <?php esc_html_e( "Date", "gotham" ); ?> </h3>
								<p> <?php echo esc_html($portfolio_date); ?> </p>
							</div>
						<?php } ?>

						<div class="portfolio-category">
							<h3> <?php esc_html_e( "Category", "gotham" ); ?> </h3>
							<p><?php $custom = wp_get_post_terms(get_the_ID(), 'portfolio_category'); $t = count($custom); $counter = 0;
							foreach($custom as $custom) {
								$counter++; 
								echo $custom->name; if ($counter < $t) echo ', ';
							}?>
							</p>
						</div>
						<?php wp_link_pages(array('before' => '<div class="link_pages"><ul>', 'after' => '</ul></div>', 'pagelink' => '<span>%</span>')); ?>
					</div>
				</div>
			<?php }
			elseif ($portfolio_presentation == "small_slider"){ ?>
			<!-- Small slider -->
				<div class="tiers_column_inverse">
					<div class="tiers_column_inverse inner1 smallslimg">
						<div class="image_post smsl">
							<div class="flexslider">
								<ul class="slides">
									<?php if (is_array($image)) : ?>
									<?php foreach($image as $attachment_id => $image): ?>
									<li>
										<img src="<?php echo esc_url(wp_get_attachment_url($attachment_id)); ?>" title="<?php echo esc_attr(get_the_title($attachment_id)); ?>" alt="<?php echo esc_attr(get_post_meta($attachment_id, '_wp_attachment_image_alt', true)); ?>">
									</li>
									<?php endforeach; ?>
									<?php endif; ?>
									<?php if (is_array($video1)) : ?>
									<?php foreach($video1 as $video => $video1) : ?>
										<li>
											<div class="video_post">
												<?php echo wp_oembed_get(esc_url($video1)); ?>
											</div> 
										</li>
									<?php endforeach; ?>
									<?php elseif($video1 != "") : /* old version */?>
									<li>
										<div class="video_post">
											<?php echo wp_oembed_get(esc_url($video1)); ?>
										</div> 
									</li>
									<?php ?>
									<?php endif;?>
								</ul>
								<?php if ($share_port != "no") {?>
									<div class="share-opf">
										<button class="md-icon-email"></button>
										<button class="iconfa-facebook"></button>
										<button class="iconfa-twitter"></button>
										<button class="md-icon-share"></button>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>

					<div class="tiers_column_inverse inner2 meta-area smallslimg">
						<div class="portfolio-content">
							<h3> <?php esc_html_e( "ABOUT THIS PROJECT", "gotham" ); ?> </h3>
							<?php if ($about_project != "") {?>
								<p><?php echo wp_kses($about_project, array('br'=> array(), 'strong' => array())); ?></p>
							<?php } ?>
						</div>
						<?php if ($portfolio_url != "") {?>
							<div class="purl">
								<a href="<?php echo esc_url($portfolio_url); ?>"> <?php esc_html_e( "VISIT WEBSITE", "gotham" ); ?> </a>
							</div>
						<?php } ?>
						<?php if ($colorpalette != "") {?>
							<div class="palette">
								<span> <?php esc_html_e( "PALETTE", "gotham" ); ?> </span>
							</div>
						<?php } ?>
						<?php if ($portfolio_date != "") {?> 
							<div class="pdate">
								<h3> <?php esc_html_e( "Date", "gotham" ); ?> </h3>
								<p> <?php echo esc_html($portfolio_date); ?> </p>
							</div>
						<?php } ?>

						<div class="portfolio-category" > 
							<h3> <?php esc_html_e( "Category", "gotham" ); ?> </h3>
							<p><?php $custom = wp_get_post_terms(get_the_ID(), 'portfolio_category'); $t = count($custom); $counter = 0;
							foreach($custom as $custom) {
								$counter++; 
								echo $custom->name; if ($counter < $t) echo ', ';   
							}?>
							</p>
						</div>
						<?php wp_link_pages(array('before' => '<div class="link_pages"><ul>', 'after' => '</ul></div>', 'pagelink' => '<span>%</span>')); ?>
					</div>
				</div>
			<?php }
			else { ?>
			<!-- else -->
				<div class="column75-25">
					<div class="column75-25 inner1">
						<div class="portfolio-content">
							<h3> <?php esc_html_e( "ABOUT THIS PROJECT", "gotham" ) ?> </h3>
							<?php if ($about_project != "") {?>
								<p><?php echo wp_kses($about_project, array('br'=> array(), 'strong' => array())); ?></p>
							<?php } ?>
						</div>
						<?php wp_link_pages(array('before' => '<div class="link_pages"><ul>', 'after' => '</ul></div>', 'pagelink' => '<span>%</span>')); ?>
						<?php if ($portfolio_url != "") { ?>
							<div class="purl">
								<a href="<?php echo esc_url($portfolio_url); ?>"> <?php esc_html_e( "VISIT WEBSITE", "gotham" ) ?> </a>
							</div>
						<?php } ?>
						<?php if ($colorpalette != "") {?>
							<div class="palette">
								<span> <?php esc_html_e( "PALETTE", "gotham" ) ?> </span>
							</div>
						<?php } ?>
					</div>
					<div class="column75-25 inner2 meta-area">
						<?php if ($portfolio_date != "") { ?> 
						<div class="pdate">
							<h3> <?php esc_html_e( "Date", "gotham" ); ?> </h3>
							<p> <?php echo esc_html($portfolio_date); ?> </p>
						</div>
						<?php } ?>

						<div class="portfolio-category" > 
							<h3> <?php esc_html_e( "Category", "gotham" ); ?> </h3>
							<p><?php $custom = wp_get_post_terms(get_the_ID(), 'portfolio_category'); $t = count($custom); $counter = 0;
							foreach($custom as $custom) {
								$counter++; 
								echo $custom->name; if ($counter < $t) echo ', ';
							}?>
							</p>
						</div>
					</div>
				</div>
			<?php } ?>

			<?php endwhile;?> <?php endif; ?>
		</section>
		<?php if ($colorpalette != ""){?>
			<div class="colorpalette">
				<div class="palette-overlay"></div>
				<div class="palette-content">
					<h3> <?php esc_html_e( "Color picker", "gotham" ); ?> </h3>
					<div class="colorp1w"> <span class="colorp1r"></span><p><?php echo esc_html($colorp1); ?></p> </div>
					<div class="colorp2w"> <span class="colorp2r"></span><p><?php echo esc_html($colorp2); ?></p> </div>
					<div class="colorp3w"> <span class="colorp3r"></span><p><?php echo esc_html($colorp3); ?></p> </div>
					<div class="colorp4w"> <span class="colorp4r"></span><p><?php echo esc_html($colorp4); ?></p> </div>
				</div>
			</div>
		<?php } ?>
	  
	</div>

	<?php $nextPost = get_next_post(); $prevPost = get_previous_post(); ?>
	<?php if ($prevPost != "") {
		$prevtitle = $prevPost->post_title; 
		if (strlen($prevtitle) > 25) {
			$prevshortcut = substr($prevtitle,0,25).'...';
		}
		else {
			$prevshortcut = $prevtitle;
		}?>
		<div class="wrpgp">
			<?php if ( $prevPost != "") {?>
				<a href="<?php echo esc_url(get_permalink( $prevPost->ID )); ?>"><div class="pagination_prev"><span>Previous:</span><span><?php echo $prevshortcut; ?></span></div></a>
			<?php } ?>
		</div>
	<?php } ?>

	<?php if ($nextPost != "") {?>
		<?php $nexttitle = $nextPost->post_title; 
		if (strlen($nexttitle) > 25) {
			$shortcut = substr($nexttitle,0,25).'...';
		}
		else {
			$shortcut = $nexttitle;
		}?>
		<div class="wrpga">
			<?php if ( $nextPost != "") {?>
				<a href="<?php echo esc_url(get_permalink( $nextPost->ID )); ?>"><div class="pagination_after"><span>Next:</span><span><?php echo $shortcut; ?></span></div></a>
			<?php } ?>
		</div>
	<?php } ?>

	<?php if ( get_theme_mod('smooth_scroll') == 'yes' ) {?>
		<i class="icon-ios7-arrow-thin-up scroll-to-top"><span>Top</span> </i>
	<?php } ?>

</div>
<div class="modalmn"></div>

<?php } else { echo get_the_password_form(); }?>

<?php wp_footer();?>
</body>
</html>