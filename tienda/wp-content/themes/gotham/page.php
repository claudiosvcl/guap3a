<?php
/**
 * The template for displaying all pages
 */

get_header(); ?>

<?php if(! post_password_required()) {?>

<?php get_template_part('content/title_area'); ?>

<div id="content" class="spec dftpg">

	<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

	<?php get_template_part('content/content'); ?>

	<?php endwhile; endif; ?>

</div>

<?php } else { echo get_the_password_form(); }?>

<?php get_footer(); ?>