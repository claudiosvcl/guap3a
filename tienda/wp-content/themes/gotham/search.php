<?php
/**
 * The template for displaying search results pages
 */

get_header(); ?>

<?php $count = $wp_query->found_posts; ?>

<?php if ( have_posts() ) : ?>
<div class="below_header"> 
	<div class="h-below hbidar">
		<div class="h-belowrapper">
			<div class="title-wr twcenter">
				<h1 class="single-h1 archtitle"><?php printf( esc_html__( 'Search Results for: %s', 'gotham' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
				<div class="numb-posts"><span class="nbpc"><?php echo $count; ?></span><span class="nbpar"><?php if($count > 1){ esc_html_e('ALL RESULTS', 'gotham'); } else { esc_html_e('ALL RESULT', 'gotham'); } ?></span></div>
			</div>
		</div>
	</div>
</div>

<div id="content" class="spec indasd">
	<section class="iasec">
		<?php while ( have_posts() ) : the_post(); ?>
		<div class="search_layout">

		<?php $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ) , 'full' ); $video = esc_url( get_post_meta( get_the_ID(), 'gotham_video_post', 1 ) ); $quote = get_post_meta( get_the_ID(), 'gotham_quote_post', true ); $author_quote = get_post_meta( get_the_ID(), 'gotham_author_quote_post', true ); $audio = esc_url( get_post_meta( get_the_ID(), 'gotham_audio_post', 1 ) ); $link = get_post_meta( get_the_ID(), 'gotham_link_post', true ); $link_text = get_post_meta( get_the_ID(), 'gotham_link_text', true ); $the_title_Post = ''; 
		
		$title_Post = get_the_title();
  		if ($title_Post != "") {
	  		if (strlen($title_Post) > 37) {
	  			$the_title_Post = substr($title_Post,0,37).'...';
	  		}
	  		else {
				$the_title_Post=$title_Post;
	  		}
  		}
  		$lcategory = get_the_category();
		?>

			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if ( 'post' == get_post_type() ) : ?>
				<ul>
					<li class="blog-big-item all">
						<article>
							<?php if ( ($video != "") && (has_post_format("video")) ) {?> 
							<div class="video_post">
								<?php echo wp_oembed_get( $video ); ?>
							</div>
							<?php }

							elseif  ( ($quote != "") && (has_post_format("quote")) ) {?>
								<div class="wrap-icon-text">
									<i class="md-icon-quote"></i>
									<?php echo gotham_getPostLikeLink(get_the_ID());?>
									<a href="<?php the_permalink() ?>">
										<div class="text_citation">
											<p><?php echo esc_html( $quote ); ?></p>
											<p><?php echo esc_html( $author_quote ); ?></p>
										</div>
									</a>
									<div class="links lquote"><a href="<?php the_permalink() ?>"></a></div>
								</div>
							<?php }
							elseif ( ($audio != "") && (has_post_format("audio")) ) {?>
								<div class="audio_post">
									<?php echo wp_oembed_get( $audio ); ?>
								</div>
							<?php }
							elseif ( ($link != "") && (has_post_format("link")) ) {?>
								<div class="wrap-icon-text">
									<i class="md-icon-link"></i>
									<?php echo gotham_getPostLikeLink(get_the_ID());?>
									<a href="<?php the_permalink() ?>">
										<div class="text_link">
											<p><?php echo esc_html( $link_text ); ?></p>
										</div>
									</a>
									<div class="links"><a href="<?php the_permalink() ?>"></a></div>
									<a href="<?php echo esc_url( $link ); ?>">
										<div class="text_link">
											<span><?php echo esc_html( $link ); ?></span>
										</div>
									</a>
								</div>
							<?php }
							elseif ( (has_post_thumbnail()) && ((has_post_format("link")or(has_post_format("audio")or(has_post_format("quote")or(has_post_format("video") ) )))== "") ) {?>
								<a href="<?php the_permalink() ?>"  title="<?php the_title_attribute(); ?>">
									<div class="fitbgimg" style="background-image:url('<?php echo $thumbnail_src[0]; ?>')"></div>
								</a>
							<?php }
							else { ?>
								<a href="<?php the_permalink() ?>"  class="rpbackgroundcol" title="<?php the_title_attribute(); ?>"></a>
							<?php } ?>

							<?php if( (has_post_format("link")or(has_post_format("quote")))== "" ) {?>
								<div class="blog-thumb">
									<?php echo gotham_getPostLikeLink(get_the_ID());?>
									<a class="more-description"></a>
									<h3><a href="<?php the_permalink() ?>"><?php echo $the_title_Post; ?></a></h3>
									<div class="athcat">
										<p class="author"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php echo get_avatar( get_the_author_meta( 'ID' ), 20 ); ?> <?php the_author(); ?></a></p>
										<span><?php esc_html_e('in', 'gotham'); ?></span>
										<div class="category"><a href="<?php echo get_category_link($lcategory[0]);?>"><?php echo $lcategory[0]->cat_name; ?></a></div>
									</div>
									<p class="excerpt"><a href="<?php the_permalink() ?>"><?php echo gotham_custom_excerpt_length(); ?></a></p>
									<div class="links"><a href="<?php the_permalink() ?>"></a></div>
									<p class="date"><a href="<?php the_permalink() ?>"><?php echo get_the_date(); ?></a></p>
								</div>
							<?php } ?>
						</article>
					</li>
				</ul>

				<?php elseif ( 'portfolio' == get_post_type() ) : ?>

				<ul>
					<li class="blog-big-item all">
						<article class="search-ptf">
							<a href="<?php the_permalink() ?>"  title="<?php the_title_attribute(); ?>">
								<div class="fitbgimg" style="background-image:url('<?php echo $thumbnail_src[0]; ?>')"></div>
							</a>
							<div class="blog-thumb">
								<h4><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h4>
								<?php the_excerpt(); ?>
							</div>
						</article>
					</li>
				</ul>

				<?php elseif ( class_exists( 'WooCommerce' ) && 'product' == get_post_type() ) : ?>

				<ul>
					<li class="blog-big-item all">
						<article class="search-ptf">
							<a href="<?php the_permalink() ?>"  title="<?php the_title_attribute(); ?>">
								<div class="fitbgimg" style="background-image:url('<?php echo $thumbnail_src[0]; ?>')"></div>
							</a>
							<div class="blog-thumb woosresult">
								<h4><a href="<?php the_permalink() ?>"><?php echo $the_title_Post; ?></a></h4>
								<p><a href="<?php the_permalink() ?>"><?php echo gotham_custom_excerpt_length(); ?></a></p>
							</div>
						</article>
					</li>
				</ul>

				<?php else : ?>
				<h4><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h4>
				<a href="<?php the_permalink(); ?>">
					<?php the_excerpt(); ?>
				</a>
				<?php endif; ?>

			</div>
		</div>
		<?php endwhile; ?>

	</section>
</div>

<?php get_template_part('content/pagination'); ?>

<?php get_footer(); ?>

<?php else : ?>
<div class="below_header"> 
  	<div class="h-below">
		<div class="h-belowrapper">
			<div class="title-wr twcenter">
				<h1 class="single-h1 archtitle"><?php esc_html_e( 'Nothing Found', 'gotham' ); ?></h1>
				<div class="numb-posts"><span class="nbpc"><?php echo $count; ?></span><span class="nbpar"><?php esc_html_e('RESULT', 'gotham');?></span></div>
			</div>
		</div>
	</div>
</div>
<div id="content" class="spec indasd">
	<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'gotham' ); ?></p>
</div>
<?php get_footer(); ?>
<?php endif; ?>