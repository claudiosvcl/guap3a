<?php
/**
 * The template for displaying our searchform
 */
?>

<div id="searchoverlay">
	<form method="get" id="searchform" action="<?php echo  esc_url(home_url( '/' )); ?>" autocomplete="off">
		<input type="search" value="" name="s" id="s" placeholder="<?php esc_attr_e('Search...', 'gotham');?>"/>
		<button type="submit" id="searchsubmit" class="md-icon-search"></button>
	</form>
	<span class="search-close"></span>
	<span class="uihsearch"></span>
</div>