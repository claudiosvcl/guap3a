<?php
/**
 * The template for displaying 404 pages
 */

get_header(); ?>

<div id="content">
	<section class="error_four">
		<div class="err-wr">
			<h1><?php esc_html_e( "Oops! That page can&rsquo;t be found.", "gotham" ); ?></h1>
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
				<?php esc_html_e( "RETURN", "gotham" ); ?>
			</a>
		</div>
	</section>
</div>

<?php wp_footer();?>