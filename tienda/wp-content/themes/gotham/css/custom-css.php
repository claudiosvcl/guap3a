<?php

header('Content-type: text/css');
ob_start('ob_gzhandler');
header('Cache-Control: max-age=31536000, must-revalidate');

//compressing the CSS
ob_start("compress");
function compress($buffer) {
    //Remove CSS comments
    $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
    //Remove tabs, spaces, newlines, etc.
    $buffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $buffer);
    return $buffer;
}
if (isset($_GET['post-id'])) {
	/*if $_GET['post-id'] is set*/ 
	$id = $_GET['post-id'];
}
?>

<?php 
$title_area = get_post_meta( $id, 'gotham_select_title_area', true ); $title_area_parallax = get_post_meta( $id, 'gotham_title_area_parallax', true ); $title_area_background_image = get_post_meta( $id, 'gotham_title_area_background_image', true );
$area_height = get_post_meta( $id, 'gotham_area_height', true ); $title_area_color = get_post_meta( $id, 'gotham_title_area_color', true ); $title_area_background_color = get_post_meta( $id, 'gotham_title_area_background_color', true ); 
$information_title_color = get_post_meta( $id, 'gotham_information_title_color', true ); $information_text_color = get_post_meta( $id, 'gotham_information_text_color', true );
$portfolio_presentation = get_post_meta( $id, 'gotham_portfolio_presentation', true ); $portfolio_background_color = get_post_meta( $id, 'gotham_portfolio_background_color', true );
$header_style = get_post_meta( $id, 'gotham_header_style', true ); $colorp1 = get_post_meta( $id, 'gotham_color_p1', true ); $colorp2 = get_post_meta( $id, 'gotham_color_p2', true );
$colorp3 = get_post_meta( $id, 'gotham_color_p3', true ); $colorp4 = get_post_meta( $id, 'gotham_color_p4', true ); $project_background_color = get_post_meta( $id, 'gotham_project_background_color', true );
$select_sidebar = get_post_meta( $id, 'gotham_select_sidebar', true ); $primary = get_theme_mod('primary'); $secondary = get_theme_mod('secondary');
?>

<?php if ($primary != '') { ?>
.wrap-flex .flex-control-nav li.flex-active, .flex-direction-nav a:hover, .team-info .team-social .send a,
.map-information .md-icon-expand-more, .link_pages ul li.active, mark, ins,
.ui-autocomplete li.search-info .sebgcopo,
.wpcf7-form input[type=checkbox]:checked:after,
.woocommerce div.header-cart a.cart-contents span, div.header-cart a.cart-contents span,
.woocommerce div.header-cart .woocommerce.widget_shopping_cart p.buttons a:hover, div.header-cart .woocommerce.widget_shopping_cart p.buttons a:hover,
.woocommerce div.product form.cart button.button:hover,
.woocommerce #respond form input#submit:hover,
.woocommerce .woocommerce-message a.button:hover,
.woocommerce-cart .woocommerce .cart_totals a.button.alt:hover,
.woocommerce-checkout .woocommerce .check-order .woocommerce-checkout-payment input.button.alt:hover,
.woocommerce-checkout .woocommerce form.login .button:hover,
.woocommerce-account .woowrap-account .woocommerce-MyAccount-content .woocommerce-info a.button:hover,
.woocommerce-account .woowrap-account input.button:hover,
.woocommerce .woowrap-account .woocommerce-Pagination a.button:hover,
.woocommerce-account .form-row input.button:hover,
.woocommerce .widget-area #sidebar .woocommerce.widget_shopping_cart p.buttons a:hover,
.woocommerce .widget-area #sidebar .woocommerce.widget_price_filter .button:hover,
.woocommerce div.widget-area .woocommerce.widget_price_filter .ui-slider .ui-slider-range,
.woocommerce div.widget-area .woocommerce.widget_price_filter .ui-slider .ui-slider-handle,
.woocommerce div.summary p.cart .single_add_to_cart_button:hover,
div.woocommerce button.button:hover,
div.woocommerce input.button:hover{background: <?php echo $primary; ?>;}
#filter-line, #pt-filter-line, .numb-posts .nbpar,
.wpcf7-form p .wpcf7-form-control-wrap:after,
.wpcf7-form input[type=checkbox]:checked:after,
.woocommerce .woosproduct div.product .woocommerce-tabs ul.tabs li.active,
.woocommerce-account .woowrap-account .woocommerce-MyAccount-navigation ul li.is-active{border-color:<?php echo $primary; ?>}
.loader-activity{border-top-color:<?php echo $primary; ?>; border-left-color:<?php echo $primary; ?>;}
<?php } ?>

<?php if ($secondary != '') { ?>
.ui-autocomplete li.search-info .sebgcopa{background: <?php echo $secondary; ?>;}
<?php } ?>

<?php if ( $title_area != "no") { ?>
  #content.dftpg {
    padding-top: 80px;
  }
@media only screen and (max-width: 1024px) {
  #content.dftpg {
    padding-top: 30px;
  }
}
@media only screen and (max-width: 480px) {
  body.tc_blog #content.dftpg{
    padding-top: 0; padding-bottom: 0;
  }
  body.page-template-default.tc_blog.tc_masonry_blog #content{
        padding-top: 0;
  }
}
<?php } ?>

/* animation bottom to top */
<?php if(get_theme_mod('bottom_to_top') == "no") { ?>
.below_header, #content, .single-bh {
  -webkit-transform: translate3d(0,0,0);
  transform: translate3d(0,0,0);
}
<?php } ?>

<?php if ($select_sidebar != '') { ?>
@media only screen and (max-width: 991px) {
  #content:not(.menu-visible){transform:none !important;}
}
<?php } ?>

<?php if (( $title_area_parallax == '' )&&( $title_area_background_color != '' )&&( $title_area_background_color != '#' )){?>
.h-below:not(.hbidar), .single-screen{
  background-color: <?php echo $title_area_background_color; ?>;
}
<?php } ?>
<?php if ($area_height != '') { ?>
.h-below:not(.hbidar), .single-screen{
  height: <?php echo esc_html( $area_height );?>px;
}
@media screen and (min-width:500px) {
    .single-portfolio .below_header{
        height:<?php echo esc_html( $area_height );?>px;
    }
}
@media screen and (max-width: 499px){
   .single-portfolio .h-below{height: 100%;}
}
<?php } ?>

<?php if (( $title_area_color != '' )&&( $title_area_color != '#' )){?>
.single-title a, .single-description a, .single-description p, .single-description p:after,
.single-thumb .md-icon-expand-more.stdown{color: <?php echo $title_area_color; ?> !important;}
<?php } ?>

h1.single-h1:not(.archtitle), h1.single-title{
<?php if (( $title_area_color != '' )&&( $title_area_color != '#' )){ ?>
color: <?php echo $title_area_color;?> !important;
<?php } ?>
<?php if(get_theme_mod('pgtitle_font_size') != "") { ?>
font-size: <?php echo get_theme_mod('pgtitle_font_size');?>px !important;
<?php } ?>
}

h1.single-h1, h1.single-title{
<?php if(get_theme_mod('pgtitle_font_weight') != "") {?>
font-weight:<?php echo get_theme_mod('pgtitle_font_weight');?> !important;
<?php } ?>
}

<?php if (( $title_area_color != '' )&&( $title_area_color != '#' )){?>
.breadcrumbs, .breadcrumbs a, .subtitle, .cr-bullet:before, .woocommerce .breadcrumbs .woocommerce-breadcrumb,
.woocommerce .breadcrumbs .woocommerce-breadcrumb a{
color: <?php echo $title_area_color;?>;
}
<?php } ?>

<?php if (( $title_area_color != '' )&&( $title_area_color != '#' )){?>
.title-wr .md-icon-expand-more.stdown, 
.title-wr .md-icon-expand-more.stdown:before,
.title-wr .md-icon-expand-more.stdown small.scr-down,
.pclient>h3, .pclient>p{
color: <?php echo $title_area_color;?> !important;
}
.title-wr .md-icon-expand-more.stdown:after{
	background: <?php echo $title_area_color;?> !important;
}
<?php } ?>

<?php if ( $information_title_color != '' ){?>
.pdate>h3, .purl>h3, .portfolio-category>h3, .portfolio-content>h3, .purl a,
.palette span{
color: <?php echo $information_title_color;?>;
}
<?php } ?>

<?php if ( $information_text_color != '' ){?>
.pdate>p, .portfolio-category>p, .portfolio-content>p{
color: <?php echo $information_text_color;?>;
}
<?php } ?>

<?php if($portfolio_presentation == ""){?>
.column75-25 .inner1, .column75-25 .inner2{margin-top: 80px;}
<?php } ?>

<?php if($project_background_color != ""){?>
.project_title, .tiers_column_inverse, .sbimg ul, section.bibs{
background: <?php echo $project_background_color;?>;
} 
section.bibs.liftp{
padding: 0px 0 80px 0 !important; margin-top: 30px;
} 
@media only screen and (max-width: 699px) {
.portfolio-home section.rempad{background: <?php echo $project_background_color;?>;}
}
<?php } ?>

<?php if($portfolio_background_color != ""){?>
.portfolio-bg, .portfolio-bg #content{background: <?php echo $portfolio_background_color;?>;}
<?php } ?>

<?php if($colorp1 != ""){?>
.palette-content .colorp1r{background: <?php echo $colorp1; ?>;}
<?php } ?>

<?php if($colorp2 != ""){?>
.palette-content .colorp2r{background: <?php echo $colorp2; ?>;}
<?php } ?>

<?php if($colorp3 != ""){?>
.palette-content .colorp3r{background: <?php echo $colorp3; ?>;}
<?php } ?>

<?php if($colorp4 != ""){?>
.palette-content .colorp4r{background: <?php echo $colorp4; ?>;}
<?php } ?>

<?php if (($select_sidebar == "sidebar_right")||($select_sidebar == "sidebar_left")){?>
@media screen and (min-width: 43.75em){
#contain li.blog-item{width:46%;}
}
<?php } ?>

<?php if((get_theme_mod('body_bgcolor') != "")&&(get_theme_mod('bg_image') == "")&&(get_theme_mod('bg_image_pattern') == "")) { ?>
body, body.tc_blog, body.page.woocommerce-page, body.archive.woocommerce-page,
body.single-product.woocommerce{background-color:<?php echo get_theme_mod('body_bgcolor'); ?>;}
<?php } ?> 
<?php if(get_theme_mod('bg_image') != ""){ ?>
body, body.tc_blog, body.page.woocommerce-page, body.archive.woocommerce-page,
body.single-product.woocommerce{background-image:url(<?php echo get_theme_mod('bg_image'); ?>); background-repeat: no-repeat; background-position: center 0px; background-attachment: fixed;}
<?php } ?>
<?php if((get_theme_mod('bg_image_pattern') != "")&&(get_theme_mod('bg_image') == "")) { ?>
body, body.tc_blog, body.page.woocommerce-page, body.archive.woocommerce-page,
body.single-product.woocommerce{background-image:url(<?php echo get_theme_mod('bg_image_pattern'); ?>); background-repeat: repeat; background-position: 0px 0px;}
<?php } ?>
        
<?php if(get_theme_mod('content_bgcolor') != "") {?>#content, body.tc_blog #content{background-color:<?php echo get_theme_mod('content_bgcolor') ?>;} <?php } ?>

<?php if(get_theme_mod('logo_height') != "") {?>#logo img{height:<?php echo get_theme_mod('logo_height'); ?>px;} <?php } ?>

<?php if ($header_style == 'light'){?>
.menu-trigger span:before, .menu-trigger span:after, .menu-trigger span{background:#fff;}.more-sdbmb:before, .explore a:before, .h-wrap #searchoverlay #searchsubmit,
	.main-nav .uihmenu:before, .h-wrap #searchoverlay .uihsearch:before, .explore a:after, .uihpost:before, .uihpost:after, .header-cart>a.cart-contents:before{color:#fff;}
  header.scroll-up{background: rgba(32, 32, 32, 0.95);}
<?php } ?>

<?php if (($header_style == 'dark')||($header_style == '')){?>.menu-trigger span:before, .menu-trigger span:after, .menu-trigger span{background:#202020;}.more-sdbmb:before, .explore a:before, .h-wrap #searchoverlay #searchsubmit, .h-wrap #searchoverlay #searchsubmit,
	.main-nav .uihmenu:before, .h-wrap #searchoverlay .uihsearch:before, .explore a:after, .uihpost:before, .uihpost:after, .header-cart>a.cart-contents:before{color:#202020;}<?php } ?>

<?php if (get_theme_mod('nav_bgimage') != ""){?>.navigation{background-image: url(<?php echo get_theme_mod('nav_bgimage');?>);}<?php } ?>

<?php if(get_theme_mod('mgsearch') == 'no') { ?>
@media only screen and (max-width: 991px) {
  .column70-30 > .inner2 .more-sdbmb:before, .column70-30 > .inner2_left .more-sdbmb:before{right: 66px;}
}
@media only screen and (max-width: 699px) {
  .column70-30 > .inner2 .more-sdbmb:before, .column70-30 > .inner2_left .more-sdbmb:before{right: 58px;}
}
body.woocommerce .header-cart, body .header-cart {
  right: 80px;
}
@media only screen and (max-width: 991px) {
  body.woocommerce .header-cart, body .header-cart {
    right: 60px;
  }
}
@media only screen and (max-width: 699px) {
  body.woocommerce .header-cart, body .header-cart {
    right: 52px;
  }
}

body.woocommerce .header-cart .woocommerce.widget_shopping_cart, body .header-cart .woocommerce.widget_shopping_cart {
  margin-right: -44px;
}

body.woocommerce .header-cart .woocommerce.widget_shopping_cart:before, body .header-cart .woocommerce.widget_shopping_cart:before {
  margin-left:221px;
}

body.woocommerce .header-cart .woocommerce.widget_shopping_cart:after, body .header-cart .woocommerce.widget_shopping_cart:after {
  margin-left: 222px;
}

<?php }?>

<?php if (get_theme_mod('boxed_layout') == 'yes'){ ?>
@media screen and (min-width : 992px) and (max-width: 1199px) {
  header{max-width: 910px;}
}

@media only screen and (min-width : 1200px){
  header{max-width: 1010px;}
}

.below_header.menu-visible, #content.menu-visible, .single-bh.menu-visible {
  transform: translate3d(-33%, 0, 0);
}

@media only screen and (max-width : 991px) {
  body.spec{padding:0; margin: 0; max-width:none;}
}

@media only screen and (min-width : 992px) {
  body.menu-visible #logo, body.menu-visible .uihpost, body.menu-visible .explore{
    opacity: 0;
  }
  #logo, .uihpost, .explore{
    opacity: 1; transition: opacity 300ms;
  }
  #logo.menu-visible{transform: none;}
  .buttonsh-wrap{right: 0;}
  .menu-trigger.menu-visible{
    opacity: 0; visibility: hidden;
  }
  .uihpost{left: auto; margin-left: 31px;}
  .explore{left: auto; margin-left: 30px;}
  .ct_overlay{width: auto;}
  .ct_overlay .spec{
    padding: 0; margin: 0; max-width: 1010px;
  }
}

@media screen and (min-width : 992px) and (max-width: 1199px) {
  .ct_overlay .spec{max-width: 910px;}
}
<?php } ?>

/* icons */
<?php if($primary != ""){ ?>.hovericon:hover{<?php if($primary != "") {?>background-color:<?php echo $primary; ?>!important; <?php } ?> } <?php } ?>
        
/* button general */
<?php if($primary != ''){?>#submit:hover{background-color:<?php echo $primary; ?>; transition: 0.8s;} <?php } ?>

/* fonts */

/* Main font family */

body{
<?php if (strpos(get_option('main_font_family'), " ") !== false) { ?>
font-family:"<?php echo get_option('main_font_family'); ?>"
<?php } else {?>font-family:<?php echo get_option('main_font_family', 'Montserrat, arial, sans-serif'); ?> <?php } ?>; }


#sidebar #searchform ::-webkit-input-placeholder, #author, #email, #url{
<?php if (strpos(get_option('main_font_family'), " ") !== false) { ?>
font-family:"<?php echo get_option('main_font_family'); ?>"
<?php } else {?>font-family:<?php echo get_option('main_font_family', 'Montserrat, arial, sans-serif'); ?> <?php } ?>;
}

/* secondary font family */

#content .blog-item.full .wrap-icon-text .text_citation p:nth-child(2), 
#content>article>.post-content, .blog-big-item .blog-thumb .date a, 
.blog-big-item .text_citation p:nth-child(2), .blog-big-item .text_link span, 
.blog-thumb .excerpt a, .comment-list .comment-text p, .copyright, 
.ct_overlay p, .map-information p, .mwlw > a, .mwlw p, 
.pagination_after span:last-child, .pagination_prev span:last-child, .pclient>h3, .pclient>p, 
.pdate>p, .portfolio-category>p, .portfolio-content>p, 
.portfolio-item .shoot-e .category, .ppcatp, .s-author-desc, 
.search_layout .type-page>a, .single-content .text_citation p:nth-child(2), 
.single-description, .single-h1 span, .team-info .team-content, 
.thumb .category a, .title-wr .subtitle, 
.widget_random_entries .rpwrp_wrap .rptd .rdpcat, 
.widget_recent_entries .post-date, .widget_text p, .wrap-flex .process p, 
.wrsl-flex .slide-wrap p, li.blog-item .blog-thumb .date, 
li.blog-item .text_link span, #comment, .comment-form-comment textarea,
.sh-bgcaption .sh-desc, .dsptbb .category, .wooexcerpt,
.woocommerce #reviews #comments ol.commentlist li .comment-text .description,
.woocommerce-checkout #payment div.payment_box,
.woocommerce-checkout .login p:first-child,
.woocommerce-checkout .woocommerce-password-hint,
.woocommerce-order-received .woocommerce p:not(.woocommerce-thankyou-order-received),
.woocommerce-order-received address,
.woocommerce-account address, .woocommerce-account .woocommerce-password-hint,
.search-results .woosresult p, .woocommerce .track_order p:first-child{
<?php if (strpos(get_option('secondary_font_family'), " ") !== false) { ?>
font-family:"<?php echo get_option('secondary_font_family'); ?>"
<?php } else {?>font-family:<?php echo get_option('secondary_font_family', '"Crimson Text", serif'); ?> <?php } ?>; }



/* widget area */
<?php if($primary != "") {?>
#sidebar #wp-calendar #today{background-color:<?php echo $primary; ?>;}
#sidebar .widget_calendar caption{background: <?php echo $primary; ?>}
<?php } ?>

/* Blog */        
#content>article>.post-content{
<?php if (get_theme_mod('post_font_size') != "") {?> font-size:<?php echo get_theme_mod('post_font_size'); ?>px;  <?php } ?>
}

/* text selection color */
<?php if($primary != "") {?>::selection {background:<?php echo $primary; ?>;}
::-moz-selection{background:<?php echo $primary; ?>;}
<?php } ?>

<?php if(get_theme_mod('custom_theme_css') != "") {?>         
<?php $custom_theme_css = get_theme_mod('custom_theme_css'); ?>
<?php echo $custom_theme_css . "\n";?>
<?php } ?>

/* error 404 */
<?php if(get_theme_mod('err_bg_image', get_template_directory_uri() . '/img/bg-404.jpg') != ""){?>
.error404 #content{background:url(<?php echo get_theme_mod('err_bg_image', get_template_directory_uri() . '/img/bg-404.jpg'); ?>);
background-size:cover; background-position:50%; background-repeat:no-repeat;
}
<?php } ?>

<?php ob_end_flush(); ?>