=== Gotham ===

Contributors: ThemeCrispy
Tags: smooth, clean, sharper-design, visual, mobile-optimized, creative, business, timeless, smart, important-details, easy

License: GNU General Public License (GPL)

== Description ==

Gotham is a WordPress Theme created by ThemeCrispy with a clean, modern design. Gotham highlights your portfolio, your posts, your content in a simple and secure way.

== Installation ==
	
1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.