// Animacion de navegacion
$(document).on('click', 'a.navegacion', function (e) {
    "use strict";
    event.preventDefault();
    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 1000);
});

$(document).ready(function () {
    "use strict";
    $('.navbar-collapse').on("click", "a.navegacion:not([data-toggle])", null, function () {
        $('.navbar-collapse').collapse('hide');
    });
});
