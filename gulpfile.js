// Gestor de tarea para claudiosalazar.cl

var gulp 			= require('gulp'),
	runSequence 	= require('run-sequence'),
	concat 			= require('gulp-concat'),
    uglify 			= require('gulp-uglify'),
    sass 			= require('gulp-sass'),
	postcss 		= require('gulp-postcss'),
    autoprefixer	= require('autoprefixer'),
	cssnano 		= require('cssnano'),
    plumber 		= require('gulp-plumber'),
	pngcrush 		= require('imagemin-pngcrush'),
    imagemin 		= require('gulp-imagemin'),
	notify 			= require('gulp-notify');

// Concatena y minimiza archivos JS
gulp.task('uglify', function() {
    "use strict";
	gulp.src(['./node_modules/jquery/dist/jquery.js','./node_modules/tether/dist/js/tether.js','./node_modules/bootstrap/dist/js/bootstrap.js','./dist/js/*.js'])
	.pipe(plumber())
	.pipe(concat('guap3a.js'))
	.pipe(uglify())
	.pipe(gulp.dest('./js'));
});

// Procesa archivos SASS
gulp.task('scss', function() {
    "use strict";
	gulp.src(['./dist/scss/*.scss', './node_modules/font-awesome/scss/font-awesome.scss','./node_modules/bootstrap/scss/bootstrap.scss' ])
	.pipe(plumber())
	.pipe(sass())
	.pipe(gulp.dest('./dist/css'));
	//.pipe(notify("Los archivos SASS se procesaron correctamente."));
});

// Agregar prefixer a CSS
gulp.task('css', function () {
    return gulp.src('./dist/css/*.css')
	.pipe(postcss([ autoprefixer(),cssnano()]))
	.pipe(gulp.dest('./css'));
});

//Tarea para comprimir imágenes
gulp.task('images', function() {  
 	"use strict";
	gulp.src('./dist/img/structure/*.{jpg,jpeg,png,gif}')
	.pipe(plumber())
	.pipe(imagemin({
		progressive: true,
		use: [pngcrush()]
	}))
  	.pipe(gulp.dest('./img/structure'))
	//.pipe(notify("Las imagenes se compilaron correctamente."));
});

// Ejecuta en orden todas las tareas
gulp.task('default', function(callback) {
	"use strict";
	runSequence('uglify', 'scss','images','css','watch',callback);
});

// Observa cambios realizados en el proyecto
gulp.task('watch', function() {
    "use strict";
	gulp.watch('./dist/js/*.js', ['uglify']);
    gulp.watch('./dist/scss/*.scss', ['scss']);
	gulp.watch('./dist/img/structure/*.{jpg,jpeg,png,gif}', ['images']);
	gulp.watch('./dist/css/*.css', ['css']);
	//gulp.watch('./dist/css/*.css', ['csso']);
});
